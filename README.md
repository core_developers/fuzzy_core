# Установка
1. Установить [git](https://git-scm.com/).
2. Скачать [python](https://www.python.org/downloads/) (3.7+).
3. Скачать [PyCharm](https://www.jetbrains.com/pycharm/) (или любую другую IDE).
4. Добавить SSH-ключ в [настройках гитлаба](https://gitlab.com/profile/keys).
5. Перейти в каталог, где будет находиться проект.
6. Через git bash выполнить `git clone git@gitlab.com:core_developers/fuzzy_core.git`.
7. Выполнить `pip install -r requirements.txt`.

# Работа с проектом
Для внесения изменений в проект нужно выполнить следующие действия:
1. Создать фиче-ветку.
2. Сделать изменения.
3. Написать тесты.
4. Запушить ветку в репозиторий в репозиторий.
5. Создать merge-request. [Как создать merge-request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).
6. Далее каждый из участников проекта может посмотреть код, внести свои замечания и предложения.
7. После прохождения ревью код вливается в мастер.

# Как писать тесты
Тесты хранятся в каталоге ./tests, структура которого должна полностью
повторять структуру ./src, единственное отличие будет состоять в префиксе
`test_` перед именем модуля и префиксe `test_` перед именем функции/класса.
Метаклассы пока не тестируем, пока не придумаем как это делать.
По всем вопросам - [Pytest Manual](https://docs.pytest.org/en/latest/).

# Code Style
[PEP8](https://pythonworld.ru/osnovy/pep-8-rukovodstvo-po-napisaniyu-koda-na-python.html)
