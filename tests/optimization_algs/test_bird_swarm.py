from src.optimization_algs.bird_swarm import BirdSwarm, create_params
import math as m
import random as r

"TODO: improve structure and naming, fix discoverin in VSCode"


def rastrigin(X):
    result = 0
    A = 10
    for x in X:
        result += x**2 - A * m.cos(2*m.pi*x)
    result += A * len(X)
    return result


def sphere(X):
    result = 0
    for x in X:
        result += x**2
    return result


def vboot(x): return (x[0] + 2*x[1] - 7)**2 + (2*x[0] + x[1] - 5)**2


class Test_BirdSwarm:

    def test_vfuncboot(self):
        N = 50
        D = 2
        params = create_params(3, 0.9, 1.5, 1.5, 1, 1, 0.75, False)
        bounds_vboot = [[-10, 10] for _ in range(D)]
        des_vboot = [[r.uniform(-10.0, 10.0)
                      for _ in range(D)] for _ in range(5*D)]
        bs = BirdSwarm()
        bs.find_min(vboot, bounds_vboot, des_vboot, params)
        for _ in range(N):
            bs.iterate()
        x = bs.log["solution_fit_history"][0]
        y = bs.log["solution_fit_history"][49]
        assert(y < x)

    def test_sphere(self):
        N = 100
        D = 20
        params = create_params(3, 0.9, 1.5, 1.5, 1, 1, 0.75, False)
        des_sphere = [[r.uniform(-100.0, 100.0)
                       for _ in range(D)] for _ in range(5*D)]
        bs = BirdSwarm()
        bs.find_min(sphere, None, des_sphere, params)
        for _ in range(N):
            bs.iterate()
        x = bs.log["solution_fit_history"][0]
        y = bs.log["solution_fit_history"][49]
        assert(y < x)

    def test_rastrigin(self):
        N = 100
        D = 20
        params = create_params(3, 0.9, 1.5, 1.5, 1, 1, 0.75, False)
        bounds_rastr = [[-5.12, 5.12] for _ in range(D)]
        des_rastr = [[r.uniform(-5.12, 5.12)
                      for _ in range(D)] for _ in range(5*D)]
        bs = BirdSwarm()
        bs.find_min(rastrigin, bounds_rastr, des_rastr, params)
        for _ in range(N):
            bs.iterate()
        x = bs.log["solution_fit_history"][0]
        y = bs.log["solution_fit_history"][49]
        assert(y < x)
