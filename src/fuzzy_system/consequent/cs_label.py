from src.fuzzy_system.meta import ConsequentMeta


class CSLabel(ConsequentMeta):
    def execute(self, x):
        raise NotImplementedError()
