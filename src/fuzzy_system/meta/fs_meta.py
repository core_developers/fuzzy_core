from functools import partial
from itertools import chain

import numpy as np

from src.fuzzy_system.membership_function import (
    MFGauss, MFBell, MFTriangle, MFTrap)


TERM_MAP = {
    'triangle': MFTriangle,
    'trap': MFTrap,
    'bell': MFBell,
    'gauss': MFGauss
}


class FuzzySystemMeta:
    def __init__(self, metric, term_type='triangle', use_constraints=False):
        self.term_type = term_type
        self.metric = metric
        self.outputs = None
        self.term_set = None
        self.rule_database = None
        self.accepted_features = None
        self.transformer = None
        self.num_term_params = None
        self.A = None
        self.B = None
        self.log = None
        self.use_constraints = use_constraints

    @staticmethod
    def __initialize(algorithm_map, algorithm_struct):
        if not algorithm_struct:
            return

        return algorithm_map.get(
            algorithm_struct['name'])(**algorithm_struct.get('params', {}))

    def get_params(self):
        return np.fromiter(
            chain.from_iterable(item.params for item in self.term_set), float)

    def set_params(self, optimized):
        for index, params in enumerate(
            optimized[i:i + self.num_term_params]
            for i in range(0, len(optimized), self.num_term_params)
        ):
            self.term_set[index].set_params(params)

    def generate_diagonal(self):
        num_terms = len(self.term_set)

        if self.term_type == 'triangle':
            num_rows = num_terms * 2
        elif self.term_type == 'trap':
            num_rows = num_terms * 3
        else:
            return

        num_columns = self.num_term_params * num_terms

        result = np.zeros((num_rows, num_columns))
        for item in zip(
            range(num_rows),
            [
                item
                for item in range(num_columns)
                if item not in range(
                    self.num_term_params - 1,
                    num_columns,
                    self.num_term_params
                )
            ],
        ):
            result[item] = 1

        for item in zip(
            range(num_rows),
            [
                item
                for item in range(num_columns)
                if (item not in range(0, num_columns, self.num_term_params))
            ],
        ):
            result[item] = -1

        return result

    def generate_a(self):
        if self.term_type not in ('triangle', 'trap'):
            return

        return self.generate_diagonal()

    def generate_b(self):
        if self.A is None:
            return

        return np.zeros(self.A.shape[0])

    def transform_data(self, X):
        if self.transformer is not None:
            return self.transformer.transform(X)

        return X

    def _get_outputs(self, y):
        raise NotImplementedError()

    def _generate_rule_database(self, X, y, rule_generator):
        self.rule_database, self.term_set = rule_generator.execute(
            X=X, y=y, term_class=TERM_MAP[self.term_type])

        self.num_term_params = len(self.term_set[0].params)

    def _get_loss(
            self,
            x_test,
            y_test,
            rule_database=None,
            accepted_features=None):
        raise NotImplementedError()

    def _get_accepted_features(self, X, y, feature_selector):
        x_test = self.transform_data(X)

        selector = feature_selector['name'](
            **feature_selector['params'],
            function=partial(self._get_loss, x_test=x_test, y_test=y)
        )

        result = selector.run()
        self.log = result[2]
        self.accepted_features = result[0]

    def select_features(self, X, y, feature_selector):
        self._get_accepted_features(X, y, feature_selector)

    def fit(self, X, y, rule_generator, transformer=None):
        self.transformer = transformer

        if self.transformer is not None:
            X = self.transformer.fit_transform(X)

        self._get_outputs(y)
        self._generate_rule_database(X, y, rule_generator)
        self.accepted_features = np.array([True for _ in range(X.shape[1])])
        if self.use_constraints:
            self.A = self.generate_a()
            self.B = self.generate_b()

    def predict(self, X, rule_database=None, accepted_features=None):
        raise NotImplementedError()

    def optimize_rules(self, x_test, y_test, rule_optimizers=None):
        raise NotImplementedError()
