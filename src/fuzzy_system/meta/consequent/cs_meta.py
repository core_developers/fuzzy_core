class ConsequentMeta:
    def __init__(self, params, n_var):
        self.params = self._generate_params(params)
        # NPerem is n_var
        self.n_var = n_var

    def _generate_params(self, params):
        raise NotImplementedError()

    def execute(self, value):
        raise NotImplementedError()
