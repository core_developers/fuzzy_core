class MembershipFunctionMeta:
    def __init__(self, params, input_variable):
        self.params = self._generate_params(params)
        # КС: входные переменные - массив, не номер
        self.input_variable = input_variable

    def _generate_params(self, params):
        raise NotImplementedError()

    def set_params(self, params):
        self.params = params

    def execute(self, x_vector, weight=None):
        raise NotImplementedError()
