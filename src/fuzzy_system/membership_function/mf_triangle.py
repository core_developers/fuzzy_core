import numpy as np
from numba import jit, prange

from src.fuzzy_system.meta import MembershipFunctionMeta


class MFTriangle(MembershipFunctionMeta):
    def _generate_params(self, params):
        min_value, max_value = params
        return np.array([
            min_value,
            (max_value + min_value) / 2,
            max_value
        ])

    def set_params(self, params):
        self.params = params.copy()
        self.params.sort()

    def execute(self, x_vector, weight=None):
        return execute(x_vector, weight, self.params)


@jit(nopython=True, fastmath=True)
def one(value, params):
    min_value, med_value, max_value = params
    if min_value < value < med_value:
        return (value - min_value) / (med_value - min_value)
    elif med_value < value < max_value:
        return (max_value - value) / (max_value - med_value)
    elif med_value == value:
        return 1

    return 1e-16


@jit(nopython=True, parallel=True, fastmath=True)
def execute(x_vector, weight, params):
    num_params = x_vector.shape[0]
    result = np.empty_like(x_vector)
    for index in prange(num_params):
        result[index] = one(x_vector[index], params)

    return 1 - weight * (1 - result)
