import numpy as np
from numba import jit, prange

from src.fuzzy_system.meta import MembershipFunctionMeta


class MFTrap(MembershipFunctionMeta):
    def _generate_params(self, params):
        min_value, max_value = params

        return np.array([
            min_value,
            min_value + 0.2 * (max_value - min_value),
            min_value + 0.8 * (max_value - min_value),
            max_value
        ])

    def set_params(self, params):
        self.params = params.copy()
        self.params.sort()

    def execute(self, x_vector, weight=None):
        return execute(x_vector, weight, self.params)


@jit(nopython=True, fastmath=True)
def one(value, params):
    bottom_left, top_left, top_right, bottom_right = params

    if top_left < value < top_right:
        return 1
    elif bottom_left < value < top_left:
        return (value - bottom_left) / (top_left - bottom_left)
    elif top_right < value < bottom_right:
        return (bottom_right - value) / (bottom_right - top_right)

    return 1e-16


@jit(nopython=True, parallel=True, fastmath=True)
def execute(x_vector, weight, params):
    num_params = x_vector.shape[0]
    result = np.empty_like(x_vector)
    for index in prange(num_params):
        result[index] = one(x_vector[index], params)

    return 1 - weight * (1 - result)
