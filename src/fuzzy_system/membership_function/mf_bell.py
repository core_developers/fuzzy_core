import numpy as np
from numba import jit, prange

from src.fuzzy_system.meta import MembershipFunctionMeta


class MFBell(MembershipFunctionMeta):
    def _generate_params(self, params):
        min_value, max_value = params

        return np.array([
            (min_value + max_value) / 2,
            (max_value - min_value) / 6,
            ((max_value - min_value) / 6) + 1]
        )

    def execute(self, x_vector, weight=None):
        return execute(x_vector, weight, self.params)


@jit(nopython=True, fastmath=True)
def one(value, params):
    center, width, steep = params

    width = width or 1e-16

    # 1/(1+(|(x-c)/a|)^2*b)
    return 1 / (1 + (abs((value - center) / width)**(2 * steep)))


@jit(nopython=True, parallel=True, fastmath=True)
def execute(x_vector, weight, params):
    num_params = x_vector.shape[0]
    result = np.empty_like(x_vector)
    for index in prange(num_params):
        result[index] = one(x_vector[index], params)

    return 1 - weight * (1 - result)
