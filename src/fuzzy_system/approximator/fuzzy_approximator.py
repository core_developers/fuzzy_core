from functools import partial

import numpy as np
from sklearn import metrics

from src.fuzzy_system.meta.fs_meta import FuzzySystemMeta


class FuzzyApproximator(FuzzySystemMeta):
    def _get_outputs(self, y):
        self.outputs = y.copy()

    def predict(self, X, rule_database=None, accepted_features=None):
        if rule_database is None:
            rule_database = self.rule_database
        if accepted_features is None:
            accepted_features = self.accepted_features

        consequents = np.array([[rule.consequent for rule in rule_database]]).T

        tnorm_array = np.array([
            rule.execute(X, accepted_features)
            for rule in rule_database
        ])

        result = (
            (tnorm_array * consequents).sum(axis=0) / tnorm_array.sum(axis=0))

        return result, tnorm_array

    def get_loss(
            self,
            x_test,
            y_test,
            rule_database=None,
            accepted_features=None):
        return self._get_loss(
            self.transform_data(x_test),
            y_test,
            rule_database,
            accepted_features
        )

    def _get_loss(
            self,
            x_test,
            y_test,
            rule_database=None,
            accepted_features=None):
        y_predicted, _ = self.predict(x_test, rule_database, accepted_features)
        mse = metrics.mean_squared_error(y_test, y_predicted)

        if self.metric == 'mse':
            return mse
        if self.metric == 'rmse':
            return np.sqrt(mse)

    def get_loss_for_optimization(self, params, x_test, y_test):
        if self.A is None or self.B is None:
            self.set_params(params)
            return self._get_loss(x_test, y_test)

        N = (self.A * params).sum(axis=1)
        sum_greater = N[np.greater(N, self.B)].sum()
        if sum_greater > 0:
            return 1 + sum_greater

        self.set_params(params)

        return self._get_loss(x_test, y_test)

    def optimize_rules(self, x_test, y_test, rule_optimizers=None):
        x_test = self.transform_data(x_test)

        result = self.get_params()
        ff = partial(
            self.get_loss_for_optimization, x_test=x_test, y_test=y_test)

        for algorithm, params in rule_optimizers:
            optimizer = algorithm(**params, lb=0.0, ub=1.0, function=ff)
            result = optimizer.run()

        self.log = result[2]
        self.set_params(result[0])
