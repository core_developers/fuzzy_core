import numpy as np

from functools import partial


def ones_like_item(arg):
    return np.ones_like(arg)


class Rule:
    def __init__(self, term_set, consequent):
        self.weight = 1
        self.term_set = term_set
        self.consequent = consequent

    def execute(self, X, accepted_features, metric='product'):
        if not accepted_features.sum():
            result = np.zeros(X.T.shape)
        else:
            funcs = {
                item.input_variable: partial(
                    item.execute, weight=accepted_features[item.input_variable])
                for item in self.term_set
                if accepted_features[item.input_variable]
            }
            result = np.array([
                funcs.get(index, ones_like_item)(item)
                for index, item in enumerate(X.T)
            ])

        if metric == 'product':
            return result.prod(axis=0) * self.weight
