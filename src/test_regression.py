from pathlib import Path

import numpy as np
from joblib import Parallel, delayed
from sklearn import preprocessing

from src.fuzzy_system.approximator.fuzzy_approximator import FuzzyApproximator
from src.optimization_algorithm.swallow_swarm import SwallowSwarm
from src.parser.keel_parser import get_data_from_keel_dir
from src.rules_generator.approximator.kmeans import KMeans
from src.population_generator import uniform


N = 40


def get_all_data_dirs(path):
    for item in Path(path).iterdir():
        if not item.is_dir():
            continue

        yield item


def main(data_path):
    header = 'DataSet;TrainBefore;TestBefore;TrainAfter;TestAfter;Time\n'
    lines = [header]
    for data_dir in get_all_data_dirs(data_path):
        print(data_dir.name)
        result = Parallel(n_jobs=2)(
            delayed(one_cross_validation)(train, test)
            for (train, test, i) in get_data_from_keel_dir(data_dir)
        )
        b_tr, b_tst, a_tr, a_tst, exec_time = map(np.array, zip(*result))
        lines.append(';'.join(
            map(str, [
                data_dir.name,
                round(b_tr.mean(), 2),
                round(b_tst.mean(), 2),
                round(a_tr.mean(), 2),
                round(a_tst.mean(), 2),
                round(exec_time.mean(), 2)
            ])).replace('.', ',') + '\n')
        print(a_tr.mean(), a_tst.mean(), exec_time.mean())

    with Path(f'result.csv').open('w', encoding='utf-8') as f:
        f.writelines(lines)


def one_cross_validation(train, test, term_type='triangle'):
    approximator = FuzzyApproximator(
        metric='rmse', term_type=term_type, use_constraints=False)
    rules_generator = KMeans(n_clusters=8)
    transformer = preprocessing.MinMaxScaler()

    approximator.fit(train.X, train.y, rules_generator, transformer)
    before_train = approximator.get_loss(train.X, train.y)
    before_test = approximator.get_loss(test.X, test.y)

    base_element = approximator.get_params()
    rule_optimizers = [(
        SwallowSwarm, dict(
            population=uniform(
                size=(N, base_element.shape[0]),
                base_element=base_element
            ),
            max_iter=300
        )
    )]

    approximator.optimize_rules(train.X, train.y, rule_optimizers)

    after_train = approximator.get_loss(train.X, train.y)
    after_test = approximator.get_loss(test.X, test.y)
    execution_time = approximator.log['time'][-1]

    return (
        before_train, before_test, after_train, after_test, execution_time)


if __name__ == '__main__':
    # Указать свой путь к данным
    main('../data/regression')
