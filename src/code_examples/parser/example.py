from src.parser.csv_parser import load_csv, get_data_from_csv_dir
from src.parser.excel_parser import load_excel, get_data_from_excel_dir
from src.parser.keel_parser import load_keel, get_data_from_keel_dir

#  загружаем данные из файла по полному пути до файла
data = load_csv('../data/csv/file.csv',
                header=None)  # 'infer' - если есть header
#  аттрибуты данных
print(data.data_name)
print(data.inputs)
print(data.outputs)
print(data.X)
print(data.y)

#  итерируемся по всем данным из каталога
# 'infer' - если есть header
for data in get_data_from_csv_dir('../data/csv', header=None):
    pass

#   или получаем все данные из каталога
data_list = list(get_data_from_csv_dir('../data/csv', header=None))

#  то же самое для excel

data = load_excel('../data/excel/file.xls', header=None)  # 0 - если есть header

# 0 - если есть header
for data in get_data_from_excel_dir('../data/excel', header=None):
    pass

data_list = list(get_data_from_excel_dir('../data/excel', header=None))

# для данных из keel

data = load_keel('../data/keel/file.dat')  # нет параметра header

# при получении данных из каталога на выходе сразу обучающая и тестовая выборки
for train_data, test_data in get_data_from_keel_dir('../data/keel'):
    pass
