from pathlib import Path
from time import time

import numpy as np
from joblib import Parallel, delayed
from sklearn import preprocessing
from sklearn.model_selection import StratifiedKFold

from src.fuzzy_system.classifier.fuzzy_classifier import FuzzyClassifier
from src.optimization_algorithm.lightening_search import (
    LighteningSearchAlgorithm)
from src.parser.keel_parser import get_data_from_keel_dir
from src.rules_generator.classifier import ClassExtremum
from src.population_generator import binary, uniform
from src.feature_selector import LighteningSearch


N = 40


def get_all_data_dirs(path):
    for item in Path(path).iterdir():
        if not item.is_dir():
            continue

        yield item


def make_partitions(X, y, k=0):
    if k < 2:
        return X, y
    X_test = []
    y_test = []
    skf = StratifiedKFold(n_splits=k)

    for _, test_index in skf.split(X, y):
        X_test.append(X[test_index])
        y_test.append(y[test_index])
    return X_test, y_test


def main(data_path):
    header = (
        'DataSet;TrainBefore;TestBefore;TrainAfter;TestAfter;Features;Time\n')
    lines = [header]
    for data_dir in get_all_data_dirs(data_path):
        print(data_dir.name)
        start = time()
        result = Parallel(n_jobs=2)(
            delayed(one_cross_validation)(train, test)
            for train, test, _ in get_data_from_keel_dir(data_dir)
        )
        b_tr, b_tst, a_tr, a_tst, ftr, time_ = map(np.array, zip(*result))
        end = time()
        lines.append(';'.join(map(str, [
            data_dir.name,
            round(b_tr.mean() * 100, 2),
            round(b_tst.mean() * 100, 2),
            round(a_tr.mean() * 100, 2),
            round(a_tst.mean() * 100, 2),
            round(ftr.mean(), 2),
            round(end - start, 2)
        ])).replace('.', ',') + '\n')
        print(a_tr.mean() * 100, a_tst.mean() * 100, ftr.mean(), end - start)

    with Path(f'result.csv').open('w', encoding='utf-8') as f:
        f.writelines(lines)


def one_cross_validation(train, test, term_type='triangle'):
    classifier = FuzzyClassifier(
        metric='accuracy', term_type=term_type, use_constraints=False)
    rules_generator = ClassExtremum()
    transformer = preprocessing.MinMaxScaler()

    classifier.fit(train.X, train.y, rules_generator, transformer)
    before_train = classifier.get_loss(train.X, train.y)
    before_test = classifier.get_loss(test.X, test.y)

    base_element = classifier.get_params()
    alg_name, generator = (LighteningSearch, binary)
    feature_selector = dict(
        name=alg_name,
        params=dict(
            max_iter=300,
            population=generator((N, train.X.shape[1]))
        )
    )
    rule_optimizers = [
        (
            LighteningSearchAlgorithm,
            dict(
                max_iter=300,
                population=uniform(
                    size=(N, base_element.shape[0]),
                    bounds=(0.0, 1.0)
                )
            ),
        )
    ]

    start = time()
    classifier.select_features(train.X, train.y, feature_selector)
    classifier.optimize_rules(train.X, train.y, rule_optimizers)
    end = time()

    num_accepted_features = classifier.accepted_features.sum()

    after_train = classifier.get_loss(train.X, train.y)
    after_test = classifier.get_loss(test.X, test.y)

    return (
        before_train, before_test, after_train, after_test,
        num_accepted_features, end - start
    )


if __name__ == '__main__':
    # Указать свой путь к данным
    main('../data/for_test_selection')
