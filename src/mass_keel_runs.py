from pathlib import Path
import numpy as np
# import pandas as pd
# import matplotlib as mpl
import matplotlib.pyplot as plt
from joblib import Parallel, delayed
from sklearn import preprocessing
# from functools import partial

from src.fuzzy_system.classifier.fuzzy_classifier import FuzzyClassifier
# import src.optimization_algorithm.check_functions as cf

from src.optimization_algorithm.swallow_swarm import SwallowSwarm
from src.optimization_algorithm.bird_swarm import BirdSwarm
from src.optimization_algorithm.gravitational_search import GravitationalSearch
from src.optimization_algorithm.grey_wolf import GreyWolfSwarm
from src.optimization_algorithm.particle_swarm import ParticleSwarm
from src.optimization_algorithm.cat_swarm import CatSwarm
from src.optimization_algorithm.bat_swarm import BatSwarm
from src.optimization_algorithm.artificial_bee import ArtificialBeeSwarm
from src.optimization_algorithm.cuckoo_search import CuckooSearch
from src.optimization_algorithm.lightning_search import LightningSearch

from src.parser.keel_parser import get_data_from_keel_dir
# from src.feature_selector import (
#    ParticleSwarm, ParticleSwarmSigmoid, SwallowSwarm, SwallowSwarmSigmoid)
from src.rules_generator.classifier import ClassExtremum
from src.population_generator import uniform


def get_all_data_dirs(path):
    for item in Path(path).iterdir():
        if not item.is_dir():
            continue
        yield item


N = 40
iters = 300


def main(data_path):
    header = ('DataSet;TrainBefore;TestBefore;TrainAfter;'
              'TestAfter;StdLearn;StdTest;Fcount;Time\n')

    rule_optimizers = [
        ('Swallow Swarm', [(
            SwallowSwarm, dict(population=None, max_iter=iters))]),
        ('Bird Swarm', [(BirdSwarm, dict(population=None, max_iter=iters))]),
        ('Gravitational Search', [(
            GravitationalSearch, dict(population=None, max_iter=iters), None)]),
        ('Grey Wolf', [(GreyWolfSwarm, dict(population=None, max_iter=iters))]),
        ('Particle Swarm', [(
            ParticleSwarm, dict(population=None, max_iter=iters))]),
        ('Cat Swarm', [(CatSwarm, dict(population=None, max_iter=iters))]),
        ('Bat Swarm', [(BatSwarm, dict(population=None, max_iter=iters))]),
        ('Artificial Bee', [(
            ArtificialBeeSwarm, dict(population=None, max_iter=iters))]),
        ('Cuckoo Search', [(
            CuckooSearch, dict(population=None, max_iter=iters))]),
        ('Lightning Search', [(
            LightningSearch, dict(population=None, max_iter=iters))])
    ]
    for data_dir in get_all_data_dirs(data_path):
        lines = [header]
        lines2 = []

        base_rules = []

        for (train, test, i) in get_data_from_keel_dir(data_dir):
            classifier = FuzzyClassifier(
                metric='accuracy', term_type='triangle', use_constraints=False)
            rules_generator = ClassExtremum()
            transformer = preprocessing.MinMaxScaler()
            classifier.fit(train.X, train.y, rules_generator, transformer)
            result = classifier.get_params()
            base_rules.append(
                uniform(size=(N, result.shape[0]), base_element=result))
        atimes = []
        agbests = []
        afvals = []
        aiters = []
        for alg in rule_optimizers:
            print(data_dir.name + " " + alg[0] + " started")
            # for (train, test, i) in get_data_from_keel_dir(data_dir):
            #     result = (one_cross_validation)(train, test, alg[1],
            #                                     base_rules[i])
            result = []
            for _ in range(10):
                temp = []
                temp = Parallel(n_jobs=10)(
                    delayed(one_cross_validation)(
                        train, test, alg[1], base_rules[i])
                    for (train, test, i) in get_data_from_keel_dir(data_dir))
                for i in range(len(temp)):
                    result.append(temp[i])
            b_tr, b_tst, a_tr, a_tst, fcount, time, gbest = map(
                np.array, zip(*result))
            lines.append(';'.join(
                map(str, [
                    alg[0],
                    round(b_tr.mean() * 100, 2),
                    round(b_tst.mean() * 100, 2),
                    round(a_tr.mean() * 100, 2),
                    round(a_tst.mean() * 100, 2),
                    round((a_tr * 100).std(), 2),
                    round((a_tst * 100).std(), 2),
                    round(fcount[:, -1].mean(), 2),
                    round(time[:, -1].mean(), 2),
                ])).replace('.', ',') + '\n')

            atimes.append(time.mean(axis=0))
            afvals.append(fcount.mean(axis=0))
            agbests.append(gbest.mean(axis=0) * 100)
            aiters.append(np.arange(0, iters + 1))

            lines2.append(alg[0] + '\n')
            lines2.append(
                'ff;' + ';'.join(map(str, agbests[-1])).replace('.', ',') + '\n'
            )
            lines2.append(
                'time;' + ';'.join(map(str, atimes[-1])).replace('.', ',') +
                '\n'
            )
            lines2.append(
                'fcts;' + ';'.join(map(str, afvals[-1])).replace('.', ',') +
                '\n'
            )
            lines2.append(
                'iter;' + ';'.join(map(str, aiters[-1])).replace('.', ',') +
                '\n'
            )

            print(
                data_dir.name + " " + alg[0] + " finished: ",
                b_tst.mean() * 100,
                a_tst.mean() * 100
            )

        with Path(
                data_dir.name + '.csv').open('w', encoding='utf-8') as f:
            f.writelines(lines)

        with Path(data_dir.name + '_dynamics.csv').open(
                'w', encoding='utf-8') as f:
            f.writelines(lines2)

        plot_and_save_result(
            data_dir.name, atimes, agbests, rule_optimizers, 'time')
        plot_and_save_result(
            data_dir.name, afvals, agbests, rule_optimizers, 'fcount')
        plot_and_save_result(
            data_dir.name, aiters, agbests, rule_optimizers, 'iters')


def plot_and_save_result(data_dir_name, x, y, algs, name):
    plt.rcParams["figure.figsize"] = 18, 10
    plt.rcParams["figure.dpi"] = 160
    fig, (ax, lax) = plt.subplots(
        ncols=2, gridspec_kw={"width_ratios": [1000, 1]})

    for i in range(len(algs)):
        ax.plot(x[i], y[i], linewidth=4, label=algs[i][0])

    ax.set_ylabel('E, %', fontsize=12)
    label = 'time, s'
    if name == 'fcount':
        label = 'number of fitness evaluations'
    elif name == 'iters':
        label = 'iteration'
    ax.set_xlabel(label, fontsize=12)
    ax.tick_params(axis='both', which='both', labelsize=12)
    h, la = ax.get_legend_handles_labels()
    lax.legend(h, la, borderaxespad=0, prop={'size': 20})
    lax.axis("off")

    plt.tight_layout()
    s = data_dir_name + '_' + name

    ax.tick_params(axis='y', which='both', rotation=0)
    ax.set_xscale('log')
    ax.set_yscale('linear')
    plt.savefig(s + '_logx.png')

    ax.tick_params(axis='y', which='both', rotation=0)
    ax.set_xscale('linear')
    ax.set_yscale('linear')
    ax.set_xlim(left=0)
    plt.savefig(s + '.png')
    plt.clf()
    plt.close("all")


def one_cross_validation(
        train,
        test,
        rule_optimizers,
        base_rules=None,
        term_type='triangle'):
    classifier = FuzzyClassifier(term_type=term_type, use_constraints=False)
    rule_optimizers[0][1]['population'] = base_rules.copy()
    rule_optimizers = [(rule_optimizers[0][0], rule_optimizers[0][1])]
    rules_generator = ClassExtremum()
    transformer = preprocessing.MinMaxScaler()

    classifier.fit(train.X, train.y, rules_generator, transformer)

    before_train = classifier.get_loss(train.X, train.y)
    before_test = classifier.get_loss(test.X, test.y)
    classifier.optimize_rules(train.X, train.y, rule_optimizers)
    after_train = classifier.get_loss(train.X, train.y)
    after_test = classifier.get_loss(test.X, test.y)

    return (
        before_train, before_test, after_train, after_test,
        classifier.log["fcounter"], classifier.log["time"],
        classifier.log["gbest_f"]
    )


if __name__ == '__main__':
    # Указать свой путь к данным
    main('../data/for_test')
