import numpy as np

from src.fuzzy_system.rule.rule import Rule
from sklearn.cluster import KMeans as KMeans_


class KMeans:
    # refer to sci-kit documentation for params description
    def __init__(
            self,
            n_clusters=8,
            init='k-means++',
            n_init=10,
            max_iter=300,
            tol=0.0001,
            verbose=0,
            random_state=None,
            copy_x=True,
            algorithm='auto'):
        self.kwargs = dict(
            n_clusters=n_clusters, init=init, n_init=n_init, max_iter=max_iter,
            tol=tol, verbose=verbose, random_state=random_state, copy_x=copy_x,
            algorithm=algorithm
        )
        self.parameters = list()
        self.consqs = list()

    def get_max_min_for_clusters(self, X, y):
        y_ = y.copy()

        if len(y.shape) == 1:
            y_.shape = (y_.shape[0], 1)

        km = KMeans_(**self.kwargs).fit(np.concatenate([X, y_], axis=1))
        clusters = {
            i: X[np.where(km.labels_ == i)[0]]
            for i in range(km.n_clusters)
        }
        labels = {
            i: km.cluster_centers_[i, -1]
            for i in range(km.n_clusters)
        }

        for i in range(km.n_clusters):
            cluster = clusters[i]
            label = labels[i]
            minimums = cluster.min(axis=0)
            maximums = cluster.max(axis=0)
            min_maxes = zip(minimums, maximums)
            self.parameters.append(min_maxes)
            self.consqs.append(label)

    def execute(self, X, y, term_class):
        self.get_max_min_for_clusters(X, y)

        rules = []
        terms = []

        for i in range(len(self.parameters)):
            term_set = []
            for index, params in enumerate(self.parameters[i]):
                term_set.append(term_class(params, index))

            rules.append(Rule(term_set, self.consqs[i]))
            terms += term_set

        return rules, terms
