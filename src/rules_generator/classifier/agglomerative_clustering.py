import numpy as np

from src.fuzzy_system.rule.rule import Rule
from sklearn.cluster import AgglomerativeClustering as AgglomerativeClustering_


class AgglomerativeClustering:
    # refer to sci-kit documentation for params description
    def __init__(
            self,
            n_clusters=2,
            affinity='euclidean',
            memory=None,
            connectivity=None,
            compute_full_tree='auto',
            linkage='ward',
            distance_threshold=None):
        self.args = [
            n_clusters, affinity, memory, connectivity, compute_full_tree,
            linkage, distance_threshold
        ]
        self.parameters = list()
        self.consqs = list()

    def get_max_min_for_clusters(self, X, y):

        ap = AgglomerativeClustering_(*self.args).fit(X)
        clusters = {
            i: X[np.where(ap.labels_ == i)[0]]
            for i in range(ap.n_clusters)
        }
        labels = {
            i: y[np.where(ap.labels_ == i)[0]]
            for i in range(ap.n_clusters)
        }

        for i in range(ap.n_clusters):
            cluster = clusters[i]
            label = np.ndarray.tolist(labels[i])
            minimums = cluster.min(axis=0)
            maximums = cluster.max(axis=0)
            min_maxes = zip(minimums, maximums)
            self.parameters.append(min_maxes)
            self.consqs.append(np.argmax(np.bincount(label)))

    def execute(self, X, y, term_class):
        self.get_max_min_for_clusters(X, y)

        rules = []
        terms = []

        for i in range(len(self.parameters)):
            term_set = []
            for index, params in enumerate(self.parameters[i]):
                term_set.append(term_class(params, index))

            rules.append(Rule(term_set, self.consqs[i]))
            terms += term_set

        return rules, terms
