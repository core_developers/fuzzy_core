import numpy as np

from src.fuzzy_system.rule.rule import Rule
from sklearn.cluster import AffinityPropagation as AffinityPropagation_


class AffinityPropagation:
    # refer to sci-kit documentation for params description
    def __init__(
            self,
            damping=0.5,
            max_iter=200,
            convergence_iter=15,
            copy=True,
            preference=None,
            affinity='euclidean',
            verbose=False):
        self.args = [
            damping, max_iter, convergence_iter, copy, preference, affinity,
            verbose
        ]
        self.parameters = list()
        self.consqs = list()

    def get_max_min_for_clusters(self, X, y):

        ap = AffinityPropagation_(*self.args).fit(X)
        ap.n_clusters = len(ap.cluster_centers_)
        clusters = {
            i: X[np.where(ap.labels_ == i)[0]]
            for i in range(ap.n_clusters)
        }
        labels = {
            i: y[np.where(ap.labels_ == i)[0]]
            for i in range(ap.n_clusters)
        }

        for i in range(ap.n_clusters):
            cluster = clusters[i]
            label = np.ndarray.tolist(labels[i])
            minimums = cluster.min(axis=0)
            maximums = cluster.max(axis=0)
            min_maxes = zip(minimums, maximums)
            self.parameters.append(min_maxes)
            self.consqs.append(np.argmax(np.bincount(label)))

    def execute(self, X, y, term_class):
        self.get_max_min_for_clusters(X, y)

        rules = []
        terms = []

        for i in range(len(self.parameters)):
            term_set = []
            for index, params in enumerate(self.parameters[i]):
                term_set.append(term_class(params, index))

            rules.append(Rule(term_set, self.consqs[i]))
            terms += term_set

        return rules, terms
