import numpy as np

from src.fuzzy_system.rule.rule import Rule
from sklearn.cluster import KMeans as KMeans_


class KMeans:
    # refer to sci-kit documentation for params description
    def __init__(
            self,
            n_clusters=8,
            init='k-means++',
            n_init=10,
            max_iter=300,
            tol=0.0001,
            precompute_distances='auto',
            verbose=0,
            random_state=None,
            copy_x=True,
            n_jobs=-1,
            algorithm='auto'):
        self.args = [
            n_clusters, init, n_init, max_iter, tol, precompute_distances,
            verbose, random_state, copy_x, n_jobs, algorithm
        ]
        self.parameters = list()
        self.consqs = list()

    def get_max_min_for_clusters(self, X, y):

        km = KMeans_(*self.args).fit(X)
        clusters = {
            i: X[np.where(km.labels_ == i)[0]]
            for i in range(km.n_clusters)
        }
        labels = {
            i: y[np.where(km.labels_ == i)[0]]
            for i in range(km.n_clusters)
        }

        for i in range(km.n_clusters):
            cluster = clusters[i]
            label = np.ndarray.tolist(labels[i])
            minimums = cluster.min(axis=0)
            maximums = cluster.max(axis=0)
            min_maxes = zip(minimums, maximums)
            self.parameters.append(min_maxes)
            self.consqs.append(np.argmax(np.bincount(label)))

    def execute(self, X, y, term_class):
        self.get_max_min_for_clusters(X, y)

        rules = []
        terms = []

        for i in range(len(self.parameters)):
            term_set = []
            for index, params in enumerate(self.parameters[i]):
                term_set.append(term_class(params, index))

            rules.append(Rule(term_set, self.consqs[i]))
            terms += term_set

        return rules, terms
