import numpy as np

from src.fuzzy_system.rule.rule import Rule
from sklearn.cluster import Birch as Birch_


class Birch:
    # refer to sci-kit documentation for params description
    def __init__(
            self,
            threshold=0.1,
            branching_factor=50,
            n_clusters=30,
            compute_labels=True,
            copy=True):
        self.args = [
            threshold, branching_factor, n_clusters, compute_labels, copy
        ]
        self.parameters = list()
        self.consqs = list()

    def get_max_min_for_clusters(self, X, y):

        ap = Birch_(*self.args).fit(X)
        clusters = {
            i: X[np.where(ap.labels_ == i)[0]]
            for i in range(ap.n_clusters)
        }
        labels = {
            i: y[np.where(ap.labels_ == i)[0]]
            for i in range(ap.n_clusters)
        }

        for i in range(ap.n_clusters):
            cluster = clusters[i]
            label = np.ndarray.tolist(labels[i])
            minimums = cluster.min(axis=0)
            maximums = cluster.max(axis=0)
            min_maxes = zip(minimums, maximums)
            self.parameters.append(min_maxes)
            self.consqs.append(np.argmax(np.bincount(label)))

    def execute(self, X, y, term_class):
        self.get_max_min_for_clusters(X, y)

        rules = []
        terms = []

        for i in range(len(self.parameters)):
            term_set = []
            for index, params in enumerate(self.parameters[i]):
                term_set.append(term_class(params, index))

            rules.append(Rule(term_set, self.consqs[i]))
            terms += term_set

        return rules, terms
