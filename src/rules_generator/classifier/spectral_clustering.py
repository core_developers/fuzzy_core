import numpy as np

from src.fuzzy_system.rule.rule import Rule
from sklearn.cluster import SpectralClustering as SpectralClustering_
from scipy import spatial


class SpectralClustering:
    # refer to sci-kit documentation for params description
    def __init__(
            self,
            n_clusters=8,
            eigen_solver=None,
            n_components=None,
            random_state=None,
            n_init=10,
            gamma=1.0,
            affinity='rbf',
            n_neighbors=10,
            eigen_tol=0.0,
            assign_labels='kmeans',
            degree=3,
            coef0=1,
            kernel_params=None,
            n_jobs=None):
        self.args = [
            n_clusters, eigen_solver, n_components, random_state, n_init,
            gamma, affinity, n_neighbors, eigen_tol, assign_labels, degree,
            coef0, kernel_params, n_jobs
        ]
        self.parameters = list()
        self.consqs = list()

    def get_max_min_for_clusters(self, X, y):

        ap = SpectralClustering_(*self.args).fit(X)
        clusters = {
            i: X[np.where(ap.labels_ == i)[0]]
            for i in range(ap.n_clusters)
        }
        labels = {
            i: y[np.where(ap.labels_ == i)[0]]
            for i in range(ap.n_clusters)
        }

        for i in range(ap.n_clusters):
            cluster = clusters[i]
            label = np.ndarray.tolist(labels[i])
            minimums = cluster.min(axis=0)
            maximums = cluster.max(axis=0)
            min_maxes = zip(minimums, maximums)
            self.parameters.append(min_maxes)
            self.consqs.append(np.argmax(np.bincount(label)))

    def execute(self, X, y, term_class):
        self.get_max_min_for_clusters(X, y)

        rules = []
        terms = []

        for i in range(len(self.parameters)):
            term_set = []
            for index, params in enumerate(self.parameters[i]):
                term_set.append(term_class(params, index))

            rules.append(Rule(term_set, self.consqs[i]))
            terms += term_set

        return rules, terms
