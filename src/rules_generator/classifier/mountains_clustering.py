# Алгоритм Mountain Method
# Статья: Stephen L. Chiu (1994): Fuzzy MODEL IDENTIFICATION
# BASED ON CLUSTER ESTIMATION, in Journal of Intelligent and
# Fuzzy Systems, DOI: 10.3233/IFS-1994-2306

import numpy as np

from src.fuzzy_system.rule.rule import Rule
from scipy import spatial


class MountainsClustering:
    def __init__(self, ra=0.15, Eup=0.5, Edown=0.15):
        """
        :param ra: neighbourhood radius, set higher
        to get more rules (default value is 0.15)
        :param Eup: upper bound of gray area, set
        lower to get more rules (default value is 0.5)
        :param Edown: lower limits for plot axes, set
        higher to get more rules (default value is 0.15)
        """

        self.ra = ra
        self.Eup = Eup
        self.Edown = Edown
        self.rb = 1.5 * ra
        self.parameters = list()
        self.consqs = list()

    def get_max_min_for_clusters(self, X, y):
        distance_matrix = spatial.distance.cdist(X, X, 'euclidean')
        size = len(X)
        cluster_center = []
        potential = [0.0] * size
        for i in range(size):
            potential[i] = np.sum(
                np.exp(-4.0 * ((distance_matrix[i, :] / self.ra)**2)))
        c1 = np.argmax(potential)
        p1 = potential[c1]
        cluster_center.append(X[c1])
        potential -= p1 * np.exp(
            -4.0 * (distance_matrix[c1, :]**2) / (self.rb**2))
        criteria = True
        while criteria:
            k = np.argmax(potential)
            if potential[k] > (p1 * self.Eup):
                cluster_center.append(X[k])
                potential -= potential[k] * np.exp(
                    -4.0 * (distance_matrix[k, :]**2) / (self.rb**2))
            else:
                if potential[k] < (p1 * self.Edown):
                    criteria = False
                else:
                    xk = [X[k]]
                    dmin = min(spatial.distance.cdist(
                        cluster_center, xk, 'euclidean'))
                    if (dmin / self.ra + potential[k] / p1) >= 1:
                        cluster_center.append(X[k])
                        potential -= potential[k] * np.exp(
                            -4.0 * (distance_matrix[k, :]**2) / (self.rb**2))
                    else:
                        potential[k] = 0

        d = spatial.distance.cdist(X, cluster_center, 'euclidean')
        clustered_data = [np.argmin(d[i, :]) for i in range(size)]
        clusters = {i: [] for i in range(len(cluster_center))}
        labels = {i: [] for i in range(len(cluster_center))}
        for i in range(size):
            clusters[clustered_data[i]].append(X[i])
            labels[clustered_data[i]].append(y[i])

        for i in range(len(cluster_center)):
            cluster = np.asarray(clusters[i], dtype=np.float64)
            label = labels[i]
            minimums = cluster.min(axis=0)
            maximums = cluster.max(axis=0)
            min_maxes = zip(minimums, maximums)
            self.parameters.append(min_maxes)
            self.consqs.append(np.argmax(np.bincount(label)))

    def execute(self, X, y, term_class):
        self.get_max_min_for_clusters(X, y)

        rules = []
        terms = []

        for i in range(len(self.parameters)):
            term_set = []
            for index, params in enumerate(self.parameters[i]):
                term_set.append(term_class(params, index))

            rules.append(Rule(term_set, self.consqs[i]))
            terms += term_set

        return rules, terms
