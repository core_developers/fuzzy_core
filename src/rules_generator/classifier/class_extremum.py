import numpy as np

from src.fuzzy_system.rule.rule import Rule


class ClassExtremum:
    def __init__(self):
        self.parameters = dict()

    def get_max_min_for_classes(self, X, y):
        classes = np.unique(y)
        for label in classes:
            X_i = X[y == label, :]
            minimums = X_i.min(axis=0)
            maximums = X_i.max(axis=0)
            min_maxes = np.array(list(zip(minimums, maximums)))
            self.parameters[label] = min_maxes

    def execute(self, X, y, term_class):
        self.get_max_min_for_classes(X, y)

        rules = []
        terms = []

        for class_, input_values in self.parameters.items():
            term_set = []
            for index, params in enumerate(input_values):
                term_set.append(term_class(params, index))

            rules.append(Rule(term_set, class_))
            terms += term_set

        return rules, terms
