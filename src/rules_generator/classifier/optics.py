import numpy as np

from src.fuzzy_system.rule.rule import Rule
from sklearn.cluster import OPTICS as OPTICS_


class OPTICS:
    # refer to sci-kit documentation for params description
    def __init__(
            self,
            min_samples=5,
            max_eps=np.inf,
            metric='minkowski',
            p=2,
            metric_params=None,
            cluster_method='xi',
            eps=None,
            xi=0.05,
            predecessor_correction=True,
            min_cluster_size=None,
            algorithm='auto',
            leaf_size=30,
            n_jobs=None):
        self.args = [
            min_samples, max_eps, metric, p, metric_params, cluster_method,
            eps, xi, predecessor_correction, min_cluster_size, algorithm,
            leaf_size, n_jobs
        ]
        self.parameters = list()
        self.consqs = list()

    def get_max_min_for_clusters(self, X, y):

        ap = OPTICS_(*self.args).fit(X)
        ap.n_clusters = ap.labels_.max() + 1
        clusters = {
            i: X[np.where(ap.labels_ == i)[0]]
            for i in range(ap.n_clusters)
        }
        labels = {
            i: y[np.where(ap.labels_ == i)[0]]
            for i in range(ap.n_clusters)
        }

        for i in range(ap.n_clusters):
            cluster = clusters[i]
            label = np.ndarray.tolist(labels[i])
            minimums = cluster.min(axis=0)
            maximums = cluster.max(axis=0)
            min_maxes = zip(minimums, maximums)
            self.parameters.append(min_maxes)
            self.consqs.append(np.argmax(np.bincount(label)))

    def execute(self, X, y, term_class):
        self.get_max_min_for_clusters(X, y)

        rules = []
        terms = []

        for i in range(len(self.parameters)):
            term_set = []
            for index, params in enumerate(self.parameters[i]):
                term_set.append(term_class(params, index))

            rules.append(Rule(term_set, self.consqs[i]))
            terms += term_set

        return rules, terms
