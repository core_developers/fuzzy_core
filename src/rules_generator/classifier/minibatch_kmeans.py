import numpy as np

from src.fuzzy_system.rule.rule import Rule
from sklearn.cluster import MiniBatchKMeans as MBKmeans


class MiniBatchKMeans:
    # refer to sci-kit documentation for params description
    def __init__(
            self,
            n_clusters=8,
            init='k-means++',
            max_iter=100,
            batch_size=100,
            verbose=0,
            compute_labels=True,
            random_state=None,
            tol=0.0,
            max_no_improvement=10,
            init_size=None,
            n_init=3,
            reassignment_ratio=0.01):
        self.args = [
            n_clusters, init, max_iter, batch_size, verbose, compute_labels,
            random_state, tol, max_no_improvement, init_size, n_init,
            reassignment_ratio
        ]
        self.parameters = list()
        self.consqs = list()

    def get_max_min_for_clusters(self, X, y):

        km = MBKmeans(*self.args).fit(X)
        clusters = {
            i: X[np.where(km.labels_ == i)[0]]
            for i in range(km.n_clusters)
        }
        labels = {
            i: y[np.where(km.labels_ == i)[0]]
            for i in range(km.n_clusters)
        }

        for i in range(km.n_clusters):
            cluster = clusters[i]
            label = np.ndarray.tolist(labels[i])
            minimums = cluster.min(axis=0)
            maximums = cluster.max(axis=0)
            min_maxes = zip(minimums, maximums)
            self.parameters.append(min_maxes)
            self.consqs.append(np.argmax(np.bincount(label)))

    def execute(self, X, y, term_class):
        self.get_max_min_for_clusters(X, y)

        rules = []
        terms = []

        for i in range(len(self.parameters)):
            term_set = []
            for index, params in enumerate(self.parameters[i]):
                term_set.append(term_class(params, index))

            rules.append(Rule(term_set, self.consqs[i]))
            terms += term_set

        return rules, terms
