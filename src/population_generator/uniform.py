import numpy as np


def uniform(size, bounds=None, base_element=None):
    if base_element is not None:
        result = base_element * np.random.uniform(0.5, 2, size)
        result[0] = base_element

        return result

    elif bounds is not None:
        lb, ub = bounds

        return np.random.uniform(lb, ub, size)
    else:
        return
