import numpy as np


def binary(size):
    result = np.random.randint(0, 2, size)
    result[0] = np.ones(size[1])

    return result


def fast_random_bool(size):
    n = np.prod(size)
    nb = -(-n // 8)     # ceiling division
    b = np.frombuffer(np.random.bytes(nb), np.uint8, nb)

    return np.unpackbits(b)[:n].reshape(size).view(np.bool)


def generator_random_bool(size):
    result = fast_random_bool(size)
    result[0] = np.array([True for _ in range(result[0].shape[0])])

    return result
