from pathlib import Path

import numpy as np
from joblib import Parallel, delayed
from sklearn import preprocessing

from src.fuzzy_system.classifier.fuzzy_classifier import FuzzyClassifier
from src.optimization_algorithm.swallow_swarm import SwallowSwarm
from src.parser.keel_parser import get_data_from_keel_dir
from src.rules_generator.classifier import ClassExtremum
from src.population_generator import uniform

N = 40


def get_all_data_dirs(path):
    for item in Path(path).iterdir():
        if not item.is_dir():
            continue

        yield item


def main(data_path):
    header = 'DataSet;TrainBefore;TestBefore;TrainAfter;TestAfter;Time\n'
    lines = [header]
    for data_dir in get_all_data_dirs(data_path):
        print(data_dir.name)
        # for (train, test, i) in get_data_from_keel_dir(data_dir):
        #    result = one_cross_validation(train, test)
        result = Parallel(n_jobs=2)(
            delayed(one_cross_validation)(train, test)
            for (train, test, i) in get_data_from_keel_dir(data_dir))
        b_tr, b_tst, a_tr, a_tst, exec_time = map(np.array, zip(*result))
        lines.append(';'.join(
            map(str, [
                data_dir.name,
                round(b_tr.mean() * 100, 2),
                round(b_tst.mean() * 100, 2),
                round(a_tr.mean() * 100, 2),
                round(a_tst.mean() * 100, 2),
                round(exec_time.mean(), 2)
            ])).replace('.', ',') + '\n')
        print(a_tr.mean() * 100, a_tst.mean() * 100, exec_time.mean())

    with Path(f'result.csv').open('w', encoding='utf-8') as f:
        f.writelines(lines)


def one_cross_validation(train, test, term_type='triangle'):
    classifier = FuzzyClassifier(
        metric='accuracy', term_type=term_type, use_constraints=False)
    rules_generator = ClassExtremum()
    transformer = preprocessing.MinMaxScaler()

    classifier.fit(train.X, train.y, rules_generator, transformer)
    before_train = classifier.get_loss(train.X, train.y)
    before_test = classifier.get_loss(test.X, test.y)

    base_element = classifier.get_params()
    rule_optimizers = [(
        SwallowSwarm, dict(
            population=uniform(
                size=(N, base_element.shape[0]),
                base_element=base_element
            ),
            max_iter=300
        )
    )]

    classifier.optimize_rules(train.X, train.y, rule_optimizers)

    after_train = classifier.get_loss(train.X, train.y)
    after_test = classifier.get_loss(test.X, test.y)
    execution_time = classifier.log['time'][-1]

    return (
        before_train, before_test, after_train, after_test, execution_time)


if __name__ == '__main__':
    # Указать свой путь к данным
    main('../data/for_test_selection')
