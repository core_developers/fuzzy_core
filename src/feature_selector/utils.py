import numpy as np


def fast_random_bool(size):
    n = np.prod(size)
    nb = -(-n // 8)  # ceiling division
    b = np.frombuffer(np.random.bytes(nb), np.uint8, nb)

    return np.unpackbits(b)[:n].reshape(size).view(np.bool)


def merge_one(x, y):
    if x == y:
        return x

    return np.random.randint(0, 2)


def merge_one_h(x, y):
    if x is y:
        return x

    return fast_random_bool(1)[0]


def merge(x_vector, y_vector):
    return np.array([merge_one(x, y) for x, y in zip(x_vector, y_vector)])


def merge_h(x_vector, y_vector):
    return np.array([merge_one_h(x, y) for x, y in zip(x_vector, y_vector)])


def _sigmoid(x):
    return 1 / (1 + np.exp(-x))
   
    
def _tanh(x):
    return np.fabs(np.tanh(x))


def _v(x):
    return np.fabs(np.array(x)/np.sqrt(1+np.power(x,2)))


def compute_binary_position(dimension, velocity):
    return (np.random.random_sample(size=dimension) < _sigmoid(velocity)) * 1
    
    
def compute_binary_position_tanh(dimension, velocity):
    return (np.random.random_sample(size=dimension) < _tanh(velocity)) * 1


def compute_binary_position_v(dimension, velocity):
    return (np.random.random_sample(size=dimension) < _v(velocity)) * 1
