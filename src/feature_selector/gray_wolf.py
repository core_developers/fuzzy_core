import numpy as np

from src.optimization_algorithm.meta import OptimizationAlgorithmMeta
from src.population_generator.binary import fast_random_bool
from .utils import (
    merge, compute_binary_position, compute_binary_position_tanh,
    compute_binary_position_v)


class GrayWolfMerge(OptimizationAlgorithmMeta):
    def __init__(
            self,
            population,
            function,
            max_iter,
            lb=None,
            ub=None,
            no_change_iteration=None):
        super().__init__(
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration,
            feature_selection=True
        )

        self.velocity = np.zeros((self.n, self.dimension))

    def _one_iter(self):
        self.alpha, self.beta, self.delta = self._get_abd()
        result = []
        for item in self._agents:
            Dalpha = merge(self.alpha, item)
            Dbeta = merge(self.beta, item)
            Ddelta = merge(self.delta, item)

            X1 = merge(self.alpha, Dalpha)
            X2 = merge(self.beta, Dbeta)
            X3 = merge(self.delta, Ddelta)

            result.append(merge(merge(X1, X2), X3))

        self._agents = np.array(result)
        self._fitness = [self.ff(x) for x in self._agents]

    def _get_abd(self):
        result = []
        for i in range(3):
            result.append(self._agents[self._sorted_fitness_args[i]])
        return result


class GrayWolfOperations(OptimizationAlgorithmMeta):
    def __init__(
            self,
            population,
            function,
            max_iter,
            lb=None,
            ub=None,
            no_change_iteration=None):
        super().__init__(
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration,
            feature_selection=True
        )

        self.velocity = np.zeros((self.n, self.dimension))

    def _one_iter(self):
        self.alpha, self.beta, self.delta = self._get_abd()

        r1 = fast_random_bool((self.n, self.dimension))
        r2 = fast_random_bool((self.n, self.dimension))
        r3 = fast_random_bool((self.n, self.dimension))

        Dalpha = r1 * (self.alpha ^ self._agents)
        Dbeta = r2 * (self.beta ^ self._agents)
        Ddelta = r3 * (self.delta ^ self._agents)

        X1 = self.alpha ^ Dalpha
        X2 = self.beta ^ Dbeta
        X3 = self.delta ^ Ddelta

        self._agents = X1 + X2 + X3
        self._fitness = [self.ff(x) for x in self._agents]

    def _get_abd(self):
        result = []
        for i in range(3):
            result.append(self._agents[self._sorted_fitness_args[i]])
        return result


class GrayWolfOperationsMerge(OptimizationAlgorithmMeta):
    def __init__(
            self,
            population,
            function,
            max_iter,
            lb=None,
            ub=None,
            no_change_iteration=None):
        super().__init__(
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration,
            feature_selection=True)

        self.velocity = np.zeros((self.n, self.dimension))

    def _one_iter(self):
        self.alpha, self.beta, self.delta = self._get_abd()
        r1 = fast_random_bool((self.n, self.dimension))
        r2 = fast_random_bool((self.n, self.dimension))
        r3 = fast_random_bool((self.n, self.dimension))

        Dalpha = r1 * (self.alpha ^ self._agents)
        Dbeta = r2 * (self.beta ^ self._agents)
        Ddelta = r3 * (self.delta ^ self._agents)

        X1 = self.alpha ^ Dalpha
        X2 = self.beta ^ Dbeta
        X3 = self.delta ^ Ddelta

        result = []
        for a, b, c in zip(X1, X2, X3):
            result.append(merge(merge(a, b), c))

        self._agents = np.array(result)
        self._fitness = [self.ff(x) for x in self._agents]

    def _get_abd(self):
        result = []
        for i in range(3):
            result.append(self._agents[self._sorted_fitness_args[i]])
        return result


class GrayWolfSigmoid(OptimizationAlgorithmMeta):
    def __init__(
            self,
            population,
            function,
            max_iter,
            lb=None,
            ub=None,
            no_change_iteration=None):
        super().__init__(
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration,
            feature_selection=True
        )

        self.velocity = np.zeros((self.n, self.dimension))

    def _one_iter(self):
        self.alpha, self.beta, self.delta = self._get_abd()

        a = 2 - 2 * self.current_iter / self.max_iter

        r1 = np.random.random((self.n, self.dimension))
        r2 = np.random.random((self.n, self.dimension))
        A1 = 2 * r1 * a - a
        C1 = 2 * r2

        r1 = np.random.random((self.n, self.dimension))
        r2 = np.random.random((self.n, self.dimension))
        A2 = 2 * r1 * a - a
        C2 = 2 * r2

        r1 = np.random.random((self.n, self.dimension))
        r2 = np.random.random((self.n, self.dimension))
        A3 = 2 * r1 * a - a
        C3 = 2 * r2

        Dalpha = abs(C1 * self.alpha - self._agents)
        Dbeta = abs(C2 * self.beta - self._agents)
        Ddelta = abs(C3 * self.delta - self._agents)

        X1 = self.alpha - A1 * Dalpha
        X2 = self.beta - A2 * Dbeta
        X3 = self.delta - A3 * Ddelta

        self._agents = compute_binary_position(
            self.dimension, (X1 + X2 + X3) / 3)
        self._fitness = [self.ff(x) for x in self._agents]

    def _get_abd(self):
        result = []
        for i in range(3):
            result.append(self._agents[self._sorted_fitness_args[i]])
        return result


class GrayWolfContinuous(OptimizationAlgorithmMeta):
    def __init__(
            self,
            population,
            function,
            max_iter,
            lb=None,
            ub=None,
            no_change_iteration=None):
        super().__init__(
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration,
            feature_selection=True
        )

        self.velocity = np.zeros((self.n, self.dimension))

    def _one_iter(self):
        self.alpha, self.beta, self.delta = self._get_abd()

        a = 2 - 2 * self.current_iter / self.max_iter

        r1 = np.random.random((self.n, self.dimension))
        r2 = np.random.random((self.n, self.dimension))
        A1 = 2 * r1 * a - a
        C1 = 2 * r2

        r1 = np.random.random((self.n, self.dimension))
        r2 = np.random.random((self.n, self.dimension))
        A2 = 2 * r1 * a - a
        C2 = 2 * r2

        r1 = np.random.random((self.n, self.dimension))
        r2 = np.random.random((self.n, self.dimension))
        A3 = 2 * r1 * a - a
        C3 = 2 * r2

        Dalpha = abs(C1 * self.alpha - self._agents)
        Dbeta = abs(C2 * self.beta - self._agents)
        Ddelta = abs(C3 * self.delta - self._agents)

        X1 = self.alpha - A1 * Dalpha
        X2 = self.beta - A2 * Dbeta
        X3 = self.delta - A3 * Ddelta

        self._agents = self.force_bounds((X1 + X2 + X3) / 3)
        self._fitness = [self.ff(x) for x in self._agents]

    def _get_abd(self):
        result = []
        for i in range(3):
            result.append(self._agents[self._sorted_fitness_args[i]])
        return result


class GrayWolfTanh(OptimizationAlgorithmMeta):
    def __init__(
            self,
            population,
            function,
            max_iter,
            lb=None,
            ub=None,
            no_change_iteration=None):
        super().__init__(
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration,
            feature_selection=True
        )

        self.velocity = np.zeros((self.n, self.dimension))

    def _one_iter(self):
        self.alpha, self.beta, self.delta = self._get_abd()

        a = 2 - 2 * self.current_iter / self.max_iter

        r1 = np.random.random((self.n, self.dimension))
        r2 = np.random.random((self.n, self.dimension))
        A1 = 2 * r1 * a - a
        C1 = 2 * r2

        r1 = np.random.random((self.n, self.dimension))
        r2 = np.random.random((self.n, self.dimension))
        A2 = 2 * r1 * a - a
        C2 = 2 * r2

        r1 = np.random.random((self.n, self.dimension))
        r2 = np.random.random((self.n, self.dimension))
        A3 = 2 * r1 * a - a
        C3 = 2 * r2

        Dalpha = abs(C1 * self.alpha - self._agents)
        Dbeta = abs(C2 * self.beta - self._agents)
        Ddelta = abs(C3 * self.delta - self._agents)

        X1 = self.alpha - A1 * Dalpha
        X2 = self.beta - A2 * Dbeta
        X3 = self.delta - A3 * Ddelta

        self._agents = compute_binary_position_tanh(
            self.dimension, (X1 + X2 + X3) / 3)
        self._fitness = [self.ff(x) for x in self._agents]

    def _get_abd(self):
        result = []
        for i in range(3):
            result.append(self._agents[self._sorted_fitness_args[i]])
        return result


class GrayWolfV(OptimizationAlgorithmMeta):
    def __init__(
            self,
            population,
            function,
            max_iter,
            lb=None,
            ub=None,
            no_change_iteration=None):
        super().__init__(
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration,
            feature_selection=True
        )

        self.velocity = np.zeros((self.n, self.dimension))

    def _one_iter(self):
        self.alpha, self.beta, self.delta = self._get_abd()

        a = 2 - 2 * self.current_iter / self.max_iter

        r1 = np.random.random((self.n, self.dimension))
        r2 = np.random.random((self.n, self.dimension))
        A1 = 2 * r1 * a - a
        C1 = 2 * r2

        r1 = np.random.random((self.n, self.dimension))
        r2 = np.random.random((self.n, self.dimension))
        A2 = 2 * r1 * a - a
        C2 = 2 * r2

        r1 = np.random.random((self.n, self.dimension))
        r2 = np.random.random((self.n, self.dimension))
        A3 = 2 * r1 * a - a
        C3 = 2 * r2

        Dalpha = abs(C1 * self.alpha - self._agents)
        Dbeta = abs(C2 * self.beta - self._agents)
        Ddelta = abs(C3 * self.delta - self._agents)

        X1 = self.alpha - A1 * Dalpha
        X2 = self.beta - A2 * Dbeta
        X3 = self.delta - A3 * Ddelta

        self._agents = compute_binary_position_v(
            self.dimension, (X1 + X2 + X3) / 3)
        self._fitness = [self.ff(x) for x in self._agents]

    def _get_abd(self):
        result = []
        for i in range(3):
            result.append(self._agents[self._sorted_fitness_args[i]])
        return result