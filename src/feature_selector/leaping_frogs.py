from src.optimization_algorithm.meta import OptimizationAlgorithmMeta
import numpy as np
import math
from src.population_generator.binary import fast_random_bool
from .utils import (
    merge, merge_h, compute_binary_position, compute_binary_position_tanh,
    compute_binary_position_v)

class LeapingFrogsMerge(OptimizationAlgorithmMeta):

    def __init__(
            self,
            population,
            function,
            max_iter,
            lb=None,
            ub=None,
            no_change_iteration=None,
            n_mem=4,
            local_it=5,
            const=1.2):
        super().__init__(
            population, function, lb, ub, max_iter // local_it,
            no_change_iteration, feature_selection=True)

        self.n_mem = n_mem
        self.n_agents_in_mem = self.n // n_mem
        self.best_positions = {tuple(item): item for item in self._agents}
        self.const = const
        self.local_it = local_it

    def sort_agents(self):
        order = self._sorted_fitness_args
        self._agents = self._agents[order]
        self._fitness = np.array(self._fitness)[order.tolist()]

    #def new_vector(self, x, y):
    #    new_agent = np.random.random() * self.const * (x - y) + y
    #    return new_agent

    def local_search(self):
        for number_mem in range(self.n_mem):
            f = 1
            for local_iterations in range(self.local_it):
                index = self.n - f * self.n_mem + number_mem

                nv = merge(self._agents[number_mem],
                                     self._agents[index])

                nv_fval = self.ff(nv)

                if nv_fval < self._fitness[index]:
                    self._agents[index] = nv
                    self._fitness[index] = nv_fval
                    f = f + 1
                else:
                    nv = merge(self._agents[0], self._agents[index])
                    nv_fval = self.ff(nv)
                    if nv_fval < self._fitness[index]:
                        self._agents[index] = nv
                        self._fitness[index] = nv_fval
                        f = f + 1
                    else:
                        self._agents[index] = fast_random_bool(self.dimension)
                        self._fitness[index] = self.ff(self._agents[index])
                if f > math.ceil(self.n_agents_in_mem / 2):
                    f = 1

    def _one_iter(self):
        self.sort_agents()
        self.local_search()


class LeapingFrogsOperations(OptimizationAlgorithmMeta):

    def __init__(
            self,
            population,
            function,
            max_iter,
            lb=None,
            ub=None,
            no_change_iteration=None,
            n_mem=4,
            local_it=5,
            const=1.2):
        super().__init__(
            population, function, lb, ub, max_iter // local_it,
            no_change_iteration, feature_selection=True)

        self.n_mem = n_mem
        self.n_agents_in_mem = self.n // n_mem
        self.best_positions = {tuple(item): item for item in self._agents}
        self.const = const
        self.local_it = local_it

    def sort_agents(self):
        order = self._sorted_fitness_args
        self._agents = self._agents[order]
        self._fitness = np.array(self._fitness)[order.tolist()]

    def new_vector(self, x, y):
        r = fast_random_bool(self.dimension)
        new_agent = r * np.logical_xor(x, y) + y
        return new_agent

    def local_search(self):
        for number_mem in range(self.n_mem):
            f = 1
            for local_iterations in range(self.local_it):
                index = self.n - f * self.n_mem + number_mem

                nv = self.new_vector(self._agents[number_mem],
                                     self._agents[index])

                nv_fval = self.ff(nv)

                if nv_fval < self._fitness[index]:
                    self._agents[index] = nv
                    self._fitness[index] = nv_fval
                    f = f + 1
                else:
                    nv = self.new_vector(self._agents[0], self._agents[index])
                    nv_fval = self.ff(nv)
                    if nv_fval < self._fitness[index]:
                        self._agents[index] = nv
                        self._fitness[index] = nv_fval
                        f = f + 1
                    else:
                        self._agents[index] = fast_random_bool(self.dimension)
                        self._fitness[index] = self.ff(self._agents[index])
                if f > math.ceil(self.n_agents_in_mem / 2):
                    f = 1

    def _one_iter(self):
        self.sort_agents()
        self.local_search()


class LeapingFrogsOperationsMerge(OptimizationAlgorithmMeta):

    def __init__(
            self,
            population,
            function,
            max_iter,
            lb=None,
            ub=None,
            no_change_iteration=None,
            n_mem=4,
            local_it=5,
            const=1.2):
        super().__init__(
            population, function, lb, ub, max_iter // local_it,
            no_change_iteration, feature_selection=True)

        self.n_mem = n_mem
        self.n_agents_in_mem = self.n // n_mem
        self.best_positions = {tuple(item): item for item in self._agents}
        self.const = const
        self.local_it = local_it

    def sort_agents(self):
        order = self._sorted_fitness_args
        self._agents = self._agents[order]
        self._fitness = np.array(self._fitness)[order.tolist()]

    def new_vector(self, x, y):
        r = fast_random_bool(self.dimension)
        new_agent = r * np.logical_xor(x, y)
        new_agent = merge_h(new_agent, y)
        return new_agent

    def local_search(self):
        for number_mem in range(self.n_mem):
            f = 1
            for local_iterations in range(self.local_it):
                index = self.n - f * self.n_mem + number_mem

                nv = self.new_vector(self._agents[number_mem],
                                     self._agents[index])

                nv_fval = self.ff(nv)

                if nv_fval < self._fitness[index]:
                    self._agents[index] = nv
                    self._fitness[index] = nv_fval
                    f = f + 1
                else:
                    nv = self.new_vector(self._agents[0], self._agents[index])
                    nv_fval = self.ff(nv)
                    if nv_fval < self._fitness[index]:
                        self._agents[index] = nv
                        self._fitness[index] = nv_fval
                        f = f + 1
                    else:
                        self._agents[index] = fast_random_bool(self.dimension)
                        self._fitness[index] = self.ff(self._agents[index])
                if f > math.ceil(self.n_agents_in_mem / 2):
                    f = 1

    def _one_iter(self):
        self.sort_agents()
        self.local_search()


class LeapingFrogsContinuous(OptimizationAlgorithmMeta):

    def __init__(
            self,
            population,
            function,
            max_iter,
            lb=None,
            ub=None,
            no_change_iteration=None,
            n_mem=4,
            local_it=5,
            const=1.2):
        super().__init__(
            population, function, lb, ub, max_iter // local_it,
            no_change_iteration, feature_selection=True)

        self.n_mem = n_mem
        self.n_agents_in_mem = self.n // n_mem
        self.best_positions = {tuple(item): item for item in self._agents}
        self.const = const
        self.local_it = local_it

    def sort_agents(self):
        order = self._sorted_fitness_args
        self._agents = self._agents[order]
        self._fitness = np.array(self._fitness)[order.tolist()]

    def new_vector(self, x, y):
        new_agent = np.random.random() * self.const * (x - y) + y
        return new_agent

    def local_search(self):
        for number_mem in range(self.n_mem):
            f = 1
            for local_iterations in range(self.local_it):
                index = self.n - f * self.n_mem + number_mem

                nv = self.new_vector(self._agents[number_mem],
                                     self._agents[index])

                nv_fval = self.ff(nv)

                if nv_fval < self._fitness[index]:
                    self._agents[index] = nv
                    self._fitness[index] = nv_fval
                    f = f + 1
                else:
                    nv = self.new_vector(self._agents[0], self._agents[index])
                    nv_fval = self.ff(nv)
                    if nv_fval < self._fitness[index]:
                        self._agents[index] = nv
                        self._fitness[index] = nv_fval
                        f = f + 1
                    else:
                        self._agents[index] = (
                            self._agents[0] *
                            np.random.uniform(0, 2, self.dimension)
                        )
                        self._fitness[index] = self.ff(self._agents[index])
                if f > math.ceil(self.n_agents_in_mem / 2):
                    f = 1

    def _one_iter(self):
        self.sort_agents()
        self.local_search()


class LeapingFrogsTanh(OptimizationAlgorithmMeta):

    def __init__(
            self,
            population,
            function,
            max_iter,
            lb=None,
            ub=None,
            no_change_iteration=None,
            n_mem=4,
            local_it=5,
            const=1.2):
        super().__init__(
            population, function, lb, ub, max_iter // local_it,
            no_change_iteration, feature_selection=True)

        self.n_mem = n_mem
        self.n_agents_in_mem = self.n // n_mem
        self.best_positions = {tuple(item): item for item in self._agents}
        self.const = const
        self.local_it = local_it

    def sort_agents(self):
        order = self._sorted_fitness_args
        self._agents = self._agents[order]
        self._fitness = np.array(self._fitness)[order.tolist()]

    def new_vector(self, x, y):
        new_agent = np.random.random() * self.const * (x - y) + y
        return new_agent

    def calculate_vll(self, worst, local_leader, global_leader):
        r1 = np.random.uniform(0, 1, self.dimension)
        r2 = np.random.uniform(0, 1, self.dimension)

        return (
            (global_leader - worst) * r1 +
            (local_leader - worst) * r2
        )

    def local_search(self):
        for number_mem in range(self.n_mem):
            f = 1
            for local_iterations in range(self.local_it):
                index = self.n - f * self.n_mem + number_mem

                v = self.calculate_vll(self._agents[index], self._agents[number_mem], self._agents[0])

                new_position = compute_binary_position_tanh(self.dimension, v)

                nv_fval = self.ff(new_position)

                if nv_fval < self._fitness[index]:
                    self._agents[index] = new_position
                    self._fitness[index] = nv_fval
                    f = f + 1
                else:
                    self._agents[index] = fast_random_bool(self.dimension)
                    self._fitness[index] = self.ff(self._agents[index])
                if f > math.ceil(self.n_agents_in_mem / 2):
                    f = 1

    def _one_iter(self):
        self.sort_agents()
        self.local_search()


class LeapingFrogsV(OptimizationAlgorithmMeta):

    def __init__(
            self,
            population,
            function,
            max_iter,
            lb=None,
            ub=None,
            no_change_iteration=None,
            n_mem=4,
            local_it=5,
            const=1.2):
        super().__init__(
            population, function, lb, ub, max_iter // local_it,
            no_change_iteration, feature_selection=True)

        self.n_mem = n_mem
        self.n_agents_in_mem = self.n // n_mem
        self.best_positions = {tuple(item): item for item in self._agents}
        self.const = const
        self.local_it = local_it

    def sort_agents(self):
        order = self._sorted_fitness_args
        self._agents = self._agents[order]
        self._fitness = np.array(self._fitness)[order.tolist()]

    def new_vector(self, x, y):
        new_agent = np.random.random() * self.const * (x - y) + y
        return new_agent

    def calculate_vll(self, worst, local_leader, global_leader):
        r1 = np.random.uniform(0, 1, self.dimension)
        r2 = np.random.uniform(0, 1, self.dimension)

        return (
            (global_leader - worst) * r1 +
            (local_leader - worst) * r2
        )

    def local_search(self):
        for number_mem in range(self.n_mem):
            f = 1
            for local_iterations in range(self.local_it):
                index = self.n - f * self.n_mem + number_mem

                v = self.calculate_vll(self._agents[index], self._agents[number_mem], self._agents[0])

                new_position = compute_binary_position_v(self.dimension, v)

                nv_fval = self.ff(new_position)

                if nv_fval < self._fitness[index]:
                    self._agents[index] = new_position
                    self._fitness[index] = nv_fval
                    f = f + 1
                else:
                    self._agents[index] = fast_random_bool(self.dimension)
                    self._fitness[index] = self.ff(self._agents[index])
                if f > math.ceil(self.n_agents_in_mem / 2):
                    f = 1

    def _one_iter(self):
        self.sort_agents()
        self.local_search()


class LeapingFrogsSigm(OptimizationAlgorithmMeta):

    def __init__(
            self,
            population,
            function,
            max_iter,
            lb=None,
            ub=None,
            no_change_iteration=None,
            n_mem=4,
            local_it=5,
            const=1.2):
        super().__init__(
            population, function, lb, ub, max_iter // local_it,
            no_change_iteration, feature_selection=True)

        self.n_mem = n_mem
        self.n_agents_in_mem = self.n // n_mem
        self.best_positions = {tuple(item): item for item in self._agents}
        self.const = const
        self.local_it = local_it

    def sort_agents(self):
        order = self._sorted_fitness_args
        self._agents = self._agents[order]
        self._fitness = np.array(self._fitness)[order.tolist()]

    def new_vector(self, x, y):
        new_agent = np.random.random() * self.const * (x - y) + y
        return new_agent

    def calculate_vll(self, worst, local_leader, global_leader):
        r1 = np.random.uniform(0, 1, self.dimension)
        r2 = np.random.uniform(0, 1, self.dimension)

        return (
            (global_leader - worst) * r1 +
            (local_leader - worst) * r2
        )

    def local_search(self):
        for number_mem in range(self.n_mem):
            f = 1
            for local_iterations in range(self.local_it):
                index = self.n - f * self.n_mem + number_mem

                v = self.calculate_vll(self._agents[index], self._agents[number_mem], self._agents[0])

                new_position = compute_binary_position(self.dimension, v)

                nv_fval = self.ff(new_position)

                if nv_fval < self._fitness[index]:
                    self._agents[index] = new_position
                    self._fitness[index] = nv_fval
                    f = f + 1
                else:
                    self._agents[index] = fast_random_bool(self.dimension)
                    self._fitness[index] = self.ff(self._agents[index])
                if f > math.ceil(self.n_agents_in_mem / 2):
                    f = 1

    def _one_iter(self):
        self.sort_agents()
        self.local_search()