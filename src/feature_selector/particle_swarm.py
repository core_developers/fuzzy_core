import numpy as np

from src.optimization_algorithm.meta import OptimizationAlgorithmMeta
from src.population_generator.binary import fast_random_bool
from .utils import (
    merge, compute_binary_position, merge_h, compute_binary_position_tanh,
    compute_binary_position_v)


class ParticleSwarmMerge(OptimizationAlgorithmMeta):
    def __init__(
            self,
            population,
            function,
            max_iter,
            no_change_iteration=None,
            lb=0,
            ub=1,
            w=0.5,
            c1=1,
            c2=1):
        super().__init__(
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration,
            feature_selection=True)

        self.w = w
        self.c1 = c1
        self.c2 = c2
        self.velocity = np.zeros((self.n, self.dimension))

    def _one_iter(self):
        self._agents = np.array([
            merge(merge(self._gbest, item), merge(self._pbest, item))
            for item in self._agents
        ])
        self._fitness = [self.ff(x) for x in self._agents]


class ParticleSwarmOperations(OptimizationAlgorithmMeta):
    def __init__(
            self,
            population,
            function,
            max_iter,
            no_change_iteration=None,
            lb=0,
            ub=1,
            w=0.5,
            c1=1,
            c2=1):

        super().__init__(
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration,
            feature_selection=True)

        self.w = w
        self.c1 = c1
        self.c2 = c2
        self.velocity = fast_random_bool((self.n, self.dimension))

    def _one_iter(self):
        r1 = fast_random_bool((self.n, self.dimension))
        r2 = fast_random_bool((self.n, self.dimension))

        self.velocity = (
            self.velocity + r1 * (self._pbest ^ self._agents) +
            r2 * (self._gbest ^ self._agents)
        )

        self._agents = self._agents + self.velocity
        self._fitness = [self.ff(x) for x in self._agents]


class ParticleSwarmOperationsMerge(OptimizationAlgorithmMeta):
    def __init__(
            self,
            population,
            function,
            max_iter,
            no_change_iteration=None,
            lb=0,
            ub=1,
            w=0.5,
            c1=1,
            c2=1):
        super().__init__(
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration,
            feature_selection=True)

        self.w = w
        self.c1 = c1
        self.c2 = c2
        self.velocity = fast_random_bool((self.n, self.dimension))

    def _one_iter(self):
        r1 = fast_random_bool((self.n, self.dimension))
        r2 = fast_random_bool((self.n, self.dimension))

        result = []
        for index, item in enumerate(self._agents):
            self.velocity[index] = merge_h(
                self.velocity[index],
                merge_h(
                    r1[index] * (self._pbest ^ item),
                    r2[index] * (self._gbest ^ item)
                )
            )

            result.append(merge_h(item, self.velocity[index]))

        self._agents = np.array(result)
        self._fitness = [self.ff(x) for x in self._agents]


class ParticleSwarmSigmoid(OptimizationAlgorithmMeta):
    def __init__(
            self,
            population,
            function,
            max_iter,
            no_change_iteration=None,
            lb=0,
            ub=1,
            w=0.5,
            c1=1,
            c2=1):
        super().__init__(
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration,
            feature_selection=True)

        self.w = w
        self.c1 = c1
        self.c2 = c2
        self.velocity = np.zeros((self.n, self.dimension))

    def _one_iter(self):
        r1 = np.random.random((self.n, self.dimension))
        r2 = np.random.random((self.n, self.dimension))

        self.velocity = (
            self.w * self.velocity +
            self.c1 * r1 * (self._pbest - self._agents) +
            self.c2 * r2 * (self._gbest - self._agents)
        )

        self._agents = compute_binary_position(self.dimension, self.velocity)
        self._fitness = [self.ff(x) for x in self._agents]


class ParticleSwarmContinuous(OptimizationAlgorithmMeta):
    def __init__(
            self,
            population,
            function,
            max_iter,
            no_change_iteration=None,
            lb=0,
            ub=1,
            w=0.5,
            c1=1,
            c2=1):
        super().__init__(
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration,
            feature_selection=True)

        self.w = w
        self.c1 = c1
        self.c2 = c2
        self.velocity = np.zeros((self.n, self.dimension))

    def _one_iter(self):
        r1 = np.random.random((self.n, self.dimension))
        r2 = np.random.random((self.n, self.dimension))

        self.velocity = (
            self.w * self.velocity +
            self.c1 * r1 * (self._pbest - self._agents) +
            self.c2 * r2 * (self._gbest - self._agents)
        )

        self._agents = self.force_bounds(self._agents + self.velocity)
        self._fitness = [self.ff(x) for x in self._agents]
        
        
class ParticleSwarmTanh(OptimizationAlgorithmMeta):
    def __init__(
            self,
            population,
            function,
            max_iter,
            no_change_iteration=None,
            lb=0,
            ub=1,
            w=0.5,
            c1=1,
            c2=1):
        super().__init__(
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration,
            feature_selection=True)

        self.w = w
        self.c1 = c1
        self.c2 = c2
        self.velocity = np.zeros((self.n, self.dimension))

    def _one_iter(self):
        r1 = np.random.random((self.n, self.dimension))
        r2 = np.random.random((self.n, self.dimension))

        self.velocity = (
            self.w * self.velocity +
            self.c1 * r1 * (self._pbest - self._agents) +
            self.c2 * r2 * (self._gbest - self._agents)
        )

        self._agents = compute_binary_position_tanh(self.dimension, self.velocity)
        self._fitness = [self.ff(x) for x in self._agents]


class ParticleSwarmV(OptimizationAlgorithmMeta):
    def __init__(
            self,
            population,
            function,
            max_iter,
            no_change_iteration=None,
            lb=0,
            ub=1,
            w=0.5,
            c1=1,
            c2=1):
        super().__init__(
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration,
            feature_selection=True)

        self.w = w
        self.c1 = c1
        self.c2 = c2
        self.velocity = np.zeros((self.n, self.dimension))

    def _one_iter(self):
        r1 = np.random.random((self.n, self.dimension))
        r2 = np.random.random((self.n, self.dimension))

        self.velocity = (
            self.w * self.velocity +
            self.c1 * r1 * (self._pbest - self._agents) +
            self.c2 * r2 * (self._gbest - self._agents)
        )

        self._agents = compute_binary_position_v(self.dimension, self.velocity)
        self._fitness = [self.ff(x) for x in self._agents]
