import numpy as np

from src.optimization_algorithm.meta import OptimizationAlgorithmMeta


class RandomSearch(OptimizationAlgorithmMeta):
    def __init__(
        self,
        function,
        population,
        max_iter,
        no_change_iteration=None
    ):
        super().__init__(
            population, function, None, None, max_iter, no_change_iteration,
            feature_selection=True
        )

    def _one_iter(self):
        self._set_agents(np.random.randint(0, 2, self._agents.shape))
