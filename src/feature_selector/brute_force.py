import numpy as np

from src.optimization_algorithm.meta import OptimizationAlgorithmMeta


class BruteForce(OptimizationAlgorithmMeta):
    """
    Алгоритм отбора методом полного перебора.
    """
    def __init__(
        self,
        function,
        max_iter,
        population,
        no_change_iteration=None
    ):
        super().__init__(
            population, function, None, None, 1, no_change_iteration,
            feature_selection=True
        )
        self.dimension = population.shape[1]
        self.best_vector = np.zeros(self.dimension, dtype=int)
        self.best_error = 1

    def change_vector(self, vector, index):
        """
        Рекурсивная функция, которая изменяет 1 из элементов бинарного вектора и рассчитывает от него значение
        фитесс-функции.
        :param vector: Изменяемый вектор
        :param index: Индекс, на место которого будет поставлена 1
        """
        if index < 0:
            return
        vector1 = vector.copy()
        vector1[index] = 1
        error = self.ff(vector1)
        if error < self.best_error:
            self.best_error = error
            self.best_vector = vector1.copy()
        index -= 1
        self.change_vector(vector, index)
        self.change_vector(vector1, index)

    def _one_iter(self):
        # Начинаем поиск с вектора полностью заполненного нулями.
        vector = np.zeros(self.dimension, dtype=int)
        self.change_vector(vector, self.dimension-1)
        # print(self.best_vector, self.best_error)
        self._set_agents(np.array([self.best_vector]))
