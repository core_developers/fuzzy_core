import numpy as np
from src.optimization_algorithm.meta import OptimizationAlgorithmMeta
from .utils import (
    compute_binary_position, compute_binary_position_tanh,
    compute_binary_position_v)


class GravitationalSearchSigmoid(OptimizationAlgorithmMeta):
    def __init__(
            self,
            population,
            function,
            max_iter,
            lb=None,
            ub=None,
            no_change_iteration=None,
            G0=10,
            alpha=10,
            kbest=10):
        super().__init__(
            population, function, lb, ub, max_iter, no_change_iteration,
            feature_selection=True
        )

        self.kbest = kbest
        self.kbest_agents = np.zeros(self.n)
        self.G0 = G0
        self.G = G0
        self.alpha = alpha
        self.eps = np.finfo(float).eps
        self.M = np.zeros(self.n)
        self.acc = np.zeros((self.n, self.dimension))
        self.vel = np.zeros((self.n, self.dimension))
        self.K = kbest

    def _masses(self):
        Amax = self._pworst_f
        Amin = self._pbest_f
        if Amax == Amin:
            self.M = np.ones(self.n)  # случай, когда популяция сошлась
        else:
            self.M = (np.array(self._fitness) - Amax) / (Amin - Amax)
        Msum = sum(self.M)
        self.M /= Msum

        return self.M

    def _gconstant(self):
        self.G = self.G0 * np.exp(
            -self.alpha * (self.current_iter - 1.0) / self.max_iter)

        return self.G

    def _gfield(self):
        if self.K > self.n:
            raise ValueError()
        else:
            T = np.zeros((self.n, self.dimension))
            r = np.random.random(size=(self.K, self.n))
            Mt = self.M[self._sorted_fitness_args[:self.K]]
            for i in range(int(self.K)):
                T1 = (
                    np.tile(
                        self._agents[self._sorted_fitness_args[i]],
                        (self.n, 1)
                    ) - self._agents
                )
                T2 = np.linalg.norm(T1, axis=1, keepdims=True) + self.eps
                T += T1 / T2.flatten()[:, None] * r[i][:, None] * Mt[i] * self.G
            self.acc = T

        return self.acc

    def _velocity(self):
        self.vel = np.random.random(
            size=(self.n, self.dimension)) * self.vel + self.acc

    def _transform(self):
        self._agents = compute_binary_position(
            self.dimension, self.vel)
        self._fitness = [self.ff(x) for x in self._agents]

    def _one_iter(self):
        self._masses()
        self._gconstant()
        self._gfield()
        self._velocity()
        self._transform()


class GravitationalSearchTanh(OptimizationAlgorithmMeta):
    def __init__(
            self,
            population,
            function,
            max_iter,
            lb=None,
            ub=None,
            no_change_iteration=None,
            G0=10,
            alpha=10,
            kbest=10):
        super().__init__(
            population, function, lb, ub, max_iter, no_change_iteration,
            feature_selection=True
        )

        self.kbest = kbest
        self.kbest_agents = np.zeros(self.n)
        self.G0 = G0
        self.G = G0
        self.alpha = alpha
        self.eps = np.finfo(float).eps
        self.M = np.zeros(self.n)
        self.acc = np.zeros((self.n, self.dimension))
        self.vel = np.zeros((self.n, self.dimension))
        self.K = kbest

    def _masses(self):
        Amax = self._pworst_f
        Amin = self._pbest_f
        if Amax == Amin:
            self.M = np.ones(self.n)  # случай, когда популяция сошлась
        else:
            self.M = (np.array(self._fitness) - Amax) / (Amin - Amax)
        Msum = sum(self.M)
        self.M /= Msum

        return self.M

    def _gconstant(self):
        self.G = self.G0 * np.exp(
            -self.alpha * (self.current_iter - 1.0) / self.max_iter)

        return self.G

    def _gfield(self):
        if self.K > self.n:
            raise ValueError()
        else:
            T = np.zeros((self.n, self.dimension))
            r = np.random.random(size=(self.K, self.n))
            Mt = self.M[self._sorted_fitness_args[:self.K]]
            for i in range(int(self.K)):
                T1 = (
                    np.tile(
                        self._agents[self._sorted_fitness_args[i]],
                        (self.n, 1)
                    ) - self._agents
                )
                T2 = np.linalg.norm(T1, axis=1, keepdims=True) + self.eps
                T += T1 / T2.flatten()[:, None] * r[i][:, None] * Mt[i] * self.G
            self.acc = T

        return self.acc

    def _velocity(self):
        self.vel = np.random.random(
            size=(self.n, self.dimension)) * self.vel + self.acc

    def _transform(self):
        self._agents = compute_binary_position_tanh(
            self.dimension, self.vel)
        self._fitness = [self.ff(x) for x in self._agents]

    def _one_iter(self):
        self._masses()
        self._gconstant()
        self._gfield()
        self._velocity()
        self._transform()


class GravitationalSearchV(OptimizationAlgorithmMeta):
    def __init__(
            self,
            population,
            function,
            max_iter,
            lb=None,
            ub=None,
            no_change_iteration=None,
            G0=10,
            alpha=10,
            kbest=10):
        super().__init__(
            population, function, lb, ub, max_iter, no_change_iteration,
            feature_selection=True
        )

        self.kbest = kbest
        self.kbest_agents = np.zeros(self.n)
        self.G0 = G0
        self.G = G0
        self.alpha = alpha
        self.eps = np.finfo(float).eps
        self.M = np.zeros(self.n)
        self.acc = np.zeros((self.n, self.dimension))
        self.vel = np.zeros((self.n, self.dimension))
        self.K = kbest

    def _masses(self):
        Amax = self._pworst_f
        Amin = self._pbest_f
        if Amax == Amin:
            self.M = np.ones(self.n)  # случай, когда популяция сошлась
        else:
            self.M = (np.array(self._fitness) - Amax) / (Amin - Amax)
        Msum = sum(self.M)
        self.M /= Msum

        return self.M

    def _gconstant(self):
        self.G = self.G0 * np.exp(
            -self.alpha * (self.current_iter - 1.0) / self.max_iter)

        return self.G

    def _gfield(self):
        if self.K > self.n:
            raise ValueError()
        else:
            T = np.zeros((self.n, self.dimension))
            r = np.random.random(size=(self.K, self.n))
            Mt = self.M[self._sorted_fitness_args[:self.K]]
            for i in range(int(self.K)):
                T1 = (
                    np.tile(
                        self._agents[self._sorted_fitness_args[i]],
                        (self.n, 1)
                    ) - self._agents
                )
                T2 = np.linalg.norm(T1, axis=1, keepdims=True) + self.eps
                T += T1 / T2.flatten()[:, None] * r[i][:, None] * Mt[i] * self.G
            self.acc = T

        return self.acc

    def _velocity(self):
        self.vel = np.random.random(
            size=(self.n, self.dimension)) * self.vel + self.acc

    def _transform(self):
        self._agents = compute_binary_position_v(
            self.dimension, self.vel)
        self._fitness = [self.ff(x) for x in self._agents]

    def _one_iter(self):
        self._masses()
        self._gconstant()
        self._gfield()
        self._velocity()
        self._transform()


class GravitationalSearchContinuous(OptimizationAlgorithmMeta):
    def __init__(
            self,
            population,
            function,
            max_iter,
            lb=None,
            ub=None,
            no_change_iteration=None,
            G0=10,
            alpha=10,
            kbest=10):
        super().__init__(
            population, function, lb, ub, max_iter, no_change_iteration,
            feature_selection=True
        )

        self.kbest = kbest
        self.kbest_agents = np.zeros(self.n)
        self.G0 = G0
        self.G = G0
        self.alpha = alpha
        self.eps = np.finfo(float).eps
        self.M = np.zeros(self.n)
        self.acc = np.zeros((self.n, self.dimension))
        self.vel = np.zeros((self.n, self.dimension))
        self.K = kbest

    def _masses(self):
        Amax = self._pworst_f
        Amin = self._pbest_f
        if Amax == Amin:
            self.M = np.ones(self.n)  # случай, когда популяция сошлась
        else:
            self.M = (self._fitness - Amax) / (Amin - Amax)
        Msum = sum(self.M)
        self.M /= Msum

        return self.M

    def _gconstant(self):
        self.G = self.G0 * np.exp(
            -self.alpha * (self.current_iter - 1.0) / self.max_iter)

        return self.G

    def _gfield(self):
        if self.K > self.n:
            raise ValueError()
        else:
            T = np.zeros((self.n, self.dimension))
            r = np.random.random(size=(self.K, self.n))
            Mt = self.M[self._sorted_fitness_args[:self.K]]
            for i in range(int(self.K)):
                T1 = (
                    np.tile(
                        self._agents[self._sorted_fitness_args[i]],
                        (self.n, 1)
                    ) - self._agents
                )
                T2 = np.linalg.norm(T1, axis=1, keepdims=True) + self.eps
                T += T1 / T2.flatten()[:, None] * r[i][:, None] * Mt[i] * self.G
            self.acc = T

        return self.acc

    def _move_vector(self):
        self.vel = np.random.random(
            size=(self.n, self.dimension)) * self.vel + self.acc
        result = self._agents + self.vel
        self._set_agents(result, update_sort=False)

    def _one_iter(self):
        self._masses()
        self._gconstant()
        self._gfield()
        self._move_vector()
