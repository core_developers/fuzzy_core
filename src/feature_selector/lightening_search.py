import random
import math
import numpy as np
from src.optimization_algorithm.meta import OptimizationAlgorithmMeta


class LighteningSearch(OptimizationAlgorithmMeta):
    def __init__(
            self,
            population,
            function,
            max_iter,
            no_change_iteration=None,
            lb=None,
            ub=None,
            max_channel_time=5,
            mu=0,
            sigma=1):
        """
        n - размер популяции
        function - функция, минимум которой требуется найти
        lb - нижняя граница
        ub - верхняя граница
        dimension - размерность
        iteration - количество итераций
        max_channel_time - количество проходов, через которое худший канал будет отброшен, а энергия перераспределена к лидеру
        mu - параметр нормального распределения (мат. ожидание)
        sigma - параметр нормального распределения (среднеквадратическое отклонение)
        """
        super().__init__(
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration,
            feature_selection=True)

        self.maxChannelTime = max_channel_time
        self.mu = mu
        self.sigma = sigma

        self.forkProbability = 0.5
        self.channelTime = 0
        self.iter_count = 0

        self._fitness = np.array([
            self.ff(item)
            for item in self._agents
        ])

    def fork(self, pos_new, i):
        """
        Расщепление канала. Будет найдена симметричная позиция и выбрана та, в которой функция имеет лучшее значение.
        pos_new - новая позиция частицы, для которой выбирается симметричная позиция
        i - порядковый номер частицы, для которой производится расщепление
        """
        pos_sym = np.array([0 if i else 1 for i in pos_new])
        pos_new_fitness = self.ff(pos_new)
        pos_sym_fitness = self.ff(pos_sym)
        if pos_new_fitness < pos_sym_fitness:
            self._agents[i] = pos_new
            self._fitness[i] = pos_new_fitness
        else:
            self._agents[i] = pos_sym
            self._fitness[i] = pos_sym_fitness

    def fork_best(self):
        """
        Перераспределение энергии к лидеру.
        """
        index = self._fitness.argmin()
        pos_new = self.transform(self._agents[index] + np.random.normal(self.mu, self.sigma, self.dimension))
        self._agents = np.insert(self._agents, self._agents.shape[0], pos_new, axis=0)
        self._fitness = np.insert(self._fitness, self._fitness.shape[0], self.ff(pos_new))

    def eliminate_worst(self):
        """
        Удаление худшей частицы.
        """
        index = self._fitness.argmax()
        self._agents = np.delete(self._agents, index, axis=0)
        self._fitness = np.delete(self._fitness, index, axis=0)

    def change_params(self):
        """
        Изменение параметров
        """
        #  print( self.iter_count)
        if self.iter_count > 25:
            self.sigma = self.sigma * 0.7
            self.iter_count = 0
            return
        if self.iter_count > 20:
            self.sigma = self.sigma * 0.8
            self.iter_count = 0

    def T(self, p):
        return abs(math.tanh(p))

    def transform(self, vector):
        pos_new = []
        for d in range(self.dimension):
            if random.random() > self.T(vector[d]):
                pos_new.append(1)
            else:
                pos_new.append(0)
        if not any(pos_new):
            pos_new[random.randint(0, self.dimension-1)] = 1
        return np.array(pos_new)

    def _one_iter(self):
        """
        Одна итерация цикла.
        """
        for i in range(self.n):
            if self.channelTime > self.maxChannelTime:
                self.eliminate_worst()
                self.fork_best()
                self.channelTime = 0
            self.channelTime += 1
            pos_new = self.transform(self._agents[i] + np.random.normal(self.mu, self.sigma, self.dimension))
            pos_new_fitness = self.ff(pos_new)

            if pos_new_fitness > self.ff(np.array(self._agents[i])):
                self.iter_count += 1
                continue
            #if np.random.random() > self.forkProbability:
            #    self.fork(pos_new, i)
            #else:
            #    self._agents[i] = pos_new
            #    self.Y[i] = self.function(self.X[i])
            self._agents[i] = pos_new
            self._fitness[i] = pos_new_fitness
        # index = self.Y.argmin()
        # self._set_Gbest(self.X[index])
        # print(self.get_Gbest())
