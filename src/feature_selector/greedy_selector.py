import numpy as np

from src.optimization_algorithm.meta import OptimizationAlgorithmMeta


class GreedySelector(OptimizationAlgorithmMeta):
    """
    "Жадный" алгоритм отбора признаков.
    """

    def __init__(
            self,
            function,
            max_iter,
            population,
            no_change_iteration=None):
        super().__init__(
            population, function, None, None, 1, no_change_iteration,
            feature_selection=True
        )
        self.dimension = population.shape[1]
        self.stop_search = False  # Флаг, чтобы остановить поиск в случае, если новый признак не был добавлен в вектор.

    def find_new_best(self, vector, best_error):
        self.stop_search = True
        best_vector = vector.copy()
        for i in range(self.dimension):
            new_vector = vector.copy()
            if new_vector[i] == 1:
                continue
            new_vector[i] = 1
            error = self.ff(new_vector)
            if error < best_error:
                best_error = error
                best_vector = new_vector.copy()
                self.stop_search = False
        return best_vector, best_error

    def _one_iter(self):
        vector = np.zeros(self.dimension, dtype=int)
        error = 1
        best_result = vector.copy()
        for _ in range(self.dimension):
            best_result, error = self.find_new_best(best_result, error)
            if self.stop_search:
                break
        # print(best_result, error)
        self._set_agents(np.array([best_result]))
