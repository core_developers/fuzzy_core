class DataScheme:
    def __init__(self, data_name, attributes, inputs, outputs, X, y):
        self.data_name = data_name
        self.inputs = inputs
        self.outputs = outputs
        self.attributes = attributes
        self.X = X
        self.y = y
