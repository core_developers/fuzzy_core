from pandas import read_csv
from pathlib import Path
import numpy as np

from src.parser.data_scheme import DataScheme


def load_csv(file_path, header=None):
    file_path = Path(file_path)
    data_name = file_path.name.rsplit('.')[0]
    data = read_csv(file_path.open('rb'), header=header)
    *inputs, output = data.columns

    return DataScheme(
        data_name,
        dict(),
        inputs,
        [output],
        np.array(data.iloc[:, :-1]),
        np.array(data.iloc[:, -1])
    )


def get_data_from_csv_dir(data_dir, header=None):
    data_dir = Path(data_dir)
    for item in sorted(data_dir.iterdir()):
        yield load_csv(item, header)
