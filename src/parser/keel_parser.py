import re
from pathlib import Path
from pandas import DataFrame, Categorical
import numpy as np

from .data_scheme import DataScheme

# TODO: implement multiple outputs support
# TODO: refactor, change regexp


class KeelParser:
    def __call__(self, data):
        self.data = data

        data_name = self.get_data_name()
        attributes = self.get_attributes()
        inputs = self.get_inputs()
        outputs = self.get_outputs()
        data_frame = self.get_data()
        data_frame.columns = [*inputs, *outputs]
        X = np.array(data_frame[inputs])
        y = np.array(data_frame[outputs]).ravel()

        if len(outputs) > 1:
            raise Exception('Multiple outputs in dataset not yet supported')
        if attributes[outputs[0]][0] == 'nominal':
            y = Categorical(
                y,
                ordered=False,
                categories=attributes[outputs[0]][1]
            ).codes
        elif attributes[outputs[0]][0] == 'real':
            y = np.array(y, dtype=float)
        return DataScheme(data_name, attributes, inputs, outputs, X, y)

    def get_data_name(self):
        """
        Функция для получения названия набора данных
        :return:
        """
        parsed = re.search(r'^@relation\s+(\S+)$', self.data, re.MULTILINE)
        return parsed.group(1)

    def get_attributes(self):
        """
        Функция для получения описаний входных и выходных параметров
        набора данных
        :return:
        """
        attributes = dict()
        if "Attribute" in self.data:
            parsed = re.findall(r'^@Attribute\s+(.+)$', self.data, re.MULTILINE)
        else:
            parsed = re.findall(r'^@attribute\s+(.+)$', self.data, re.MULTILINE)

        for att in parsed:
            if '{' in att:
                name = att[:att.index('{')].strip()
                att = att[att.index('{'):]
                for char in '{}':
                    att = att.replace(char, '')
                att = att.replace(" ", "")
                att = att.split(',')
                attributes[name] = ['nominal', att]
            else:
                if 'real' in att or 'integer' in att:
                    name = att[:att.index(' ')]
                    att = att[att.index(' ') + 1:]
                    att_type = att[:att.index('[')].strip()
                    att = att[att.index('['):]
                    for char in '[]':
                        att = att.replace(char, '')
                    attributes[name] = [
                        att_type, att.replace(' ', '').split(',')]
                else:
                    if 'numeric' in att:
                        att = att.split(' ')
                        attributes[att[0].strip()] = ['numeric']
                    else:
                        raise Exception(
                            f'Error during @attribute line parse: {att}')

        return attributes

    def get_inputs(self):
        """
        Функция для получения списка входных параметров набора данных
        :return:
        """
        parsed = re.search(r'^@inputs\s+(.+)$', self.data, re.MULTILINE)
        return [j.strip() for j in parsed.group(1).split(',')]

    def get_outputs(self):
        """
        Функция для получения списка выходных меток набора данных
        :return:
        """
        if "outputs" in self.data:
            parsed = re.search(r'^@outputs\s+(.+)$', self.data, re.MULTILINE)
        else:
            parsed = re.search(r'^@output\s+(.+)$', self.data, re.MULTILINE)

        return [x.strip() for x in parsed.group(1).split(',')]

    def get_data(self):
        """
        Функция для получения данных из выбранного набора
        :return:
        """
        parsed = re.search(
            r'^@data\n(.+)', self.data, flags=re.MULTILINE | re.DOTALL)
        data = np.array([
            re.split(r', ?', item) for item in parsed.group(1).splitlines()])
        dim = len(data[0])
        ys = np.copy(data[:, dim - 1])
        data[:, dim - 1] = 0
        df = DataFrame(data=data, dtype=float)
        df[dim - 1] = df[dim - 1].astype(str)
        df[dim - 1] = ys

        return df


def load_keel(file_path):
    file_path = Path(file_path)
    return KeelParser()(file_path.read_text())


def get_data_from_keel_dir(dir_path):
    """
    Функция для получения всех данных из директории dir_path
    :param dir_path: string
    :return: list
    """
    data_dir = Path(dir_path)
    files = sorted([
        item for item in data_dir.iterdir()
        if 'tra' in item.stem or 'tst' in item.stem
    ])
    counter = 0
    for pair in [files[i:i + 2] for i in range(0, len(files), 2)]:
        train, test = pair

        yield load_keel(train), load_keel(test), counter
        counter += 1
