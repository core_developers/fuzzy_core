from src.optimization_algorithm.meta import OptimizationAlgorithmMeta

import numpy as np


class LighteningSearchAlgorithm(OptimizationAlgorithmMeta):
    """Lightning Search Alghorithm

    The implementation follows [1]_. *Add important things in desc*.

    Parameters
    ----------
    population : ndarray
        Population of optimization agents
    function : callable
        Function which values are going to be minimized
    lb : None or array_like
        Lower limits of variables being optimized.
    ub : None or array_like
        Lower limits of variables being optimized.
    max_iter : int
        Maximum number of iterations to be performed.
    no_change_iteration: None or int, optional
        Number of consecutive iterations wtih no change of global best
        fitness value
    max_channel_time : int
        Количество проходов, через которое худший
        канал будет отброшен, а энергия перераспределена к лидеру
        (default value is 5)
    mu : float
        параметр нормального распределения (мат. ожидание)
        (default value is 0.0)
    sigma : float
        параметр нормального распределения (СКО)
        (default value is 1.0)

    Attributes
    ----------
    *Add description*

    References
    ----------
    .. [1] *add*.
    """
    def __init__(
            self,
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration=None,
            max_channel_time=5,
            mu=0,
            sigma=1):
        super().__init__(
            population, function, lb, ub, max_iter, no_change_iteration)

        if (lb is None) or (ub is None):
            self.ub = np.amax(self._agents.all()) * 5
            self.lb = np.amin(self._agents.all()) * 5
        else:
            self.lb = lb
            self.ub = ub

        self.maxChannelTime = max_channel_time
        self.mu = mu
        self.sigma = sigma #self.ub / 5
        self.forkProbability = 0.5
        self.channelTime = 0
        self.iter_count = 0
        self._fitness = np.array([self.ff(x) for x in self._agents])

    def fork(self, pos_new, i):
        """
        Расщепление канала. Будет найдена симметричная позиция и выбрана та, в которой функция имеет лучшее значение.
        pos_new - новая позиция частицы, для которой выбирается симметричная позиция
        i - порядковый номер частицы, для которой производится расщепление
        """
        pos_sym = self.lb + self.ub - pos_new

        pos_new_fitness = self.ff(pos_new)
        pos_sym_fitness = self.ff(pos_sym)

        if pos_new_fitness < pos_sym_fitness:
            self._agents[i] = pos_new
            self._fitness[i] = pos_new_fitness
        else:
            self._agents[i] = pos_sym
            self._fitness[i] = pos_sym_fitness

    def fork_best(self):
        """
        Перераспределение энергии к лидеру.
        """
        index = self._fitness.argmin()
        pos_new = self._agents[index] + np.random.normal(self.mu, self.sigma, self.dimension)

        self._agents = np.insert(self._agents, self._agents.shape[0], pos_new, axis=0)
        self._fitness = np.insert(self._fitness, self._fitness.shape[0], self.ff(pos_new))

    def eliminate_worst(self):
        """
        Удаление худшей частицы.
        """
        index = self._fitness.argmax()

        self._agents = np.delete(self._agents, index, 0)
        self._fitness = np.delete(self._fitness, index)

    def change_params(self):
        """
        Изменение параметров
        """
        #  print( self.iter_count)
        if self.iter_count > 25:
            self.sigma = self.sigma * 0.7
            self.iter_count = 0
            return
        if self.iter_count > 20:
            self.sigma = self.sigma * 0.8
            self.iter_count = 0

    def _one_iter(self):
        """
        Одна итерация цикла.
        """
        for i in range(self.n):
            if self.channelTime > self.maxChannelTime:
                self.eliminate_worst()
                self.fork_best()
                self.channelTime = 0
            self.channelTime += 1
            pos_new = self._agents[i] + np.random.normal(self.mu, self.sigma, self.dimension)
            pos_new_fitness = self.ff(pos_new)
            if pos_new_fitness > self._fitness[i]:
                self.iter_count += 1
                continue
            if np.random.random() > self.forkProbability:
                self.fork(pos_new, i)
            else:
                self._agents[i] = pos_new
                self._fitness[i] = pos_new_fitness
