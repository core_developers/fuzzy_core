import numpy as np
import time

from src.constant.status import EARLY_STOP, OK

# TODO: check on self._fitness type
# TODO: move status to log
# TODO: implement adaptive params to all algs


class OptimizationAlgorithmMeta:
    """Optimization algorithm base class

    The implementation applies only to population-based algorithms.

    Parameters
    ----------
    n : int
        Number of agents in population
    function : callable
        Function which values are going to be minimized
    lb : None or array_like
        Lower limits of variables being optimized.
    ub : None or array_like
        Lower limits of variables being optimized.
    dimension : int
        Search space dimension.
    max_iter : int
        Number of iterations performed.
    no_change_iteration: None or int, optional
        Number of consecutive iterations with no change of global best
        fitness value
    population_generator: None or callable, optional
        Population generator
    trace_path: bool
        if True, saves history of agents positions and fitness values
    feature_select: bool
        NEEDS to be set with True in case of feature selection

    Attributes
    ----------
    n : int
        Number of agents in population
    ff : callable
        Wrapper of function which values are going to be minimized
    lb : None or array_like
        Lower limits of variables being optimized.
    ub : None or array_like
        Lower limits of variables being optimized.
    dimension : int
        Search space dimension.
    max_iter : int
        Number of iterations performed.
    log : dictionary
        Dictionary containing history of agents positions, fitness values (if
        trace_path is True), previous iteration's best postion and fitness
        value, lifetime's best postion and fitness value, time spent on,
        counter of fitness function calls
    """
    def __init__(
            self,
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration,
            trace_path=False,
            feature_selection=False):
        self.log = {
            "agents": [],
            "fitness": [],
            "pbest": [],
            "gbest": [],
            "pbest_f": [],
            "gbest_f": [],
            "time": [],
            "fcounter": [],
            "status": OK
        }
        if population is None:
            raise TypeError()
        self._agents = population
        if len(population) > 0:
            if len(population[0]) > 0:
                self.n = len(population)
                self.dimension = len(population[0])
            else:
                raise ValueError()
        else:
            raise ValueError()
        self._trace_path = trace_path
        self.max_iter = max_iter
        self.current_iter = 1
        self._ff_count = 0
        self.no_change_iteration = no_change_iteration
        if lb is None or ub is None:
            self.lb = 0
            self.ub = 1
        else:
            self.lb = lb
            self.ub = ub
        self._ff = function
        if feature_selection:
            self.ff = self._ff_fs
        self._fitness = [self.ff(x) for x in self._agents]
        self._time = 0
        self._gbest = None
        self._gbest_f = -1
        self._update_sorted()
        self._update_history()

    def ff(self, x):
        """
        Get fitness function value for vector x.
        Before using it make sure that x is clipped
        with force_bounds(x) and saved if needed

        Parameters
        ----------
        x : nparray
            Vector for which function value need to be evaluated
        Returns
        -------
        fvalue : float
            Fitness function value
        """
        self._ff_count += 1
        return self._ff(x)

    def _ff_fs(self, x):
        self._ff_count += 1
        return self._ff(accepted_features=x)

    def force_bounds(self, x):
        """
        Clip vector into set bounds and return result. If bounds weren't set,
        returns vector's copy

        Parameters
        ----------
        x : nparray
            Vector to be clipped

        Returns
        -------
        y : nparray
            Clipped vector or it's copy
        """
        if not ((self.lb is None) or (self.ub is None)):
            return np.clip(x, self.lb, self.ub)
        return x.copy()

    def _set_agent(self, value, ind, update_fit=True, update_sort=True):
        """
        Set agent's new position

        Parameters
        ----------
        value : nparray
            Agent's new position
        ind : int
            Agent's index
        update_fit : bool
            If True, update fitness value according to new position
        update_sort : bool
            If True, sort fitness values and save best/worst positions
        """
        self._agents[ind] = self.force_bounds(value)
        if update_fit:
            self._fitness = self.ff(value)
        if update_sort:
            self._update_sorted()

    def _set_agents(self, population, update_fit=True, update_sort=True):
        """
        Set population's new position

        Parameters
        ----------
        value : nparray
            Population's new position
        update_fit : bool
            If True, update fitness values according to new position
        update_sort : bool
            If True, sort fitness values and save best/worst positions
        """
        self._agents = self.force_bounds(population)
        if update_fit:
            self._fitness = [self.ff(x) for x in self._agents]
        if update_sort:
            self._update_sorted()

    def _update_sorted(self):
        # check if changed self._fitness type
        self._sorted_fitness_args = np.argsort(self._fitness)
        self._pbest = self._agents[self._sorted_fitness_args[0]].copy()
        self._pbest_f = self._fitness[self._sorted_fitness_args[0]]
        self._pworst = self._agents[self._sorted_fitness_args[-1]].copy()
        self._pworst_f = self._fitness[self._sorted_fitness_args[-1]]
        if (self._gbest is None) or (self._pbest_f < self._gbest_f):
            self._gbest = self._pbest.copy()
            self._gbest_f = self._pbest_f

    def _update_history(self):
        if self._trace_path:
            self.log["agents"].append([list(i) for i in self._agents])
            self.log["fitness"].append(self._fitness)
        self.log["pbest"].append(self._pbest.copy())
        self.log["pbest_f"].append(self._pbest_f)
        self.log["gbest"].append(self._gbest.copy())
        self.log["gbest_f"].append(self._gbest_f)
        if not self.log["time"]:
            self.log["time"].append(self._time)
        else:
            self.log["time"].append(self._time)
        if not self.log["fcounter"]:
            self.log["fcounter"].append(self._ff_count)
        else:
            self.log["fcounter"].append(self.log["fcounter"][-1] +
                                        self._ff_count)

    def change_params(self):
        pass

    def pre_iter(self):
        pass

    def one_iter(self):
        t1 = time.thread_time()
        self._one_iter()
        t2 = time.thread_time()
        self._time += t2 - t1
        self._update_sorted()
        self._update_history()
        self.current_iter += 1

    def _one_iter(self):
        raise NotImplementedError()

    def get_best(self):
        return self._gbest, self._gbest_f, self.log

    def get_population(self):
        return self._agents.copy()

    def run(self):
        self.pre_iter()

        if not self.no_change_iteration:
            self._run()
        else:
            self._run_with_no_change_iteration()

        return self.get_best()

    def _run(self):
        for _ in range(self.max_iter):
            self.one_iter()
            self.change_params()

    def _run_with_no_change_iteration(self):
        no_change = 0
        old_best = self.get_best()[0]
        for _ in range(self.max_iter):
            self.one_iter()

            new_best = self.get_best()[0]
            if np.array_equal(new_best, old_best):
                no_change += 1
            else:
                no_change = 0
                old_best = new_best

            if no_change == self.no_change_iteration:
                self.log["status"] = EARLY_STOP
                break

            self.change_params()
