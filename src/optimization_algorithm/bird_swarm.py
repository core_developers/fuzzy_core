import numpy as np

from src.optimization_algorithm.meta import OptimizationAlgorithmMeta
from scipy.stats import levy
from sklearn.preprocessing import normalize


class BirdSwarm(OptimizationAlgorithmMeta):
    """Bird Swarm alghorithm

    The implementation follows [1]_. *Add important things in desc*.

    Parameters
    ----------
    population : ndarray
        Population of optimization agents
    function : callable
        Function which values are going to be minimized
    lb : None or array_like
        Lower limits of variables being optimized.
    ub : None or array_like
        Lower limits of variables being optimized.
    max_iter : int
        Maximum number of iterations to be performed.
    no_change_iteration: None or int, optional
        Number of consecutive iterations wtih no change of global best
        fitness value
    FQ: int, optional
        time between applying 1-2 and 3-4 strategies (default value is 3)
    P: float, optional
        probability of applying strategy 1 (default value is 0.9)
    C: float, optional
        cognitive coefficient (default value is 1.5)
    S: float, optional
        social coefficient (default value is 1.5)
    a1: float, optional
        strategy 2 parameter (default value is 1.0)
    a2: float, optional
        strategy 2 parameter (default value is 1.0)
    FL: float, optional
        strategy 4 parameter (default value is 0.75)
    levi: bool, optional
        enable Levy flights in strategy 4 (default value is False)
    adaptive: bool, optional
        enable Adaptive parameters (default value is False)

    Attributes
    ----------
    *Add description*

    References
    ----------
    .. [1] Xian-Bing Meng, X.Z. Gao, Lihua Lu, Yu Liu & Hengzhen Zhang (2015):
           A new bio-inspired optimisation algorithm: Bird Swarm Algorithm,
           Journal of Experimental & Theoretical Artificial Intelligence,
           DOI: 10.1080/0952813X.2015.1042530
    """
    def __init__(
            self,
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration=None,
            FQ=3,
            P=0.9,
            C=1.5,
            S=1.5,
            a1=1.0,
            a2=1.0,
            FL=0.75,
            levi=False,
            adaptive=False):
        super().__init__(
            population, function, lb, ub, max_iter, no_change_iteration)

        self.FQ = FQ
        self.P = P
        self.C = C
        self.S = S
        self.a1 = a1
        self.a2 = a2
        self.FL = FL
        self.W = 1
        self.H = 0
        self.levi = levi
        self.adaptive = adaptive
        self.FQV = 1
        self._prev_best = self._agents.copy()
        self._prev_best_fit = np.array(self._fitness.copy())

    def _equation1(self, temp, i):
        if self.adaptive:
            temp = (
                temp * self.W + (
                    (self._prev_best[i] - temp) * self.C *
                    np.random.uniform(0, 1, self.dimension) +
                    (self._gbest - temp) * self.S *
                    np.random.uniform(0, 1, self.dimension)
                )
            )
        else:
            temp += (
                (self._prev_best[i] - temp) * self.C *
                np.random.uniform(0, 1, self.dimension) +
                (self._gbest - temp) * self.S *
                np.random.uniform(0, 1, self.dimension)
            )

        return temp

    def _equation2(self, temp, i, mean):
        k = np.random.choice(np.delete(np.arange(0, self.n), i))
        p = normalize(self._prev_best_fit[:, np.newaxis], axis=0).ravel()
        sum_fit = np.sum(p)
        eps = np.finfo(float).eps
        # TODO: add check if overflow in np.exp()
        A1 = self.a1 * np.exp((-1.0) * self.dimension * p[i] / (sum_fit + eps))
        A2 = self.a2 * np.exp((p[i] - p[k]) / (np.abs(
            (p[i] - p[k])) + eps) * self.dimension * p[k] / (sum_fit + eps))
        temp += (
            (mean - temp) * A1 * np.random.uniform(0, 1, self.dimension) +
            (self._prev_best[i] - temp) * A2 *
            np.random.uniform(-1, 1, self.dimension)
        )

        return temp

    def _equation3(self, temp):
        if self.levi:
            temp += (
                np.random.choice([-1, 1], size=self.dimension) *
                levy.rvs(size=self.dimension)
            )
        else:
            if self.adaptive:
                temp += (
                    (np.random.normal(size=self.dimension) * temp) +
                    temp * self.H
                )
            else:
                temp += (np.random.normal(size=self.dimension) * temp)

        return temp

    def _equation4(self, temp, k):
        temp += (
            (self._agents[k] - temp) * self.FL *
            np.random.uniform(0, 1, size=self.dimension)
        )

        return temp

    def _update_bird(self, temp, i):
        temp = self.force_bounds(temp)
        temp_fit = self.ff(temp)
        if temp_fit < self._fitness[i]:
            self._prev_best[i] = self._agents[i].copy()
            self._agents[i] = temp
            self._prev_best_fit[i] = self._fitness[i]
            self._fitness[i] = temp_fit

    def _one_iter(self):
        if self.adaptive:
            v = np.log(1 + self.current_iter * (np.e - 1) / self.max_iter)
            self.W = 0.9 * (1 - v) + 0.4 * v
            self.C = ((2 * np.pi)**(-0.5)) * (np.e**((-0.5) * (
                (self.current_iter / self.max_iter)**2)))
            self.S = (2 / np.pi)**(0.5) - self.C
            self.H = (
                (self.max_iter - self.current_iter + 1) / self.max_iter)**2
        mean = np.mean(self._agents, axis=0)
        if self.current_iter % self.FQ != 0:
            rand = np.random.uniform(0, 1, self.n)
            for i in range(0, self.n):
                temp = np.copy(self._agents[i])
                if rand[i] < self.P:
                    temp = self._equation1(temp, i)
                else:
                    temp = self._equation2(temp, i, mean)
                self._update_bird(temp, i)
        else:
            if self.adaptive:
                self.FQV += 0.01
                self.FQ = np.floor(self.FQ * self.FQV)
            bird_type = np.zeros(self.n)
            sorted_fit = self._sorted_fitness_args
            for i in range(0, self.n):
                if (i + 1) <= 0.3 * self.n:
                    bird_type[sorted_fit[i]] = 1
                else:
                    bird_type[sorted_fit[i]] = 2
            producers = [ind for ind, x in enumerate(bird_type) if x == 1]
            for i in range(0, self.n):
                temp = np.copy(self._agents[i])
                if bird_type[i] == 1:
                    temp = self._equation3(temp)
                else:
                    k = np.random.choice(producers)
                    temp = self._equation4(temp, k)
                self._update_bird(temp, i)
