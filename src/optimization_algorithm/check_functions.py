import math as m

import numpy as np

# assert args is 1D numpy array or specify axis=0?


def F1(args):
    """Sphere function, multidimensional"""
    return np.sum(np.square(args))


def F2(args):
    """Schwefel's Function No.1.2 function, multidimensional"""
    return np.sum(np.square(np.cumsum(args)))


def F3(args):
    """Schwefel's Function No.2.21 function, multidimensional"""
    return np.max(np.fabs(args))


def F4(args):
    """Schwefel's Function No.2.22 function, multidimensional"""
    a = np.fabs(args)
    return (np.sum(a) + np.prod(a))


def F5(args):
    """Step Function No.02, multidimensional"""
    return np.sum(np.square(np.floor(args)))


def F6(args):
    """Sum of different power function, multidimensional"""
    return np.sum(np.power(np.fabs(args), np.arange(len(args)) + 2))


def F7(args):
    """Zakharov function, multidimensional"""
    a = (np.arange(len(args)) + 1) * 0.5
    a = np.square(np.sum(a * args))
    return F1(args) + a + np.square(a)


def F8(args):
    """Sum Squares function, multidimensional"""
    return np.sum((np.arange(len(args)) + 1) * np.square(args))


def F9(args):
    """Rastrigin function, multidimensional"""
    a = np.square(args) + 10.0
    b = np.cos(args * 2.0 * np.pi) * 10.0
    return np.sum(a - b)


def F10(args):
    """Ackley function, multidimensional"""
    a = (-20.0) * np.exp((-0.2) * np.sqrt(F1(args) / len(args)))
    b = np.exp(np.sum(np.cos(np.square(args) * 2.0 * np.pi)) / len(args))
    return a + 20.0 + np.exp(1) - b


def F11(args):
    """Griewank function, multidimensional"""
    a = F1(args) / 4000.0 + 1
    b = args / np.sqrt(np.arange(len(args)) + 1)
    return a - np.prod(np.cos(b))


def F12(args):
    """Rana function, 2-dimensional"""
    if len(args) != 2:
        raise Exception('Args length != 2')
    x, y = args
    a = m.sqrt(abs(y + 1 - x))
    b = m.sqrt(abs(y + 1 + x))
    return x * m.sin(a) * m.cos(b) + (y + 1) * m.cos(a) * m.sin(b)


def F13(args):
    if len(args) != 2:
        raise Exception('Args length != 2')
    x, y = args
    a = m.pow(x, 2) + m.pow(y, 2)
    return (-1.0) * m.pow(a, 2) - m.pow(3.0 / (a + 0.05), 2)


def F14(args):
    """Styblinski-Tang Function, 2-dimensional"""
    if len(args) != 2:
        raise Exception('Args length != 2')
    x, y = args
    a = m.pow(x, 2) + m.pow(y, 2)
    a = m.pow(x, 4) + m.pow(y, 4) - 16 * a + 5 * (x + y)
    return a / 2.0


def F15(args):
    """Matyas function, 2-dimensional"""
    if len(args) != 2:
        raise Exception('Args length != 2')
    x, y = args
    return 0.26 * (m.pow(x, 2) + m.pow(y, 2)) - 0.48 * x * y


def F16(args):
    """Beale function, 2-dimensional"""
    if len(args) != 2:
        raise Exception('Args length != 2')
    x, y = args
    a = m.pow(1.5 - x + x * y, 2)
    a = a + m.pow(2.25 - x + x * m.pow(y, 2), 2)
    return a + m.pow(2.625 - x + x * m.pow(y, 3), 2)


def F17(args):
    """Periodic function, 2-dimensional"""
    if len(args) != 2:
        raise Exception('Args length != 2')
    x, y = args
    a = 1 + m.pow(m.sin(x), 2) + m.pow(m.sin(y), 2)
    return a - 0.1 * m.pow(np.e, (-1.0) * (m.pow(x, 2) + m.pow(y, 2)))


def F18(args):
    """Shubert function, 2-dimensional"""
    if len(args) != 2:
        raise Exception('Args length != 2')
    x, y = args
    a = np.arange(5) + 1.0
    b = np.cos((a + 1.0) * x + a) * a
    a = np.cos((a + 1.0) * y + a) * a
    a = np.sum(a) * np.sum(b)
    return a + m.pow(x + 1.42513, 2) / 2 + m.pow(y + 0.80032, 2) / 2


def F19(args):
    """Easom function, 2-dimensional"""
    if len(args) != 2:
        raise Exception('Args length != 2')
    x, y = args
    a = m.exp((-1.0) * (x - np.pi)**2 - (y - np.pi)**2)
    return (-1.0) * m.cos(x) * m.cos(y) * a


def F20(args):
    """Michalewicz function, multidimensional"""
    # The parameter m defines the steepness of they valleys and ridges;
    # a larger m leads to a more difficult search. recommended: 10
    # optimum depends on number of dimensions
    m = 10
    a = np.sin(np.square(args) * (np.arange(len(args)) + 1) / np.pi)
    return (-1.0) * np.sum(np.sin(args) * np.power(a, 2 * m))


def F21(args):
    """Schaffer N.3 function, 2-dimensional"""
    if len(args) != 2:
        raise Exception('Args length != 2')
    x, y = args
    a = np.sin(np.cos(abs(x**2 - y**2)))
    b = 1 + 0.001 * (x**2 + y**2)
    return 0.5 + (a**2 - 0.5) / (b**2)


def F22(args):
    """Holder-Table function, 2-dimensional"""
    if len(args) != 2:
        raise Exception('Args length != 2')
    x, y = args
    a = m.exp(abs(1 - m.sqrt(x**2 + y**2) / m.pi))
    return (-1.0) * abs(m.sin(x) * m.cos(y) * a)


def F23(args):
    """Keane function, 2-dimensional"""
    if len(args) != 2:
        raise Exception('Args length != 2')
    x, y = args
    a = m.sin(x - y)
    b = m.sin(x + y)
    return (-1.0) * (a**2 * b**2) / m.sqrt(x**2 + y**2)


def F24(args):
    """Drop-wave function, 2-dimensional"""
    if len(args) != 2:
        raise Exception('Args length != 2')
    x, y = args
    a = 1 + m.cos(12 * m.sqrt(x**2 + y**2))
    b = (0.5 * (x**2 + y**2) + 2)
    return (-1.0) * a / b


def F25(args):
    """VBoot function, 2-dimensional"""
    if len(args) != 2:
        raise Exception('Args length != 2')
    x, y = args
    return (x + 2 * y - 7)**2 + (2 * x - 5)**2


def F26(args):
    """De Jong N.5 function, 2-dimensional"""
    if len(args) != 2:
        raise Exception('Args length != 2')
    x, y = args
    a = np.power(np.repeat(x, 25) - np.tile([-32, -16, 0, 16, 32], 5), 6)
    b = np.power(np.repeat(y, 25) - np.repeat([-32, -16, 0, 16, 32], 5), 6)
    return 1 / (0.002 + np.sum(1 / ((np.arange(25) + 1) + a + b)))


# TODO: add params of this func to map
def F27(args):
    """DeVilliers-Glasser 2 function, 5-dimensional"""
    if len(args) != 2:
        raise Exception('Args length != 2')
    x, y = args
    a = np.power(np.repeat(x, 25) - np.tile([-32, -16, 0, 16, 32], 5), 6)
    b = np.power(np.repeat(y, 25) - np.repeat([-32, -16, 0, 16, 32], 5), 6)
    return 1 / (0.002 + np.sum(1 / ((np.arange(25) + 1) + a + b)))


# TODO: rework
def F28(args):
    """Ackley N4, multidimensional"""
    x = args
    res = 0
    for i in range(len(args) - 1):
        res += (np.exp(-0.2) * np.sqrt(x[i]**2 + x[i + 1]**2) + 3 *
                (np.cos(2 * x[i]) + np.sin(2 * x[i + 1])))
    return res


def F29(args):
    """Alpine N1, multidimensional"""
    x = args
    return np.sum(np.abs(x * np.sin(x) + 0.1 * x))


def F30(args):
    """Alpine N2, multidimensional"""
    x = args
    return (-1.0) * np.prod(np.sqrt(x) * np.sin(x))


def F31(args):
    """Happycat, multidimensional"""
    x = args
    n = len(x)
    x2 = np.linalg.norm(x)**2
    return (np.power(np.trunc(np.square(x2 - n)),
                     (1.0 / 8.0)) + 1.0 / n * (0.5 * x2 + np.sum(x)) + 0.5)


def F32(args):
    """Periodic, multidimensional"""
    x = args
    return (1.0 + np.sum(np.square(np.sin(x))) - 0.1 * np.exp(
        (-1.0) * np.sum(np.square(x))))


def F33(args):
    """Salomon, multidimensional"""
    x = args
    sq = np.square(x)
    return (1.0 - np.cos(2.0 * np.pi * np.sqrt(np.sum(sq))) +
            (0.1 * np.sqrt(np.sum(sq))))


def F34(args):
    """Shubert3, multidimensional"""
    x = args
    j = np.atleast_2d(np.arange(1, 6)).T
    y = -j * np.sin((j + 1) * x + j)
    return np.sum(np.sum(y))


def F35(args):
    """Xin-She Yang N2, multidimensional"""
    x = np.abs(args)
    return np.sum(x) * np.exp((-1.0) * np.sum(np.sin(np.square(x))))


def F36(args):
    """Xin-She Yang N4, multidimensional"""
    x = args
    mx = np.sqrt(np.abs(args))
    a = np.sum(np.square(np.sin(x))) - np.exp((-1.0) * np.sum(np.square(x)))
    return (a) * np.exp((-1.0) * np.sum(np.power(np.sin(mx), 2)))


def F37(args):
    """Shubert, multidimensional"""
    x = args
    j = np.atleast_2d(np.arange(1, 6)).T
    y = j * np.cos((j + 1) * x + j)
    return np.prod(np.sum(y, axis=0))


# bounds: optimum: dimensions_restrictions: function
FUNCTION_PARAMS_MAP = {
    "F1": [[-100.0, 100.0], 0.0, None, F1],
    "F2": [[-100.0, 100.0], 0.0, None, F2],
    "F3": [[-100.0, 100.0], 0.0, None, F3],
    "F4": [[-10.0, 10.0], 0.0, None, F4],
    "F5": [[-100.0, 100.0], 0.0, None, F5],
    "F6": [[-1.0, 1.0], 0.0, None, F6],
    "F7": [[-5.0, 10.0], 0.0, None, F7],
    "F8": [[-5.12, 5.12], 0, None, F8],
    "F9": [[-5.12, 5.12], 0, None, F9],
    "F10": [[-32.0, 32.0], 0.0, None, F10],
    "F11": [[-600.0, 600.0], 0.0, None, F11],
    "F12": [[-512.0, 512.0], -511.73288188482724, 2, F12],
    "F13": [[-5.12, 5.12], -3600, 2, F13],
    "F14": [[-5.0, 5.0], -78.3323, 2, F14],
    "F15": [[-10.0, 10.0], 0.0, 2, F15],
    "F16": [[-4.5, 4.5], 0.0, 2, F16],
    "F17": [[-10.0, 10.0], 0.9, 2, F17],
    "F18": [[-10.0, 10.0], -186.7309, 2, F18],
    "F19": [[-100.0, 100.0], -1.0, 2, F19],
    "F20": [[0.0, m.pi], None, None, F20],
    "F21": [[-100.0, 100.0], 0.00156685, 2, F21],
    "F22": [[-10.0, 10.0], -19.2085, 2, F22],
    "F23": [[np.finfo(float).eps, 10.0], -0.673667521146855, 2, F23],
    "F24": [[-5.2, 5.2], -1.0, 2, F24],
    "F25": [[-10.0, 10.0], 0.0, 2, F25],
    "F26": [[-65.536, 65.536], 0.9980042867670256, 2, F26],
    "F28": [[-35.0, 35.0], None, None, F28],
    "F29": [[0.0, 10.0], 0.0, None, F29],
    "F30": [[0.0, 10.0], None, None, F30],
    "F31": [[-2.0, 2.0], 0, None, F31],
    "F32": [[-10.0, 10.0], 0.9, None, F32],
    "F33": [[-100.0, 100.0], 0.0, None, F33],
    "F34": [[-10.0, 10.0], -29.6733337, None, F34],
    "F35": [[-2 * m.pi, 2 * m.pi], 0.0, None, F35],
    "F36": [[-10.0, 10.0], -1.0, None, F36],
    "F37": [[-10.0, 10.0], -186.7309, None, F37]
}
