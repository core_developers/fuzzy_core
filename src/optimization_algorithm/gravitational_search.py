from src.optimization_algorithm.meta import OptimizationAlgorithmMeta
import numpy as np


class GravitationalSearch(OptimizationAlgorithmMeta):
    """
    Gravitational Search

    The implementation follows [1]_. *Add important things in desc*.

    Parameters
    ----------
    population : ndarray
        Population of optimization agents
    function : callable
        Function which values are going to be minimized
    lb : None or array_like
        Lower limits of variables being optimized.
    ub : None or array_like
        Lower limits of variables being optimized.
    max_iter : int
        Maximum number of iterations to be performed.
    no_change_iteration: None or int, optional
        Number of consecutive iterations wtih no change of global best
        fitness value
    G0 : float
        Initial value of gravitational constant G
        (default value is 10)
    alpha : float
        Rate parameter of exponent
        (default value is 10)
    kbest : float
        *TODO: add description*
        (default value is 10)

    Attributes
    ----------
    *Add description*

    References
    ----------
    .. [1] *add*.
    """
    def __init__(
            self,
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration=None,
            G0=10,
            alpha=10,
            kbest=10):
        super().__init__(
            population, function, lb, ub, max_iter, no_change_iteration)

        self.kbest = kbest
        self.kbest_agents = np.zeros(self.n)
        self.G0 = G0
        self.G = G0
        self.alpha = alpha
        self.eps = np.finfo(float).eps
        self.M = np.zeros(self.n)
        self.acc = np.zeros((self.n, self.dimension))
        self.vel = np.zeros((self.n, self.dimension))
        self.K = kbest

    def _masses(self):
        Amax = self._pworst_f
        Amin = self._pbest_f
        if Amax == Amin:
            self.M = np.ones(self.n)  # случай, когда популяция сошлась
        else:
            self.M = (self._fitness - Amax) / (Amin - Amax)
        Msum = sum(self.M)
        self.M /= Msum

        return self.M

    def _gconstant(self):
        self.G = self.G0 * np.exp(
            -self.alpha * (self.current_iter - 1.0) / self.max_iter)

        return self.G

    def _gfield(self):
        if self.K > self.n:
            raise ValueError()
        else:
            T = np.zeros((self.n, self.dimension))
            r = np.random.random(size=(self.K, self.n))
            Mt = self.M[self._sorted_fitness_args[:self.K]]
            for i in range(int(self.K)):
                T1 = (
                    np.tile(
                        self._agents[self._sorted_fitness_args[i]],
                        (self.n, 1)
                    ) - self._agents
                )
                T2 = np.linalg.norm(T1, axis=1, keepdims=True) + self.eps
                T += T1 / T2.flatten()[:, None] * r[i][:, None] * Mt[i] * self.G
            self.acc = T

        return self.acc

    def _move_vector(self):
        self.vel = np.random.random(
            size=(self.n, self.dimension)) * self.vel + self.acc
        result = self._agents + self.vel
        self._set_agents(result, update_sort=False)

    def _one_iter(self):
        self._masses()
        self._gconstant()
        self._gfield()
        self._move_vector()
