import numpy as np

from src.optimization_algorithm.meta import OptimizationAlgorithmMeta


class ParticleSwarm(OptimizationAlgorithmMeta):
    """
    Particle Swarm

    The implementation follows [1]_. *Add important things in desc*.

    Parameters
    ----------
    population : ndarray
        Population of optimization agents
    function : callable
        Function which values are going to be minimized
    lb : None or array_like
        Lower limits of variables being optimized.
    ub : None or array_like
        Lower limits of variables being optimized.
    max_iter : int
        Maximum number of iterations to be performed.
    no_change_iteration: None or int, optional
        Number of consecutive iterations wtih no change of global best
        fitness value
    w: float
        Balance between the range of research and consideration for
        suboptimal decisions found. if w>1 the particle velocity increases,
        they fly apart and inspect the space more carefully. If w<1 particle
        velocity decreases, convergence speed depends on parameters c1 and c2
        (default value is 0.5)
    c1 : float
        Ratio between "cognitive" and "social" component
        (default value is 1)
    c2 : float
        Ratio between "cognitive" and "social" component
        (default value is 1)

    Attributes
    ----------
    *Add description*

    References
    ----------
    .. [1] *add*.
    """
    def __init__(
            self,
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration=None,
            w=0.5,
            c1=1,
            c2=1):
        super().__init__(
            population, function, lb, ub, max_iter, no_change_iteration)

        self.w = w
        self.c1 = c1
        self.c2 = c2
        self.velocity = np.zeros((self.n, self.dimension))

    def _one_iter(self):
        r1 = np.random.random((self.n, self.dimension))
        r2 = np.random.random((self.n, self.dimension))
        self.velocity = (
            self.w * self.velocity +
            self.c1 * r1 * (self._pbest - self._agents) +
            self.c2 * r2 * (self._gbest - self._agents)
        )
        self._agents += self.velocity
        self._agents = self.force_bounds(self._agents)
        self._fitness = [self.ff(x) for x in self._agents]
