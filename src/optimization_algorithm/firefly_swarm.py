import numpy as np

from src.optimization_algorithm.meta import OptimizationAlgorithmMeta

# TODO: speed up, improve efficiency


class FireflySwarm(OptimizationAlgorithmMeta):
    """Firefly Swarm Alghorithm

    The implementation follows [1]_. *Add important things in desc*.

    Parameters
    ----------
    population : ndarray
        Population of optimization agents
    function : callable
        Function which values are going to be minimized
    lb : None or array_like
        Lower limits of variables being optimized.
    ub : None or array_like
        Lower limits of variables being optimized.
    max_iter : int
        Maximum number of iterations to be performed.
    no_change_iteration: None or int, optional
        Number of consecutive iterations wtih no change of global best
        fitness value
    csi : float
        Mutual attraction (default value is 1)
    psi : float
        Light absorption coefficient of the medium
        (default value is 1)
    alpha0 : float
        Initial value of the free randomization parameter alpha
        (default value is 1)
    alpha1 : float
        Final value of the free randomization parameter alpha
        (default value is 0.1)
    norm0 : float
        First parameter for a normal (Gaussian) distribution
        (default value is 0)
    norm1 : float
        Second parameter for a normal (Gaussian) distribution
        (default value is 0.2)

    Attributes
    ----------
    *Add description*

    References
    ----------
    .. [1] *add*.
    """
    def __init__(
            self,
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration=None,
            csi=1,
            psi=1,
            alpha0=1,
            alpha1=0.1,
            norm0=0,
            norm1=0.2):
        super().__init__(
            population, function, lb, ub, max_iter, no_change_iteration)

        self.alpha0 = alpha0
        self.alpha1 = alpha1
        self.csi = csi
        self.psi = psi
        self.norm0 = norm0
        self.norm1 = norm1

    def _one_iter(self):
        alpha = self.alpha1 + (self.alpha0 -
                               self.alpha1) * np.exp(-self.current_iter)

        for i in range(self.n):
            for j in range(self.n):
                if self._fitness[i] > self._fitness[j]:
                    self._move(i, j, alpha)
                else:
                    self._agents[i] = np.random.normal(
                        self._agents[i],
                        abs(self._agents[i]) * 0.2, self.dimension)
                self._agents[i] = self.force_bounds(self._agents[i])
                self._fitness[i] = self.ff(self._agents[i])

    def _move(self, i, j, alpha):

        r = np.linalg.norm(self._agents[i] - self._agents[j])
        beta = self.csi / (1 + self.psi * r * r)

        temp = (
            self._agents[i] + beta * (self._agents[i] - self._agents[j]) +
            alpha * np.exp(-self.current_iter) * (
                self._agents[i] *
                np.random.normal(self.norm0, self.norm1, self.dimension)
            )
        )

        self._agents[i] = temp
