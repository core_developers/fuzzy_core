import functools
import operator

import numpy as np

from src.optimization_algorithm.meta import OptimizationAlgorithmMeta

# TODO: check if _agents.copy() is necessary
# TODO: speed up, improve efficiency


class ArtificialFishSwarm(OptimizationAlgorithmMeta):
    """Artificial Fish Swarm

    The implementation follows [1]_. *Add important things in desc*.

    Parameters
    ----------
    population : ndarray
        Population of optimization agents
    function : callable
        Function which values are going to be minimized
    lb : None or array_like
        Lower limits of variables being optimized.
    ub : None or array_like
        Lower limits of variables being optimized.
    max_iter : int
        Maximum number of iterations to be performed.
    no_change_iteration: None or int, optional
        Number of consecutive iterations wtih no change of global best
        fitness value
    V: float, optional
        vision radius (default value is 0.5)
    S: float, optional
        step radius (default value is 0.2)
    D: float, optional
        delta (default value is 0.5)
    T: int, optional
        number of tries in prey phase (default value is 5)

    Attributes
    ----------
    *Add description*

    References
    ----------
    .. [1] Neshat, Mehdi & Adeli, Ali & Sepidnam, Ghodrat & Sargolzaei,
           Mahdi & Toosi, Adel. (2012). A Review of Artificial Fish Swarm
           Optimization Methods and Applications. Int. Journal on Smart
           Sensing and Intelligent Systems. 5. 107-148.
    """
    def __init__(
            self,
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration=None,
            V=0.5,
            S=0.2,
            D=0.5,
            T=5):
        super().__init__(
            population, function, lb, ub, max_iter, no_change_iteration)

        self.V = V
        self.S = S
        self.D = D
        self.T = T

        # TODO: change docstring and input params to include adaptive
        # param calcs
        self.V = 1.25 * abs(self.lb - self.ub)
        self.S = 0.9 * self.V
        self.Vmin = np.sqrt(abs(self.lb - self.ub)**2 * self.n) / 500
        self.Smin = self.Vmin

    def _one_iter(self):
        self._prey()
        self._swarm()
        self._follow()
        self._move()
        self._leap()

        al = np.exp(
            -0.75 *
            (self.max_iter - self.current_iter + 1) /
            np.power(self.max_iter, 0.75)
        )

        self.V += self.Vmin - self.V * al
        self.S += self.Smin - self.S * al

        self.Vmin /= 0.5 * np.power(self.max_iter, 1 / 3)
        self.Smin /= np.power(self.max_iter, 1 / 3)

    def _random_from_ball(self, radius):
        direction = np.random.normal(0, 1, size=(self.dimension, ))
        direction /= np.linalg.norm(direction)

        random_radius = np.random.rand()**(1 / self.dimension)
        return direction * random_radius * radius

    def _prey(self):
        for i in range(self.n):
            self._agents[i] = self._prey_one(self._agents[i], i)
        self._agents = self.force_bounds(self._agents)
        self._fitness = [self.ff(x) for x in self._agents]

    def _prey_one(self, x, i):
        y = self._fitness[i]
        for _ in range(self.T):
            x_new = self.force_bounds(
                x + self._random_from_ball(self.V) * self.S)
            y_new = self.ff(x_new)

            if y_new < y:
                return x + (x_new - x) / np.linalg.norm(x_new - x) * self.S

        return x + self._random_from_ball(self.V)

    def _swarm(self):
        x_center = functools.reduce(operator.add, self._agents) / self.n
        x_future_coords = []
        for x_index, x in enumerate(self._agents):
            num_neighbors = 0
            for other_index, other_unit in enumerate(self._agents):
                if (
                    other_index != x_index and
                    np.linalg.norm(x - other_unit) <= self.V
                ):
                    num_neighbors += 1

            if num_neighbors / len(self._agents) < self.D:
                x_future_coords.append(
                    x + (x_center - x) / np.linalg.norm(x_center - x) * self.S)
            else:
                x_future_coords.append(self._prey_one(x, x_index))

        self._agents = self.force_bounds(x_future_coords)
        self._fitness = [self.ff(x) for x in self._agents]

    def _follow(self):
        x_future_coords = []
        for x_index, x in enumerate(self._agents):

            best_neighbour_index = None
            best_neighbour_loss = None
            num_neighbors = 0
            for other_index, other_unit in enumerate(self._agents):
                if (
                    other_index != x_index and
                    np.linalg.norm(x - other_unit) <= self.V
                ):
                    num_neighbors += 1
                    if (
                        best_neighbour_loss is None or
                        best_neighbour_loss > self._fitness[other_index]
                    ):
                        best_neighbour_loss = self._fitness[other_index]
                        best_neighbour_index = other_index

            if (
                best_neighbour_index is not None and
                self._fitness[x_index] > best_neighbour_loss
            ):
                x_future_coords.append(
                    x + (self._agents[best_neighbour_index] - x) /
                    np.linalg.norm(self._agents[best_neighbour_index] - x) *
                    self.S
                )
            else:
                x_future_coords.append(self._prey_one(x, x_index))

        self._agents = self.force_bounds(x_future_coords)
        self._fitness = [self.ff(x) for x in self._agents]

    def _move(self):
        for index in range(self.n):
            self._agents[index] += self._random_from_ball(self.V)
        self._agents = self.force_bounds(self._agents)
        self._fitness = [self.ff(x) for x in self._agents]

    def _leap(self):
        pass
