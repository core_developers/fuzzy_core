import random

from src.optimization_algorithm.meta import OptimizationAlgorithmMeta

import numpy as np


class RandomSearch(OptimizationAlgorithmMeta):
    def __init__(self, n, function, lb, ub, dimension, iteration,
                 base_element=None):
        """
        n - размер популяции
        function - функция, минимум которой требуется найти
        lb - нижняя граница
        ub - верхняя граница
        dimension - размерность
        iteration - количество итераций
        """
        super().__init__()
        self.n = n
        self.ff = function
        if base_element is not None:
            self.lb = min(base_element) * 2
            self.ub = max(base_element) * 2
        else:
            self.lb = lb
            self.ub = ub

        self.iteration = iteration
        self.dimension = dimension
        self.iter_count = 0
        self.__agents = self.__generate_population(base_element)

    def __generate_population(self, base_element):
        if base_element is None:
            return np.random.uniform(self.lb, self.ub, (self.n, self.dimension))
        result = (
                base_element * np.random.uniform(0, 2, (self.n, self.dimension)))
        result[0] = base_element
        return result

    def sort_agents(self):
        order = np.array([
            self.ff(item)
            for item in self.__agents
        ]).argsort()

        self.__agents = self.__agents[order]

    def one_iter(self):
        """
        Одна итерация цикла.
        """
        self.__agents = np.random.uniform(self.lb, self.ub, (self.n, self.dimension))
        self.sort_agents()
        self._set_Gbest(self.__agents[0])

    def get_best(self):
        return self.get_Gbest()
