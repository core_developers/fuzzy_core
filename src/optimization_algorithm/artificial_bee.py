import numpy as np

from src.optimization_algorithm.meta import OptimizationAlgorithmMeta
from src.population_generator import uniform


class ArtificialBeeSwarm(OptimizationAlgorithmMeta):
    """Artificial Bee Alghorithm

    The implementation follows [1]_. *Add important things in desc*.

    Parameters
    ----------
    population : ndarray
        Population of optimization agents
    function : callable
        Function which values are going to be minimized
    lb : None or array_like
        Lower limits of variables being optimized.
    ub : None or array_like
        Lower limits of variables being optimized.
    max_iter : int
        Maximum number of iterations to be performed.
    no_change_iteration: None or int, optional
        Number of consecutive iterations wtih no change of global best
        fitness value

    Attributes
    ----------
    *Add description*

    References
    ----------
    .. [1] Dervis Karaboga (2010) Artificial bee colony algorithm.
           Scholarpedia, 5(3):6915.
    """
    def __init__(
            self,
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration=None):
        super().__init__(
            population, function, lb, ub, max_iter, no_change_iteration)

        if self.n <= 10:
            self.count = self.n - self.n // 2, 1, 1, 1
        else:
            a = self.n // 10
            b = 5
            c = (self.n - a * b - a) // 2
            d = 2
            self.count = a, b, c, d

    def _one_iter(self):
        best = [
            self._agents[i] for i in self._sorted_fitness_args[:self.count[0]]
        ]
        selected = [
            self._agents[i]
            for i in self._sorted_fitness_args[self.count[0]:self.count[2]]
        ]
        newbee = (
            self._new(best, self.count[1]) + self._new(selected, self.count[3]))
        m = len(newbee)
        new_pop = uniform(size=(self.n - m, self.dimension), bounds=(0.0, 1.0))
        new_pop = np.concatenate((newbee, new_pop), axis=0)
        self._set_agents(new_pop)

    def _new(self, l, c):
        bee = []
        for i in l:
            new = [self._neighbour(i) for _ in range(c)]
            bee += new
        bee += l
        return bee

    def _neighbour(self, who):
        neighbour = (
            np.array(who) + np.random.uniform(-1, 1) *
            (
                np.array(who) -
                np.array(self._agents[np.random.randint(0, self.n)])
            )
        )
        return list(neighbour)
