import numpy as np

from src.optimization_algorithm.meta import OptimizationAlgorithmMeta


class BatSwarm(OptimizationAlgorithmMeta):
    """Bat Swarm alghorithm

    The implementation follows [1]_. *Add important things in desc*.

    Parameters
    ----------
    population : ndarray
        Population of optimization agents
    function : callable
        Function which values are going to be minimized
    lb : None or array_like
        Lower limits of variables being optimized.
    ub : None or array_like
        Lower limits of variables being optimized.
    max_iter : int
        Maximum number of iterations to be performed.
    no_change_iteration: None or int, optional
        Number of consecutive iterations wtih no change of global best
        fitness value
    r0: float, optional
        level of impulse emission (default value is 0.9)
    V0: float, optional
        volume of sound (default value is 0.5)
    fmin: float, optional
        min wave frequency (default value is 0)
    fmax: float, optional
        max wave frequency (default value is 0.02)
    alpha: float, optional
        constant for change a volume of sound (default value is 0.9)
    csi: float, optional
        constant for change a level of impulse emission (default value is 0.9)

    Attributes
    ----------
    *Add description*

    References
    ----------
    .. [1] A. Chakri, R. Khelif, M. Benouaret, X.-S. Yang, New
           directional bat algorithm for continuous optimization
           problems, Expert Systems with Applications, vol. 69, 159-175
           (2017).DOI: 10.1016/j.eswa.2016.10.050
    """
    def __init__(
            self,
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration=None,
            r0=0.9,
            V0=0.5,
            fmin=0,
            fmax=0.02,
            alpha=0.9,
            csi=0.9):
        super().__init__(
            population, function, lb, ub, max_iter, no_change_iteration)

        self.r = np.repeat(r0, self.n)
        self.velocity = np.zeros((self.n, self.dimension))
        self.V = np.repeat(V0, self.n)
        self.alpha = alpha
        self.csi = csi
        self.fmin = fmin
        self.fmax = fmax

    def _one_iter(self):
        sol = np.zeros((self.n, self.dimension))
        F = np.random.uniform(
            self.fmin, self.fmax, size=(self.n, self.dimension))
        self.velocity += (self._agents - self._gbest) * F
        rand = np.random.random(size=(self.n, 2))
        for i in range(self.n):
            if rand[i][0] > self.r[i]:
                sol[i] = (
                    self._gbest +
                    np.random.uniform(-1, 1, self.dimension) *
                    self.V.sum() / self.n
                )
            else:
                sol[i] = self._agents[i] + self.velocity[i]
            sol[i] = self.force_bounds(sol[i])
            fval = self.ff(sol[i])
            if fval < self._fitness[i] and rand[i][1] < self.V[i]:
                self._agents[i] = sol[i]
                self._fitness[i] = fval
                self.V[i] *= self.alpha
                self.r[i] *= (1 - np.exp(-self.csi * self.current_iter))
