from math import ceil, exp, floor
import numpy as np
from random import random

from src.optimization_algorithm.meta import OptimizationAlgorithmMeta

# TODO: speed up, improve efficiency


class SocialSpidersSwarm(OptimizationAlgorithmMeta):
    """
    Social Spiders

    The implementation follows [1]_. *Add important things in desc*.

    Parameters
    ----------
    population : ndarray
        Population of optimization agents
    function : callable
        Function which values are going to be minimized
    lb : None or array_like
        Lower limits of variables being optimized.
    ub : None or array_like
        Lower limits of variables being optimized.
    max_iter : int
        Maximum number of iterations to be performed.
    no_change_iteration: None or int, optional
        Number of consecutive iterations wtih no change of global best
        fitness value
    pf : float
        Random parameter from 0 to 1 (default value is 0.4)

    Attributes
    ----------
    *Add description*

    References
    ----------
    .. [1] Dervis Karaboga (2010) Artificial bee colony algorithm.
           Scholarpedia, 5(3):6915.
    """
    def __init__(
            self,
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration=None,
            pf=0.4):
        super().__init__(
            population, function, lb, ub, max_iter, no_change_iteration)

        self.pf = pf
        self.nf = floor((0.9 - random() * 0.25) * self.n)
        self.nm = self.n - self.nf
        self.Nf = self._agents[:self.nf].copy()
        self.Nm = self._agents[self.nf:].copy()
        self.r = (self.ub - self.lb) / 2

    def _one_iter(self):
        self.pb = self._sorted_fitness_args[0]
        self.pw = self._sorted_fitness_args[-1]
        W = (np.array(self._fitness) - self._pworst_f) / (self._pbest_f -
                                                          self._pworst_f)
        Wf = W[:self.nf]
        Wm = W[self.nf:]

        Distf = [self._nearest_spider(i, self.Nf) for i in self.Nm]
        Vibrf = np.array([W[i[1]] * exp(-i[0]**2) for i in Distf])

        # TODO: calc distance matrix with SciPy
        Distb = [np.linalg.norm(i - self._pbest) for i in self._agents]
        Vibrb = np.array([W[self.pb] * exp(-i**2) for i in Distb])

        Distc = [
            [
                (np.linalg.norm(self._agents[i] - self._agents[j]), j)
                for j in range(self.n)
            ]
            for i in range(self.n)
        ]
        for i in range(len(Distc)):
            Distc[i].sort()
        Vibrc = []
        for i in range(self.n):
            for j in range(1, self.n):
                dist = Distc[i][j][0]
                k = Distc[i][j][1]
                if W[k] < W[i]:
                    Vibrc.append(W[k] * exp(-dist**2))
                    break
            else:
                Vibrc.append(W[i])
        Vibrc = np.array(Vibrc)

        fitness = [(self.ff(self.Nm[i]), i) for i in range(self.nm)]
        fitness.sort()
        cent_male = self.Nm[fitness[ceil(self.nm / 2)][1]]
        a = (
            sum([self.Nm[j] * Wm[j] for j in range(self.nm)]) /
            sum([Wm[j] for j in range(self.nm)])
        )
        for i in range(self.n):
            alpha = np.random.random((1, self.dimension))[0]
            beta = np.random.random((1, self.dimension))[0]
            gamma = np.random.random((1, self.dimension))[0]

            r1 = np.random.random((1, self.dimension))[0]
            r2 = np.random.random((1, self.dimension))[0]
            r3 = np.random.random((1, self.dimension))[0]

            if i < self.nf:
                if random() < self.pf:
                    k = Distc[i][1][1]
                    self._agents[i] += alpha * Vibrc[i] * (
                        self._agents[k] -
                        self._agents[i]) + beta * Vibrb[i] * (
                            self._pbest - self._agents[i]) + gamma * (r1 - 0.5)
                else:
                    k = Distc[i][1][1]
                    self._agents[i] -= alpha * Vibrc[i] * (
                        self._agents[k] -
                        self._agents[i]) - beta * Vibrb[i] * (
                            self._pbest - self._agents[i]) + gamma * (r2 - 0.5)
            else:
                # TODO: check source article if same
                # sign in following comparison
                if self.ff(cent_male) > self._fitness[i]:
                    m = i - self.nf - 1
                    k = Distf[m][1]
                    self._agents[i] += alpha * Vibrf[m] * (
                        self._agents[k] - self._agents[i]) + gamma * (r3 - 0.5)
                else:
                    self._agents[i] += alpha * (a - self._agents[i])
            self._agents[i] = self.force_bounds(self._agents[i])
            self._fitness[i] = self.ff(self._agents[i])

        self.Nf = self._agents[:self.nf].copy()
        self.Nm = self._agents[self.nf:].copy()

        best = self.Nm[np.array([self.ff(x) for x in self.Nm]).argmin()]
        indexes = [
            i for i in range(self.nf)
            if np.linalg.norm(self.Nf[i] - best) <= self.r
        ]
        nearest = [self.Nf[i] for i in indexes]
        L = len(nearest)

        if L:
            P = [Wf[i] / sum([Wf[i] for i in indexes]) for i in indexes]
            new_spiders = best + sum([P[i] * nearest[i] for i in range(L)])
            fval = self.ff(new_spiders)
            if fval < self.ff(self._pworst):
                self._agents[self.pw] = new_spiders
                self._agents[self.pw] = self.force_bounds(
                    self._agents[self.pw])
                self._fitness[self.pw] = fval

    # TODO: calc distances vector with numpy
    def _nearest_spider(self, spider, spiders):

        spudis = list(spiders)

        try:
            pos = spudis.index(spider)
            spudis.pop(pos)
        except ValueError:
            pass

        dists = np.array([np.linalg.norm(spider - s) for s in spudis])
        m = dists.argmin()
        d = dists[m]

        return d, m
