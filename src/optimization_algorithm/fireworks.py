import numpy as np
import random

from src.optimization_algorithm.meta import OptimizationAlgorithmMeta


class FireworksSearch(OptimizationAlgorithmMeta):
    """Fireworks Alghorithm

    The implementation follows [1]_. *Add important things in desc*.

    Parameters
    ----------
    population : ndarray
        Population of optimization agents
    function : callable
        Function which values are going to be minimized
    lb : None or array_like
        Lower limits of variables being optimized.
    ub : None or array_like
        Lower limits of variables being optimized.
    max_iter : int
        Maximum number of iterations to be performed.
    no_change_iteration: None or int, optional
        Number of consecutive iterations wtih no change of global best
        fitness value
    m1 : float
        Parameter controlling the number of normal sparks
        (default value is 7)
    m2 : float
        Parameter controlling the number of Gaussian sparks
        (default value is 7)
    amp : float
        Amplitude of normal explosion (default value is 2)
    a : float
        Parameter controlling the lower bound for number
        of normal sparks (default value is 0.3)
    b : float
        Parameter controlling the upper bound for number of normal
        sparks, b must be greater than a (default value is 3)

    Attributes
    ----------
    *Add description*

    References
    ----------
    .. [1] *add*.
    """
    def __init__(
            self,
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration=None,
            m1=7,
            m2=7,
            amp=2,
            a=0.3,
            b=3):
        super().__init__(
            population, function, lb, ub, max_iter, no_change_iteration)

        self.m1 = m1
        self.m2 = m2
        self.eps = np.finfo(float).eps
        self.amp = amp
        self.a = a
        self.b = b

    def _one_iter(self):
        Ymax = self._pworst_f
        sparks = []
        sparks_fit = []
        for i in range(self.n):
            self._explosion_operator(sparks, i, Ymax)
            self._gaussian_mutation(sparks, i)

        for i in range(len(sparks)):
            sparks[i] = self.force_bounds(sparks[i])
            sparks_fit.append(self.ff(sparks[i]))

        self._selection(sparks, sparks_fit)

    def _explosion_operator(self, sparks, index, Ymax):
        sparks_num = self._round(
            self.m1 * (
                Ymax - self._fitness[index] +
                self.eps / (
                    sum([
                        Ymax - self._fitness[i]
                        for i in range(self.n)
                    ]) + self.eps
                )
            ),
            self.m1,
            self.a,
            self.b
        )

        amplitude = (
            self.amp * (self._fitness[index] - Ymax + self.eps) / (
                sum([
                    self._fitness[i] - Ymax
                    for i in range(self.n)
                ]) + self.eps
            )
        )

        for _ in range(int(sparks_num)):
            sparks.append(self._agents[index].copy())
            if random.choice([True, False]):
                sparks[-1] += np.random.uniform(
                    -amplitude, amplitude, self.dimension)

    def _gaussian_mutation(self, sparks, index):
        for _ in range(int(self.m2)):
            sparks.append(self._agents[index].copy())
            if random.choice([True, False]):
                sparks[-1] *= np.random.normal(1.0, 1.0, self.dimension)

    # TODO: reduce number of ff evals
    def _selection(self, sparks, sparks_fit):
        fs = [(sparks_fit[i], i, 0) for i in range(len(sparks_fit))]
        fa = [(self._fitness[i], i, 1) for i in range(self.n)]
        d = np.concatenate((fa, fs))
        f = d[d[:, 0].argsort()]
        temp = self._agents.copy()
        tempf = self._fitness.copy()
        for i in range(self.n):
            ind = int(f[i][1])
            if int(f[i][2]) == 0:
                temp[i] = sparks[ind]
                tempf[i] = sparks_fit[ind]
            else:
                temp[i] = self._agents[ind]
                tempf[i] = self._fitness[ind]
        self._agents = temp
        self._fitness = tempf

    def _round(self, s, m, a, b):
        if s < a * m:
            return round(a * m)
        elif s > b * m:
            return round(b * m)
        else:
            return round(s)
