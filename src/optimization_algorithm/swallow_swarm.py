from src.optimization_algorithm.meta import OptimizationAlgorithmMeta
from hashlib import sha1
import numpy as np


class SwallowSwarm(OptimizationAlgorithmMeta):
    """
    Swallow Swarm

    The implementation follows [1]_. *Add important things in desc*.

    Parameters
    ----------
    population : ndarray
        Population of optimization agents
    function : callable
        Function which values are going to be minimized
    lb : None or array_like
        Lower limits of variables being optimized.
    ub : None or array_like
        Lower limits of variables being optimized.
    max_iter : int
        Maximum number of iterations to be performed.
    no_change_iteration: None or int, optional
        Number of consecutive iterations wtih no change of global best
        fitness value
    nll : float
        Number of local leaders
        (default value is 3)
    nal : float
        Number of aimless particles
        (default value is 6)

    Attributes
    ----------
    *Add description*

    References
    ----------
    .. [1] *add*.
    """
    def __init__(
            self,
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration=None,
            nll=3,
            nal=6):
        super().__init__(
            population, function, lb, ub, max_iter, no_change_iteration)

        self.nll = nll
        self.nal = nal
        self.head_leader = None
        self.local_leaders = None
        self.explorer_particles = None
        self.aimless_particles = None

        self.best_positions = {
            sha1(item).digest(): item
            for item in self._agents
        }

        self.ffs = {
            sha1(self._agents[i]).digest(): self._fitness[i]
            for i in range(self.n)
        }

        self.sort_agents()

    def sort_agents(self):
        fitness = np.array(
            [self.ffs[sha1(item).digest()] for item in self._agents])
        order = fitness.argsort()

        self._agents = self._agents[order]
        for i in range(self.n):
            self._fitness[i] = self.ffs[sha1(self._agents[i]).digest()]

    def set_roles(self):
        diff = self.n - self.nal
        self.head_leader = self._agents[0]
        self.local_leaders = self._agents[1:self.nll + 1]
        self.explorer_particles = self._agents[self.nll + 1:diff]
        self.aimless_particles = self._agents[diff:self.n]

    def discard_roles(self):
        self._agents = np.array([
            self.head_leader, *self.local_leaders, *self.explorer_particles,
            *self.aimless_particles
        ])

    def update_best_position(self, old, new):
        hash_sum = sha1(new).digest()
        old_hash_sum = sha1(old).digest()

        new_fitness = self.ff(new)
        old_best = self.best_positions[old_hash_sum]
        old_fitness = self.ffs[sha1(old_best).digest()]

        self.ffs[hash_sum] = new_fitness

        if new_fitness < old_fitness:
            self.best_positions[hash_sum] = new
        else:
            self.best_positions[hash_sum] = old_best
            self.ffs[sha1(old_best).digest()] = old_fitness

    def find_nearest_local_leader(self, explorer):
        nearest_index = np.linalg.norm(
            self.local_leaders - explorer, axis=1).argmin()

        return self.local_leaders[nearest_index]

    def calculate_vhl(self, explorer, best_explorer_position):
        r1 = np.random.uniform(0, 1, self.dimension)
        r2 = np.random.uniform(0, 1, self.dimension)

        return (
            (best_explorer_position - explorer) * r1 +
            (self.head_leader - explorer) * r2
        )

    def calculate_vll(self, explorer, local_leader, best_explorer_position):
        r1 = np.random.uniform(0, 1, self.dimension)
        r2 = np.random.uniform(0, 1, self.dimension)

        return (
            (best_explorer_position - explorer) * r1 +
            (local_leader - explorer) * r2
        )

    def change_one_explorer(self, explorer):
        best_explorer_position = self.best_positions[sha1(explorer).digest()]

        nearest_ll = self.find_nearest_local_leader(explorer)
        v = (
            self.calculate_vhl(explorer, best_explorer_position) +
            self.calculate_vll(explorer, nearest_ll, best_explorer_position)
        )
        new_position = self.force_bounds(explorer + v)
        self.update_best_position(explorer, new_position)

        return new_position

    def change_explorer_positions(self):
        self.explorer_particles = np.array([
            self.change_one_explorer(item) for item in self.explorer_particles
        ])

    def change_one_aimless(self, aimless):
        new = self.force_bounds(
            aimless * np.random.uniform(0, 2, self.dimension))
        self.update_best_position(aimless, new)

        return new

    def change_aimless_positions(self):
        self.aimless_particles = np.array([
            self.change_one_aimless(item)
            for item in self.aimless_particles
        ])

    def _one_iter(self):
        self.set_roles()
        self.change_explorer_positions()
        self.change_aimless_positions()
        self.discard_roles()
        self.sort_agents()
