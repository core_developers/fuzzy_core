import numpy as np

from src.optimization_algorithm.meta import OptimizationAlgorithmMeta


class WhaleSwarm(OptimizationAlgorithmMeta):
    """
    Whale Swarm

    The implementation follows [1]_. *Add important things in desc*.

    Parameters
    ----------
    population : ndarray
        Population of optimization agents
    function : callable
        Function which values are going to be minimized
    lb : None or array_like
        Lower limits of variables being optimized.
    ub : None or array_like
        Lower limits of variables being optimized.
    max_iter : int
        Maximum number of iterations to be performed.
    no_change_iteration: None or int, optional
        Number of consecutive iterations wtih no change of global best
        fitness value
    ro0 : float
        intensity of ultrasound at the origin of source
        (default value is 2)
    eta : float
        probability of message distortion at large distances
        (default value is 0.005)

    Attributes
    ----------
    *Add description*

    References
    ----------
    .. [1] *add*.
    """
    def __init__(
            self,
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration=None,
            ro0=2,
            eta=0.005):
        super().__init__(
            population, function, lb, ub, max_iter, no_change_iteration)

        self.ro0 = ro0
        self.eta = eta

    def _one_iter(self):
        new_agents = self._agents.copy()
        new_fitness = self._fitness.copy()
        for i in range(self.n):
            y = self._better_and_nearest_whale(i, self.n)
            if y:
                new_agents[i] += np.dot(
                    np.random.uniform(
                        0,
                        self.ro0 * np.exp(-self.eta * self._whale_dist(i, y))
                    ),
                    self._agents[y] - self._agents[i])
                new_agents[i] = self.force_bounds(self._agents[i])
                new_fitness[i] = self.ff(new_agents[i])
            else:
                new_fitness[i] = self._fitness[i]
        self._agents = new_agents
        self._fitness = new_fitness

    def _whale_dist(self, i, j):
        return np.linalg.norm(self._agents[i] - self._agents[j])

    def _better_and_nearest_whale(self, u, n):
        temp = float("inf")
        v = None
        for i in range(n):
            if self._fitness[i] < self._fitness[u]:
                dist_iu = self._whale_dist(i, u)
                if dist_iu < temp:
                    v = i
                    temp = dist_iu

        return v
