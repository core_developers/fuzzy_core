from math import gamma, pi, sin
import numpy as np
from random import normalvariate
from scipy.stats import levy

from src.optimization_algorithm.meta import OptimizationAlgorithmMeta
from src.population_generator import uniform

# TODO: speed up, improve efficiency


class CuckooSearch(OptimizationAlgorithmMeta):
    """Cuckoo Search Alghorithm

    The implementation follows [1]_. *Add important things in desc*.

    Parameters
    ----------
    population : ndarray
        Population of optimization agents
    function : callable
        Function which values are going to be minimized
    lb : None or array_like
        Lower limits of variables being optimized.
    ub : None or array_like
        Lower limits of variables being optimized.
    max_iter : int
        Maximum number of iterations to be performed.
    no_change_iteration: None or int, optional
        Number of consecutive iterations wtih no change of global best
        fitness value
    pa : float
        Probability of cuckoo's egg detection
        (default value is 0.25)
    nest : int
        Number of nests (default value is 100)

    Attributes
    ----------
    *Add description*

    References
    ----------
    .. [1] *add*.
    """
    def __init__(
            self,
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration=None,
            pa=0.25,
            nest=100):
        super().__init__(
            population, function, lb, ub, max_iter, no_change_iteration)

        self.pa = pa
        self.nest = nest

        self.beta = 1.5
        self.sigma = (
            gamma(1 + self.beta) * sin(pi * self.beta / 2) /
            (gamma((1 + self.beta) / 2) * self.beta * 2**((self.beta - 1) / 2))
        )**(1 / self.beta)
        self.u = np.array([
            normalvariate(0, 1)
            for _ in range(self.dimension)]
        ) * self.sigma
        self.v = np.array([normalvariate(0, 1) for _ in range(self.dimension)])
        self.step = self.u / abs(self.v)**(1 / self.beta)

        self._nests = uniform((nest, self.dimension), bounds=(0.0, 1.0))
        self._fitness_nests = [
            self.ff(self._nests[i]) for i in range(self.nest)
        ]

        self.best_nest = self._nests[np.array(self._fitness_nests).argmin()]

    def _one_iter(self):
        vals = np.random.randint(0, self.nest - 1, size=self.n)
        for i in range(self.n):
            if self._fitness[i] < self._fitness_nests[vals[i]]:
                self._nests[vals[i]] = self._agents[i].copy()
                self._fitness_nests[vals[i]] = self._fitness[i]

        fnests = [(self._fitness_nests[i], i) for i in range(self.nest)]
        fnests.sort()
        fcuckoos = [(self._fitness[i], i) for i in range(self.n)]
        fcuckoos.sort(reverse=True)

        nworst = self.nest // 2
        worst_nests = [fnests[-i - 1][1] for i in range(nworst)]
        r = np.random.random(size=nworst)
        to_generate = []
        for i in range(nworst):
            if r[i] < self.pa:
                to_generate.append(worst_nests[i])
        pop = uniform(
            size=(len(to_generate), self.dimension),
            bounds=(0.0, 1.0)
        )
        counter = 0
        for i in to_generate:
            self._nests[i] = pop[counter]
            self._fitness_nests[i] = self.ff(self._nests[i])
            counter += 1

        if self.nest > self.n:
            mworst = self.n
        else:
            mworst = self.nest

        for i in range(mworst):
            if fnests[i][0] < fcuckoos[i][0]:
                self._agents[fcuckoos[i][1]] = self._nests[fnests[i][1]].copy()
                self._fitness[fcuckoos[i][1]] = self._fitness_nests[
                    fnests[i][1]]

        self._levy_fly(self.step, self.best_nest)

    # TODO: check into paper, stepsize not needed mb?
    def _levy_fly(self, step, best_nest):
        flights = levy.rvs(size=(self.n, self.dimension))
        stepsize = 0.2 * step * (self._agents - best_nest)
        self._agents = self._agents + stepsize * flights
        self._agents = self.force_bounds(self._agents)
        self._fitness = [self.ff(x) for x in self._agents]
