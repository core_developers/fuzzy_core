import numpy as np

from src.optimization_algorithm.meta import OptimizationAlgorithmMeta


class GreyWolfSwarm(OptimizationAlgorithmMeta):
    """Gray Wolf Alghorithm

    The implementation follows [1]_. *Add important things in desc*.

    Parameters
    ----------
    population : ndarray
        Population of optimization agents
    function : callable
        Function which values are going to be minimized
    lb : None or array_like
        Lower limits of variables being optimized.
    ub : None or array_like
        Lower limits of variables being optimized.
    max_iter : int
        Maximum number of iterations to be performed.
    no_change_iteration: None or int, optional
        Number of consecutive iterations wtih no change of global best
        fitness value

    Attributes
    ----------
    *Add description*

    References
    ----------
    .. [1] Seyedali Mirjalili, Seyed Mohammad Mirjalili,
        Andrew Lewis (2014): Grey Wolf Optimizer, Advances in
        Engineering Software 69 (2014) 46–61,
        DOI: 10.1016/j.advengsoft.2013.12.0075.
    """
    def __init__(
            self,
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration=None):
        super().__init__(
            population, function, lb, ub, max_iter, no_change_iteration)

        self.velocity = np.zeros((self.n, self.dimension))

    def _one_iter(self):

        self.alpha, self.beta, self.delta = self._get_abd()

        a = 2 - 2 * self.current_iter / self.max_iter

        r1 = np.random.random((self.n, self.dimension))
        r2 = np.random.random((self.n, self.dimension))
        A1 = 2 * r1 * a - a
        C1 = 2 * r2

        r1 = np.random.random((self.n, self.dimension))
        r2 = np.random.random((self.n, self.dimension))
        A2 = 2 * r1 * a - a
        C2 = 2 * r2

        r1 = np.random.random((self.n, self.dimension))
        r2 = np.random.random((self.n, self.dimension))
        A3 = 2 * r1 * a - a
        C3 = 2 * r2

        Dalpha = abs(C1 * self.alpha - self._agents)
        Dbeta = abs(C2 * self.beta - self._agents)
        Ddelta = abs(C3 * self.delta - self._agents)

        X1 = self.alpha - A1 * Dalpha
        X2 = self.beta - A2 * Dbeta
        X3 = self.delta - A3 * Ddelta

        self._agents = (X1 + X2 + X3) / 3
        self._agents = self.force_bounds(self._agents)
        self._fitness = [self.ff(x) for x in self._agents]

    def _get_abd(self):
        result = []
        for i in range(3):
            result.append(self._agents[self._sorted_fitness_args[i]])
        return result
