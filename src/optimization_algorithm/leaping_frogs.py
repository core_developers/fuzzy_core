from src.optimization_algorithm.meta import OptimizationAlgorithmMeta
import numpy as np
import math


class LeapingFrogs(OptimizationAlgorithmMeta):
    """
    Shuffle frog leaping algorithm

    The implementation follows [1]_. *Add important things in desc*.

    Parameters
    ----------
    population : ndarray
        Population of optimization agents
    function : callable
        Function which values are going to be minimized
    lb : None or array_like
        Lower limits of variables being optimized.
    ub : None or array_like
        Lower limits of variables being optimized.
    max_iter : int
        Maximum number of iterations to be performed.
    no_change_iteration: None or int, optional
        Number of consecutive iterations wtih no change of global best
        fitness value
    n_mem : int
        Number of memeplexes (default value is 4)
    local_it : int
        Number of local iterations
    const : float
        coefficient in operator (default value is 1.2)

    Attributes
    ----------
    *Add description*

    References
    ----------
    .. [1] Elbeltagi E., Hegazy T., Grierson D.
        A modified shuffled frog-leaping optimization algorithm:
        applications to project management DOI: 10.1080/15732470500254535.
    """
    def __init__(
            self,
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration=None,
            n_mem=4,
            local_it=5,
            const=1.2):
        super().__init__(
            population, function, lb, ub, max_iter // local_it,
            no_change_iteration)

        self.n_mem = n_mem
        self.n_agents_in_mem = self.n // n_mem
        self.best_positions = {tuple(item): item for item in self._agents}
        self.const = const
        self.local_it = local_it

    def sort_agents(self):
        order = self._sorted_fitness_args
        self._agents = self._agents[order]
        self._fitness = np.array(self._fitness)[order.tolist()]

    def new_vector(self, x, y):
        new_agent = np.random.random() * self.const * (x - y) + y
        return new_agent

    def local_search(self):
        for number_mem in range(self.n_mem):
            f = 1
            for local_iterations in range(self.local_it):
                index = self.n - f * self.n_mem + number_mem

                nv = self.new_vector(self._agents[number_mem],
                                     self._agents[index])

                nv_fval = self.ff(nv)

                if nv_fval < self._fitness[index]:
                    self._agents[index] = nv
                    self._fitness[index] = nv_fval
                    f = f + 1
                else:
                    nv = self.new_vector(self._agents[0], self._agents[index])
                    nv_fval = self.ff(nv)
                    if nv_fval < self._fitness[index]:
                        self._agents[index] = nv
                        self._fitness[index] = nv_fval
                        f = f + 1
                    else:
                        self._agents[index] = (
                            self._agents[0] *
                            np.random.uniform(0, 2, self.dimension)
                        )
                        self._fitness[index] = self.ff(self._agents[index])
                if f > math.ceil(self.n_agents_in_mem / 2):
                    f = 1

    def _one_iter(self):
        self.sort_agents()
        self.local_search()
