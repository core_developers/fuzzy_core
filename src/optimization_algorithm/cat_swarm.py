from random import choice, randint, random

import numpy as np

from src.optimization_algorithm.meta import OptimizationAlgorithmMeta

# TODO : look into source article to reduce amount of code
# TODO: check if _agents.copy() is necessary


class CatSwarm(OptimizationAlgorithmMeta):
    """Cat Swarm Alghorithm

    The implementation follows [1]_. *Add important things in desc*.

    Parameters
    ----------
    population : ndarray
        Population of optimization agents
    function : callable
        Function which values are going to be minimized
    lb : None or array_like
        Lower limits of variables being optimized.
    ub : None or array_like
        Lower limits of variables being optimized.
    max_iter : int
        Maximum number of iterations to be performed.
    no_change_iteration: None or int, optional
        Number of consecutive iterations wtih no change of global best
        fitness value
    mr : int
        Number of cats that hunt (default value is 10)
    smp : int
        Seeking memory pool (default value is 2)
    spc : bool
        Self-position considering (default value is False)
    cdc : int
        Counts of dimension to change (default value is 1)
    srd : float
        Seeking range of the selected dimension
        (default value is 0.1)
    w : float
        Constant (default value is 0.1)
    c : float
        Constant (default value is 1.05)
    csi : float
        Constant (default value is 0.6)

    Attributes
    ----------
    *Add description*

    References
    ----------
    .. [1] *add*
    """
    def __init__(
            self,
            population,
            function,
            lb,
            ub,
            max_iter,
            no_change_iteration=None,
            mr=10,
            smp=2,
            spc=False,
            cdc=1,
            srd=0.1,
            w=0.1,
            c=1.05,
            csi=0.6):
        super().__init__(
            population, function, lb, ub, max_iter, no_change_iteration)

        self.mr = mr
        self.smp = smp
        self.spc = spc
        self.cdc = cdc
        self.srd = srd
        self.w = w
        self.c = c
        self.csi = csi
        self.velocity = np.zeros((self.n, self.dimension))

        self.flag = self._set_flag(self.n, mr)
        if spc:
            self.sm = smp - 1
        else:
            self.sm = smp

    def _one_iter(self):
        for i in range(self.n):
            if self.flag[i] == 0:
                if self.spc:
                    cop = self._change_copy([
                        self._agents[i]], self.cdc, self.srd)[0]
                    tmp = [self._agents[i] for _ in range(self.sm)]
                    tmp.append(cop)
                    copycat = np.array(tmp)
                else:
                    copycat = np.array(
                        [self._agents[i] for j in range(self.sm)])
                copycat = self._change_copy(copycat, self.cdc, self.srd)

                if (
                    copycat.all() ==
                    np.array([copycat[0] for _ in range(self.sm)]).all()
                ):
                    P = np.array([1 for j in range(len(copycat))])
                else:
                    flist = [self.ff(j) for j in copycat]
                    fmax = max(flist)
                    fmin = min(flist)
                    if fmax == fmin:
                        eps = np.finfo(float).eps
                    else:
                        eps = 0
                    P = np.array([
                        abs(self.ff(j) - fmin) / (fmax - fmin + eps)
                        for j in copycat
                    ])
                self._agents[i] = copycat[P.argmax()]
            else:
                delt = (self.max_iter - self.current_iter) / (2 *self.max_iter)
                ww = self.w + delt
                cc = self.c - delt
                r = random()
                self.velocity[i] = (
                    ww * np.array(self.velocity[i]) +
                    r * cc * (np.array(self._pbest) - np.array(self._agents[i]))
                )

                vinf, cinf = self._get_inf(
                    i, self.velocity, self._agents, self.csi)
                self._agents[i] = list(1 / 2 * (vinf + cinf))
            self._agents[i] = self.force_bounds(self._agents[i])
            self._fitness[i] = self.ff(self._agents[i])
        self.flag = self._set_flag(self.n, self.mr)

    @staticmethod
    def _set_flag(n, mr):
        flag = [0 for _ in range(n)]
        m = mr
        while m > 0:
            tmp = randint(0, n - 1)
            if flag[tmp] == 0:
                flag[tmp] = 1
                m -= 1
        return flag

    @staticmethod
    def _change_copy(copycat, cdc, crd):
        for i in range(len(copycat)):
            flag = [0 for _ in range(len(copycat[i]))]
            c = cdc
            while c > 0:
                tmp = randint(0, len(copycat[i]) - 1)
                if flag[tmp] == 0:
                    c -= 1
                    copycat[i][tmp] = copycat[i][tmp] + choice([-1, 1]) * crd
        return copycat

    @staticmethod
    def _get_inf(i, velocity, cat, csi):
        if i == 0:
            vinf = (
                np.array(velocity[i]) + (
                    csi * np.array(velocity[1]) +
                    (1 - csi) * np.array(velocity[2])
                ) / 2 + (
                    csi * np.array(velocity[-1]) +
                    (1 - csi) * np.array(velocity[-2])
                ) / 2
            )

            cinf = (
                np.array(cat[i]) + (
                    csi * np.array(cat[1]) + (1 - csi) * np.array(cat[2])
                ) / 2 + (
                    csi * np.array(cat[-1]) + (1 - csi) * np.array(cat[-2])
                ) / 2
            )
        elif i == 1:
            vinf = (
                np.array(velocity[i]) + (
                    csi * np.array(velocity[2]) +
                    (1 - csi) * np.array(velocity[3])
                ) / 2 + (
                    csi * np.array(velocity[0]) +
                    (1 - csi) * np.array(velocity[-1])
                ) / 2
            )

            cinf = (
                np.array(cat[i]) + (
                    csi * np.array(cat[2]) + (1 - csi) * np.array(cat[3])
                ) / 2 + (
                    csi * np.array(cat[0]) + (1 - csi) * np.array(cat[-1])
                ) / 2
            )

        elif i == len(velocity) - 1:
            vinf = (
                np.array(velocity[i]) + (
                    csi * np.array(velocity[0]) +
                    (1 - csi) * np.array(velocity[1])
                ) / 2 + (
                    csi * np.array(velocity[i - 1]) +
                    (1 - csi) * np.array(velocity[i - 2])
                ) / 2
            )

            cinf = (
                np.array(cat[i]) + (
                    csi * np.array(cat[0]) + (1 - csi) * np.array(cat[1])
                ) / 2 + (
                    csi * np.array(cat[i - 1]) +
                    (1 - csi) * np.array(cat[i - 2])
                ) / 2)

        elif i == len(velocity) - 2:
            vinf = (
                np.array(velocity[i]) + (
                    csi * np.array(velocity[i + 1]) +
                    (1 - csi) * np.array(velocity[0])
                ) / 2 + (
                    csi * np.array(velocity[i - 1]) +
                    (1 - csi) * np.array(velocity[i - 2])
                ) / 2
            )

            cinf = (
                np.array(cat[i]) + (
                    csi * np.array(cat[i + 1]) + (1 - csi) * np.array(cat[0])
                ) / 2 + (
                    csi * np.array(cat[i - 1]) +
                    (1 - csi) * np.array(cat[i - 2])
                ) / 2
            )

        else:
            vinf = (
                np.array(velocity[i]) + (
                    csi * np.array(velocity[i + 1]) +
                    (1 - csi) * np.array(velocity[i + 2])
                ) / 2 + (
                    csi * np.array(velocity[i - 1]) +
                    (1 - csi) * np.array(velocity[i - 2])
                ) / 2
            )

            cinf = (
                np.array(cat[i]) + (
                    csi * np.array(cat[i + 1]) +
                    (1 - csi) * np.array(cat[i + 2])
                ) / 2 + (
                    csi * np.array(cat[i - 1]) +
                    (1 - csi) * np.array(cat[i - 2])
                ) / 2
            )

        return vinf, cinf
