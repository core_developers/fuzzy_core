from pathlib import Path
from functools import partial
from collections import defaultdict
from math import sqrt
import re
import zipfile

from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.preprocessing import MinMaxScaler
from pandas import DataFrame
from joblib import Parallel, delayed
import numpy as np

from src.feature_selector import (
    SwallowSwarmMerge, SwallowSwarmOperations, SwallowSwarmOperationsMerge,
    SwallowSwarmSigmoid, SwallowSwarmTanh, SwallowSwarmV)
from src.population_generator.binary import binary, generator_random_bool
from src.parser.keel_parser import get_data_from_keel_dir
from experiments.feature_selection_with_knn.transformer.transform import (
    transform)


NUM_RUNS = 30
N_JOBS = 15  # -1 для максимальной скорости
PATH_TO_DATA = '../../data/Task1'


def metric(X, Y, weights):
    return sqrt((((X - Y)**2) * weights).sum())


def iterate_data(path):
    for index, item in enumerate(sorted(
        Path(path).iterdir(),
        key=lambda x: int(re.search(r'\d+$', x.name).group(0))), start=1
    ):
        data = list(get_data_from_keel_dir(item))
        yield index, data


def split_train(X, y):
    k_fold = StratifiedKFold(n_splits=10)
    X_res, y_res = [], []
    for train_sub, test_sub in k_fold.split(X, y):
        X_res.append([X[train_sub], X[test_sub]])
        y_res.append([y[train_sub], y[test_sub]])

    return X_res, y_res


def get_accepted_columns(accepted_features):
    return [index for index, item in enumerate(accepted_features) if item]


def fitness_function(X, y, accepted_features, **kwargs):
    accepted_columns = get_accepted_columns(accepted_features)
    alpha = kwargs['alpha']

    if not accepted_columns:
        return 999999

    values = []
    for Xs, ys in zip(X, y):
        X_tra = Xs[0][:, accepted_columns]
        X_tst = Xs[1][:, accepted_columns]
        y_tra, y_tst = ys

        knn = KNeighborsClassifier(n_neighbors=5)

        knn.fit(X_tra, y_tra)
        y_pred = knn.predict(X_tst)
        error = 2 - accuracy_score(y_tst, y_pred, normalize=False)
        values.append(
            (alpha * error) +
            ((1 - alpha) * len(accepted_columns) / Xs[0].shape[1])
        )

    return sum(values) / len(values)


def report(name, item):
    item = np.array(item)

    return (
        f'{name}: '
            f'mean {round(item.mean(), 5)}, '
            f'std {round(item.std(), 5)}, '
            f'min {round(item.min(), 5)}, '
            f'max {round(item.max(), 5)} '
    )


def write(file_obj, lines):
    with file_obj.open('a') as file:
        file.write('\n'.join(lines) + '\n')


def run(algorithm, generator, alg_params, data_path):
    result_file = Path('./result/result.txt')
    if result_file.exists():
        result_file.unlink()
    result_file.parent.mkdir(exist_ok=True)
    result_file.touch()
    features = defaultdict(lambda: defaultdict(list))
    common_features = defaultdict(list)
    for index, data in iterate_data(data_path):
        print(f'USER: {index}')
        write(result_file, [f'USER: {index}'])
        for alpha in (1, 0.9, 0.7, 0.5):
            print(f'ALPHA: {alpha}')
            write(result_file, [f'ALPHA: {alpha}'])
            ff = partial(fitness_function, alpha=alpha)
            accuracy, far_list, frr_list = [], [], []
            accuracy_w, far_list_w, frr_list_w = [], [], []
            num_of_features = []
            feature_weights = defaultdict(int)
            for train, test, _ in data:
                cross_val_weights = defaultdict(int)

                scaler = MinMaxScaler()
                scaler.fit(train.X)
                X, y = split_train(scaler.transform(train.X), train.y)
                result = Parallel(n_jobs=N_JOBS)(
                    delayed(one_run)(
                        algorithm, generator, X, y, ff, alg_params, train, test,
                        scaler
                    )
                    for _ in range(NUM_RUNS)
                )
                for fn, acc, far, frr, res in result:
                    accuracy.append(acc)
                    far_list.append(far)
                    frr_list.append(frr)
                    num_of_features.append(fn)
                    for item in res:
                        cross_val_weights[item] += 1 / NUM_RUNS
                        feature_weights[item] += 1 / (NUM_RUNS * 2)

                weights = np.array([
                    round(cross_val_weights[num], 2)
                    for num in range(100)
                ])

                knn_with_weights = KNeighborsClassifier(
                    metric=metric,
                    metric_params={'weights': weights}
                )

                knn_with_weights.fit(scaler.transform(train.X), train.y)
                y_pred_w = knn_with_weights.predict(
                    np.clip(scaler.transform(test.X), 0.0, 1.0))
                a, b, c, d = confusion_matrix(
                    test.y, y_pred_w).ravel() / len(test.y)
                accuracy_w.append(a + d)
                frr_list_w.append(b / (a + b))
                far_list_w.append(c / (c + d))

            feature_weights = np.array([
                round(feature_weights[num], 2)
                for num in range(100)
            ])

            features[index][alpha].append(feature_weights)
            common_features[alpha].append(feature_weights)

            print(report('Accuracy', accuracy))
            print(report('FAR', far_list))
            print(report('FRR', frr_list))
            print(report('Accuracy Weight', accuracy_w))
            print(report('FAR Weight', far_list_w))
            print(report('FRR Weight', frr_list_w))
            print(report('Feature Num', num_of_features))

            write(result_file, [
                report('Accuracy', accuracy),
                report('FAR', far_list),
                report('FRR', frr_list),
                report('Accuracy Weight', accuracy_w),
                report('FAR Weight', far_list_w),
                report('FRR Weight', frr_list_w),
                report('Feature Num', num_of_features),
            ])

    try:
        transform(str(result_file))
    except Exception:
        pass

    result = defaultdict(lambda: defaultdict(list))
    for user, user_features in features.items():
        for alpha_value, feature_values in user_features.items():
            result[alpha_value][user] = (
                sum(feature_values) / len(feature_values)).round(5)

    for alpha_value, users in result.items():
        DataFrame(users.values(), columns=range(1, 101)).to_csv(
            f'./result/alpha_{str(alpha_value).replace(".", "")}.csv', sep=';')

    features_file = Path('./result/features.txt')

    print('COMMON FEATURE WEIGHTS:')
    write(features_file, ['COMMON FEATURE WEIGHTS:'])
    for alpha_value, feature_values in common_features.items():
        print(f'ALPHA: {alpha_value}')
        write(features_file, [f'ALPHA: {alpha_value}'])

        alpha_features = [
            f'{num}: {round(item, 5)}' for num, item in enumerate(
                sum(feature_values) / len(feature_values), start=1)
        ]

        print('\n'.join(['Features:', *alpha_features]))
        write(features_file, ['Features:', *alpha_features])


def one_run(algorithm, generator, X, y, ff, alg_params, train, test, scaler):
    knn = KNeighborsClassifier(n_neighbors=5)
    function = partial(ff, X=X, y=y)
    alg = algorithm(
        function=function,
        population=generator((50, 100)),
        **alg_params
    )
    best_result = alg.run()[0]
    accepted_columns = get_accepted_columns(best_result)
    X_tra = scaler.transform(train.X)[:, accepted_columns]
    y_tra = train.y
    X_tst = np.clip(scaler.transform(test.X), 0.0, 1.0)[:, accepted_columns]
    y_tst = test.y
    knn.fit(X_tra, y_tra)
    y_pred = knn.predict(X_tst)
    a, b, c, d = confusion_matrix(y_tst, y_pred).ravel() / len(y_tst)
    acc = a + d
    frr = b / (a + b)
    far = c / (c + d)

    return len(accepted_columns), acc, far, frr, accepted_columns


if __name__ == '__main__':
    ALGORITHM_MAP = {
        'sso': (SwallowSwarmMerge, binary),
        'sso_oper': (SwallowSwarmOperations, generator_random_bool),
        'sso_oper_merge': (SwallowSwarmOperationsMerge, generator_random_bool),
        'sso_sigm': (SwallowSwarmSigmoid, binary),
        'sso_tanh': (SwallowSwarmTanh, binary),
        'sso_v': (SwallowSwarmV, binary)
    }

    for alg_name, algorithm in ALGORITHM_MAP.items():
        alg, generator = algorithm
        params = dict(max_iter=50)
        run(alg, generator, params, PATH_TO_DATA)
        z = zipfile.ZipFile(f'result_{alg_name}.zip', 'w')
        for file in Path('./result').iterdir():
            z.write(file)
            file.unlink()
        z.close()
