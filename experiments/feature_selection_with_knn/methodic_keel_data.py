from pathlib import Path
from functools import partial
from math import sqrt

from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import MinMaxScaler
import numpy as np
from joblib import Parallel, delayed

from src.feature_selector import (
    SwallowSwarmMerge, SwallowSwarmOperations, SwallowSwarmOperationsMerge,
    SwallowSwarmSigmoid, SwallowSwarmTanh, SwallowSwarmV)
from src.population_generator.binary import binary, generator_random_bool
from src.parser.keel_parser import get_data_from_keel_dir


N_JOBS = 10  # -1 для максимальной скорости
PATH_TO_DATA = '../../data/for_test_selection'


def metric(X, Y, weights):
    return sqrt((((X - Y)**2) * weights).sum())


def iterate_data(path):
    for item in sorted(Path(path).iterdir()):
        data = list(get_data_from_keel_dir(item))
        yield item.name, data


def split_train(X, y):
    k_fold = StratifiedKFold(n_splits=10)
    X_res, y_res = [], []
    for train_sub, test_sub in k_fold.split(X, y):
        X_res.append([X[train_sub], X[test_sub]])
        y_res.append([y[train_sub], y[test_sub]])

    return X_res, y_res


def get_accepted_columns(accepted_features):
    return [index for index, item in enumerate(accepted_features) if item]


def fitness_function_error(X_tra, y_tra, X_tst, y_tst, accepted_features):
    accepted_columns = get_accepted_columns(accepted_features)

    if not accepted_columns:
        return 1.0

    X_tra = X_tra[:, accepted_columns]
    X_tst = X_tst[:, accepted_columns]

    knn = KNeighborsClassifier(n_neighbors=5)
    knn.fit(X_tra, y_tra)

    y_pred = knn.predict(X_tst)

    return 1 - accuracy_score(y_tst, y_pred)


def fitness_function(X, y, accepted_features, **kwargs):
    accepted_columns = get_accepted_columns(accepted_features)
    alpha = kwargs['alpha']

    if not accepted_columns:
        return 999999

    values = []
    for Xs, ys in zip(X, y):
        X_tra = Xs[0][:, accepted_columns]
        X_tst = Xs[1][:, accepted_columns]
        y_tra, y_tst = ys

        knn = KNeighborsClassifier(n_neighbors=5)

        knn.fit(X_tra, y_tra)
        y_pred = knn.predict(X_tst)
        error = 2 - accuracy_score(y_tst, y_pred, normalize=False)
        values.append(
            (alpha * error) +
            ((1 - alpha) * len(accepted_columns) / Xs[0].shape[1])
        )

    return sum(values) / len(values)


def report(name, item):
    item = np.array(item)

    return (
        f'{name}: '
            f'mean {round(item.mean(), 5)}, '
            f'std {round(item.std(), 5)}, '
            f'min {round(item.min(), 5)}, '
            f'max {round(item.max(), 5)} '
    )


def write(file_obj, lines):
    with file_obj.open('a') as file:
        file.write('\n'.join(lines) + '\n')


def run(algorithm, name, generator, alg_params, data_path):
    result_file = Path(f'./result/{name}.txt')
    if result_file.exists():
        result_file.unlink()
    result_file.parent.mkdir(exist_ok=True)
    result_file.touch()
    for name, data in iterate_data(data_path):
        print(f'DATASET: {name}')
        write(result_file, [f'DATASET: {name}'])
        error_tra_list, error_tst_list = [], []
        num_of_features = []

        result = Parallel(n_jobs=N_JOBS)(
            delayed(cross_val)(train, test, alg_params, algorithm, generator)
            for train, test, _ in data
        )

        for error_tra, error_tst, fn in result:
            error_tra_list.append(error_tra)
            error_tst_list.append(error_tst)
            num_of_features.append(fn)

        print(f'Tra: {round(np.array(error_tra_list).mean() * 100, 3)}')
        print(f'Tst: {round(np.array(error_tst_list).mean() * 100, 3)}')
        print(f'Ftr: {np.array(num_of_features).mean()}')
        write(
            result_file,
            [
                f'Tra: {round(np.array(error_tra_list).mean() * 100, 3)}',
                f'Tst: {round(np.array(error_tst_list).mean() * 100, 3)}',
                f'Ftr: {np.array(num_of_features).mean()}'
            ]
        )


def cross_val(train, test, alg_params, algorithm, generator):
    scaler = MinMaxScaler()
    scaler.fit(train.X)
    X_tra, y_tra, X_tst, y_tst = (
        scaler.transform(train.X),
        train.y,
        scaler.transform(test.X),
        test.y
    )
    fn, error_tra, error_tst, res = one_run(
        algorithm, generator, X_tra, y_tra, X_tst, y_tst,
        fitness_function_error, alg_params, train, test, scaler
    )

    return error_tra, error_tst, fn


def one_run(algorithm, generator, X_tra, y_tra, X_tst, y_tst, ff, alg_params, train, test, scaler):
    knn = KNeighborsClassifier(n_neighbors=5)
    function = partial(ff, X_tra=X_tra, y_tra=y_tra, X_tst=X_tst, y_tst=y_tst)
    alg = algorithm(
        function=function,
        population=generator((50, X_tra.shape[1])),
        **alg_params
    )
    best_result = alg.run()[0]
    accepted_columns = get_accepted_columns(best_result)
    X_tra = scaler.transform(train.X)[:, accepted_columns]
    y_tra = train.y
    X_tst = scaler.transform(test.X)[:, accepted_columns]
    y_tst = test.y
    knn.fit(X_tra, y_tra)
    error_tra = 1 - accuracy_score(y_tra, knn.predict(X_tra))
    error_tst = 1 - accuracy_score(y_tst, knn.predict(X_tst))

    return len(accepted_columns), error_tra, error_tst, accepted_columns


if __name__ == '__main__':
    ALGORITHM_MAP = {
        'sso': (SwallowSwarmMerge, binary),
        'sso_oper': (SwallowSwarmOperations, generator_random_bool),
        'sso_oper_merge': (SwallowSwarmOperationsMerge, generator_random_bool),
        'sso_sigm': (SwallowSwarmSigmoid, binary),
        'sso_tanh': (SwallowSwarmTanh, binary),
        'sso_v': (SwallowSwarmV, binary)
    }

    for alg_name, algorithm in ALGORITHM_MAP.items():
        alg, generator = algorithm
        params = dict(max_iter=50)
        run(alg, alg_name, generator, params, PATH_TO_DATA)
