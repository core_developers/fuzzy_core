from collections import defaultdict

from pandas import DataFrame


def get_list(line, name):
    return [
        float(block.split(' ')[1])
        for block in line.strip().replace(f'{name}: ', '').split(', ')
    ]


def transform(file_path):
    with open(file_path, 'r') as f:
        lines = defaultdict(list)
        for i in range(40):
            f.readline()
            alpha_tables = defaultdict(list)
            for j in range(4):
                f.readline()
                alpha_tables['Acc'].extend(get_list(f.readline(), 'Accuracy'))
                alpha_tables['FAR'].extend(get_list(f.readline(), 'FAR'))
                alpha_tables['FRR'].extend(get_list(f.readline(), 'FRR'))
                alpha_tables['AccW'].extend(
                    get_list(f.readline(), 'Accuracy Weight'))
                alpha_tables['FARW'].extend(
                    get_list(f.readline(), 'FAR Weight'))
                alpha_tables['FRRW'].extend(
                    get_list(f.readline(), 'FRR Weight'))
                alpha_tables['Features'].extend(
                    get_list(f.readline(), 'Feature Num'))
            for type_ in ('Acc', 'FAR', 'FRR', 'AccW', 'FARW', 'FRRW',
                          'Features'):
                lines[type_].append(alpha_tables[type_])

        for name, item in zip(
            ('Acc', 'FAR', 'FRR', 'AccW', 'FARW', 'FRRW', 'Features'),
                lines.values()):
            DataFrame(item, columns=['mean', 'std', 'min', 'max'] * 4).to_csv(
                f'./result/{name}.csv', sep=';')
