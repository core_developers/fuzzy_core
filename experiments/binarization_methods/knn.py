from pathlib import Path
from functools import partial
from math import sqrt

from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import MinMaxScaler
import numpy as np
from joblib import Parallel, delayed

from src.feature_selector import (
    SwallowSwarmMerge, SwallowSwarmOperations, SwallowSwarmOperationsMerge,
    SwallowSwarmSigmoid, SwallowSwarmTanh, SwallowSwarmV)
from src.population_generator.binary import binary, generator_random_bool
from src.parser.keel_parser import get_data_from_keel_dir


N = 40
N_NEIGHBORS = 5
N_ITER = 300
N_JOBS = 2  # -1 для максимальной скорости
PATH_TO_DATA = '../../data/for_test_selection'

ALGORITHM_MAP = {
    'sso_merge': (SwallowSwarmMerge, binary),
    'sso_oper': (SwallowSwarmOperations, generator_random_bool),
    'sso_oper_merge': (SwallowSwarmOperationsMerge, generator_random_bool),
    'sso_sigm': (SwallowSwarmSigmoid, binary),
    'sso_tanh': (SwallowSwarmTanh, binary),
    'sso_v': (SwallowSwarmV, binary)
}


def metric(X, Y, weights):
    return sqrt((((X - Y)**2) * weights).sum())


def iterate_data(path):
    for item in sorted(Path(path).iterdir()):
        data = list(get_data_from_keel_dir(item))
        yield item.name, data


def get_accepted_columns(accepted_features):
    return [index for index, item in enumerate(accepted_features) if item]


def fitness_function_error(X_tra, y_tra, accepted_features):
    accepted_columns = get_accepted_columns(accepted_features)

    if not accepted_columns:
        return 1.0

    X_tra = X_tra[:, accepted_columns]

    knn = KNeighborsClassifier(n_neighbors=5)
    knn.fit(X_tra, y_tra)

    y_pred = knn.predict(X_tra)

    return 1 - accuracy_score(y_tra, y_pred)


def run(algorithm, name, generator, alg_params, data_path):
    result_file = Path(f'./result_knn/{name}.csv')
    if result_file.exists():
        result_file.unlink()
    result_file.parent.mkdir(exist_ok=True)
    result_file.touch()
    header = ('DataSet;TrainBefore;TestBefore;TrainAfter'
              ';TestAfter;Features\n')
    lines = [header]
    for name, data in iterate_data(data_path):
        print(f'DATASET: {name}')
        error_tra_before_list, error_tst_before_list, error_tra_list, error_tst_list = [], [], [], []
        num_of_features = []

        result = Parallel(n_jobs=N_JOBS)(
            delayed(cross_val)(train, test, alg_params, algorithm, generator)
            for train, test, _ in data
        )

        for error_tra_before, error_tst_before, error_tra, error_tst, fn in result:
            error_tra_before_list.append(error_tra_before)
            error_tst_before_list.append(error_tst_before)
            error_tra_list.append(error_tra)
            error_tst_list.append(error_tst)
            num_of_features.append(fn)

        lines.append(';'.join(
            map(str, [
                name,
                round(np.array(error_tra_before_list).mean() * 100, 2),
                round(np.array(error_tst_before_list).mean() * 100, 2),
                round(np.array(error_tra_list).mean() * 100, 2),
                round(np.array(error_tst_list).mean() * 100, 2),
                round(np.array(num_of_features).mean(), 2),
            ])).replace('.', ',') + '\n')

        print(
            round(np.array(error_tra_list).mean() * 100, 2), round(np.array(error_tst_list).mean() * 100, 2),
            round(np.array(num_of_features).mean(), 2)
        )

    with Path(result_file).open('w', encoding='utf-8') as f:
        f.writelines(lines)


def cross_val(train, test, alg_params, algorithm, generator):
    scaler = MinMaxScaler()
    scaler.fit(train.X)
    X_tra, y_tra, X_tst, y_tst = (
        scaler.transform(train.X),
        train.y,
        scaler.transform(test.X),
        test.y
    )

    knn = KNeighborsClassifier()
    knn.fit(X_tra, y_tra)

    error_tra_before = 1 - accuracy_score(y_tra, knn.predict(X_tra))
    error_tst_before = 1 - accuracy_score(y_tst, knn.predict(X_tst))

    fn, error_tra, error_tst, res = one_run(
        algorithm, generator, X_tra, y_tra,
        fitness_function_error, alg_params, train, test, scaler
    )

    return error_tra_before, error_tst_before, error_tra, error_tst, fn


def one_run(algorithm, generator, X_tra, y_tra, ff, alg_params,train, test, scaler):
    knn = KNeighborsClassifier(n_neighbors=N_NEIGHBORS)
    function = partial(ff, X_tra=X_tra, y_tra=y_tra)
    alg = algorithm(
        function=function,
        population=generator((N, X_tra.shape[1])),
        **alg_params
    )
    best_result = alg.run()[0]
    accepted_columns = get_accepted_columns(best_result)
    X_tra = scaler.transform(train.X)[:, accepted_columns]
    y_tra = train.y
    X_tst = scaler.transform(test.X)[:, accepted_columns]
    y_tst = test.y
    knn.fit(X_tra, y_tra)
    error_tra = 1 - accuracy_score(y_tra, knn.predict(X_tra))
    error_tst = 1 - accuracy_score(y_tst, knn.predict(X_tst))

    return len(accepted_columns), error_tra, error_tst, accepted_columns


if __name__ == '__main__':
    for alg_name, algorithm in ALGORITHM_MAP.items():
        print(alg_name)
        alg, generator = algorithm
        params = dict(max_iter=N_ITER)
        run(alg, alg_name, generator, params, PATH_TO_DATA)
