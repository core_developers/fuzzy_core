from pathlib import Path

import numpy as np
from joblib import Parallel, delayed
from sklearn import preprocessing

from src.feature_selector import (
    SwallowSwarmMerge, SwallowSwarmOperations, SwallowSwarmOperationsMerge,
    SwallowSwarmSigmoid, SwallowSwarmTanh, SwallowSwarmV)
from src.fuzzy_system.classifier.fuzzy_classifier import FuzzyClassifier
from src.parser.keel_parser import get_data_from_keel_dir
from src.rules_generator.classifier import ClassExtremum
from src.population_generator import generator_random_bool, binary


N = 40
N_ITER = 300
N_JOBS = 2
PATH_TO_DATA = '../../data/for_test_selection'

ALGORITHM_MAP = {
    'sso_merge': (SwallowSwarmMerge, binary),
    'sso_oper': (SwallowSwarmOperations, generator_random_bool),
    'sso_oper_merge': (SwallowSwarmOperationsMerge, generator_random_bool),
    'sso_sigm': (SwallowSwarmSigmoid, binary),
    'sso_tanh': (SwallowSwarmTanh, binary),
    'sso_v': (SwallowSwarmV, binary)
}


def get_all_data_dirs(path):
    for item in sorted(Path(path).iterdir()):
        if not item.is_dir():
            continue

        yield item


def main(data_path):
    for alg_name, algorithm in ALGORITHM_MAP.items():
        print(alg_name)
        header = ('DataSet;TrainBefore;TestBefore;TrainAfter'
                  ';TestAfter;Features;Time\n')
        lines = [header]
        for data_dir in get_all_data_dirs(data_path):
            print(data_dir.name)
            result = Parallel(n_jobs=N_JOBS)(
                delayed(one_cross_validation)(train, test, algorithm)
                for (train, test, i) in get_data_from_keel_dir(data_dir)
            )
            b_tr, b_tst, a_tr, a_tst, ftr, exec_time = map(
                np.array, zip(*result))
            lines.append(';'.join(
                map(str, [
                    data_dir.name,
                    round(b_tr.mean() * 100, 2),
                    round(b_tst.mean() * 100, 2),
                    round(a_tr.mean() * 100, 2),
                    round(a_tst.mean() * 100, 2),
                    round(ftr.mean(), 2),
                    round(exec_time.mean(), 2)
                ])).replace('.', ',') + '\n')
            print(
                round(a_tr.mean() * 100, 2), round(a_tst.mean() * 100, 2), round(ftr.mean(), 2),
                round(exec_time.mean(), 2)
            )

        with Path(f'./result_fuzzy_classifier/{alg_name}.csv').open('w', encoding='utf-8') as f:
            f.writelines(lines)


def one_cross_validation(train, test, algorithm, term_type='triangle'):
    classifier = FuzzyClassifier(
        metric='accuracy', term_type=term_type, use_constraints=False)
    rules_generator = ClassExtremum()
    transformer = preprocessing.MinMaxScaler()
    alg_name, generator = algorithm
    feature_selector = dict(
        name=alg_name,
        params=dict(
            population=generator((N, train.X.shape[1])),
            max_iter=N_ITER
        )
    )

    classifier.fit(train.X, train.y, rules_generator, transformer)
    before_train = classifier.get_loss(train.X, train.y)
    before_test = classifier.get_loss(test.X, test.y)

    classifier.select_features(train.X, train.y, feature_selector)

    num_accepted_features = classifier.accepted_features.sum()

    after_train = classifier.get_loss(train.X, train.y)
    after_test = classifier.get_loss(test.X, test.y)
    execution_time = classifier.log['time'][-1]

    return (
        before_train, before_test, after_train, after_test,
        num_accepted_features, execution_time
    )


if __name__ == '__main__':
    # Указать свой путь к данным
    main(PATH_TO_DATA)
