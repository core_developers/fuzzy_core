from sklearn.metrics import confusion_matrix
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import MinMaxScaler
from sklearn.neural_network import MLPClassifier
import numpy as np
from catboost import CatBoostClassifier
from lightgbm import LGBMClassifier
from xgboost import XGBClassifier

from src.fuzzy_system.classifier.fuzzy_classifier import FuzzyClassifier
from src.optimization_algorithm.swallow_swarm import SwallowSwarm
from src.parser.csv_parser import load_csv
from src.population_generator.uniform import uniform
from src.rules_generator.classifier import ClassExtremum


def numpy_fill(arr):
    mask = np.isnan(arr)
    idx = np.where(~mask, np.arange(mask.shape[1]),0)
    np.maximum.accumulate(idx, axis=1, out=idx)
    out = arr[np.arange(idx.shape[0])[:, None], idx]

    return out


def fuzzy_classifier_run(train, test, scaler):
    classifier = FuzzyClassifier(metric='f1_score')
    classifier.fit(train.X, train.y, ClassExtremum(), scaler)

    base_element = classifier.get_params()
    rule_optimizers = [(
        SwallowSwarm, dict(
            population=uniform(
                size=(40, base_element.shape[0]),
                base_element=base_element
            ),
            max_iter=1000
        )
    )]

    classifier.optimize_rules(train.X, train.y, rule_optimizers)

    y_pred = classifier.predict(scaler.transform(test.X))[0]

    a, b, c, d = confusion_matrix(test.y, y_pred).ravel() / len(test.y)
    acc = (a + d) * 100
    frr = (b / (a + b)) * 100
    far = (c / (c + d)) * 100

    return acc, frr, far


def run(classifier, train, test, scaler):
    classifier.fit(scaler.transform(train.X), train.y)
    y_pred = classifier.predict(scaler.transform(test.X))

    a, b, c, d = confusion_matrix(test.y, y_pred).ravel() / len(test.y)
    acc = (a + d) * 100
    frr = (b / (a + b)) * 100
    far = (c / (c + d)) * 100

    return acc, frr, far


if __name__ == '__main__':
    CLASSIFIER_MAP = {
        'Tree': DecisionTreeClassifier(),
        'SVC': SVC(),
        'LDA': LinearDiscriminantAnalysis(),
        'Bayes': GaussianNB(),
        'KNN': KNeighborsClassifier(n_neighbors=1),
        'CatBoost': CatBoostClassifier(verbose=False),
        'LightGBM': LGBMClassifier(),
        'XGBoost': XGBClassifier(use_label_encoder=False),
    }

    train = load_csv('../../data/Biosecure_pairs/Data_BiosecurID_train.csv')
    train.X = numpy_fill(train.X)
    train.X = np.fabs(train.X[:, 100:200] - train.X[:, 0:100])
    test = load_csv('../../data/Biosecure_pairs/Data_BiosecurID_tst_random.csv')
    test.X = numpy_fill(test.X)
    test.X = np.fabs(test.X[:, 100:200] - test.X[:, 0:100])
    scaler = MinMaxScaler()
    scaler.fit(train.X)

    print('Fuzzy Classifier')
    print(fuzzy_classifier_run(train, test, scaler))

    for name, classifier in CLASSIFIER_MAP.items():
        print(name)
        print(run(classifier, train, test, scaler))
